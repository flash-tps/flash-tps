
# OpenTPS extension for FLASH PT

`opentps_flash` is an expansion of OpenTPS that enables the optimization of 3D range modulators for conformal FLASH proton therapy.

For additional information, please visit the following webpage: https://flash-tps.gitlab.io/page/ .

# Configuration
You need to configure your system and/or IDE so that it knows that `opentps_core`, `opentps_gui` and `FLASH` must be added to `PYTHONPATH`.

Instructions for `opentps_core` and `opentps_gui` are on http://opentps.org/quickstart/quickstart.html . Adapt those instructions to also include the `FLASH` directory (not `opentps_flash` directory). On Linux, this would be `export PYTHONPATH="path/to/FLASH:$PYTHONPATH"`).

# CT Calibration, beam model, and other parameters:
Can be changed in the config file located in the opentps workspace (default location is in your home folder) or directly in your script (see [simpleConformalFLASHOptimization](https://gitlab.com/flash-tps/flash-tps/-/blob/main/FLASH/opentps_flash/examples/simpleConformalFLASHOptimization.py?ref_type=heads) example).

