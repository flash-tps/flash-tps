
import opentps.gui as opentps_gui
from opentps_flash.gui.flashPanel import FlashPanel

def addToGUI(mainWindow):
    mainWindow.mainToolbar.addWidget(FlashPanel(mainWindow.viewController), 'FLASH TPS')

def run(mainWindow):
    addToGUI(mainWindow)
    opentps_gui.runWithMainWindow(mainWindow)

if __name__ == "__main__":
    viewController = opentps_gui.viewController()
    opentps_gui.runWithMainWindow(viewController.mainWindow)
