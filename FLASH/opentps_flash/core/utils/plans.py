from opentps.core.data.plan import RTPlan, PlanIonBeam, PlanIonLayer
import numpy as np

def generate_uniform_field(gantryAngle=0, energy=229, spacing=5, fieldSize=[20,20], MU=1, kind='square'):

    if kind == "square":
        endpoint_x = fieldSize[0]/2 + 0.1 #to include endpoint
        endpoint_y = fieldSize[1]/2 + 0.1
        x = np.arange(0,endpoint_x,spacing)
        x = np.append(-x[::-1][:-1],x)
        y = np.arange(0,endpoint_y,spacing)
        y = np.append(-y[::-1][:-1],y)
        X,Y = np.meshgrid(x,y, indexing='ij')
    plan = RTPlan()
    beam = PlanIonBeam()
    beam.gantryAngle = gantryAngle
    layer = PlanIonLayer(nominalEnergy=energy)
    layer.appendSpot(x=X.ravel(),y=Y.ravel(),mu=MU*np.ones(len(X.ravel())))
    layer.reorderSpots()
    beam.appendLayer(layer)
    plan.appendBeam(beam)
    return plan