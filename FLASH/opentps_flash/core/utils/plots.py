from typing import Sequence
import numpy as np
import matplotlib.pyplot as plt
from opentps.core.data.images._doseImage import DoseImage
from opentps.core.data.images._roiMask import ROIMask
from opentps_flash.core.data.drvh import DRVH
from opentps.core.data._dvh import DVH
from opentps.core.data import ROIContour

def plot_lateral_dose_profile_y(doses:Sequence[DoseImage], target:ROIMask, delta_measure = 30, save_plot=None):
    plt.figure()
    centerOfMass = target.centerOfMass
    centerOfMass_voxel = ((centerOfMass - target.origin) / target.spacing).astype(int)
    targetStartZ = np.argmax(target.imageArray[centerOfMass_voxel[0], centerOfMass_voxel[1],:] > 0)
    targetEndZ = target.gridSize[2] - np.argmax(target.imageArray[centerOfMass_voxel[0], centerOfMass_voxel[1],::-1] > 0)
    
    measureStartZ = np.maximum(targetStartZ * target.spacing[2] + target.origin[2] - delta_measure, target.origin[2])
    measureEndZ = np.minimum(targetEndZ * target.spacing[2] + target.origin[2] + delta_measure, target.gridSize[2]*target.spacing[2]+target.origin[2])
    
    if type(doses) is not list: doses = [doses]
    for dose in doses:
        indexes_start = dose.getVoxelIndexFromPosition([centerOfMass[0], centerOfMass[1], measureStartZ])
        indexes_end = dose.getVoxelIndexFromPosition([centerOfMass[0], centerOfMass[1], measureEndZ])
        profile_dose = dose.imageArray[indexes_start[0], indexes_start[1], indexes_start[2]:indexes_end[2]]
        x = np.linspace(measureStartZ, measureEndZ, indexes_end[2] - indexes_start[2])
        plt.plot(x, profile_dose, label=dose.name)
    
    plt.title("Lateral profile - y")
    plt.xlabel("Position - y[mm]")
    plt.ylabel("Dose [Gy]")
    plt.grid(True)
    plt.legend()
    if save_plot is not None:
        plt.savefig(save_plot, format='png')
    plt.show()

def plot_lateral_dose_profile_x(doses:Sequence[DoseImage], target:ROIMask, delta_measure = 30, save_plot=None):
    fig = plt.figure()
    centerOfMass = target.centerOfMass
    centerOfMass_voxel = ((centerOfMass - target.origin) / target.spacing).astype(int)
    targetStartZ = np.argmax(target.imageArray[:,centerOfMass_voxel[1], centerOfMass_voxel[2]] > 0)
    targetEndZ = target.gridSize[0] - np.argmax(target.imageArray[::-1,centerOfMass_voxel[1], centerOfMass_voxel[2]] > 0)
    
    measureStartZ = np.maximum(targetStartZ * target.spacing[0] + target.origin[0] - delta_measure, target.origin[0])
    measureEndZ = np.minimum(targetEndZ * target.spacing[0] + target.origin[0] + delta_measure, target.gridSize[0]*target.spacing[0]+target.origin[0])
    
    if type(doses) is not list: doses = [doses]
    for dose in doses:
        indexes_start = dose.getVoxelIndexFromPosition([measureStartZ, centerOfMass[1], centerOfMass[2]])
        indexes_end = dose.getVoxelIndexFromPosition([measureEndZ,centerOfMass[1], centerOfMass[2]])
        profile_dose = dose.imageArray[indexes_start[0]:indexes_end[0], indexes_start[1], indexes_start[2]]
        x = np.linspace(measureStartZ, measureEndZ, indexes_end[0] - indexes_start[0])
        plt.plot(x, profile_dose, label=dose.name)
    
    plt.title("Lateral profile - x")
    plt.xlabel("Position - x[mm]")
    plt.ylabel("Dose [Gy]")
    plt.grid(True)
    plt.legend()
    if save_plot is not None:
        plt.savefig(save_plot, format='png')
    plt.show()


def plot_depth_dose_profile(doses:Sequence[DoseImage], target:ROIMask, delta_measure = 30, save_plot=None):
    if isinstance(target, ROIContour):
        roi = roi.getBinaryMask(doses[0].origin, doses[0].gridSize, doses[0].spacing)
    centerOfMass = target.centerOfMass
    centerOfMass_voxel = ((centerOfMass - target.origin) / target.spacing).astype(int)
    targetStartY = np.argmax(target.imageArray[centerOfMass_voxel[0], :, centerOfMass_voxel[2]] > 0)
    targetEndY = target.gridSize[1] - np.argmax(target.imageArray[centerOfMass_voxel[0], ::-1, centerOfMass_voxel[2]] > 0)
    
    measureStartY = np.maximum(targetStartY * target.spacing[1] + target.origin[1] - delta_measure, target.origin[1])
    measureEndY = np.minimum(targetEndY * target.spacing[1] + target.origin[1] + delta_measure, target.gridSize[1]*target.spacing[1]+target.origin[1])
    
    if type(doses) is not list: doses = [doses]
    for dose in doses:
        indexes_start = dose.getVoxelIndexFromPosition([centerOfMass[0], measureStartY, centerOfMass[2]])
        indexes_end = dose.getVoxelIndexFromPosition([centerOfMass[0], measureEndY ,centerOfMass[2]])
        profile_dose = dose.imageArray[indexes_start[0], indexes_start[1]:indexes_end[1], indexes_start[2]]
        x = np.linspace(measureStartY, measureEndY, indexes_end[1] - indexes_start[1])
        plt.plot(x, profile_dose, label=dose.name)
    
    plt.title("Depth-dose profile")
    plt.xlabel("Position [mm]")
    plt.ylabel("Dose [Gy]")
    plt.grid(True)
    plt.legend()
    if save_plot is not None:
        plt.savefig(save_plot, format='png')
    plt.show()

def plot_map(plan, startTime=None, save_plot=None):
    xy = plan[0].spotXY
    x= np.array([elem[0] for elem in xy])
    y= np.array([elem[1] for elem in xy])
    mu = plan[0].spotMUs

    fig, ax = plt.subplots(1, 1)
    for i, xyElem in enumerate(xy):
        if i==0:
            if startTime is not None:  
                ax.text(x[i],y[i],f'{startTime[i]*1000:.0f}')
            continue
        # ax.arrow(-xy[i-1][0], xy[i-1][1], -xyElem[0]+xy[i-1][0], xyElem[1]-xy[i-1][1], fc='k', ec='k', head_width=1, head_length=1)
        ax.arrow(xy[i-1][0], xy[i-1][1], xyElem[0]-xy[i-1][0], xyElem[1]-xy[i-1][1], fc='k', ec='k', head_width=1, head_length=1)
        if startTime is not None:  
            # ax.text(-x[i],y[i],f'{startTime[i]*1000:.0f}')
            ax.text(x[i],y[i],f'{startTime[i]*1000:.0f}')

    # cax = ax.scatter(-x, y, c=mu, cmap=plt.cm.jet)
    cax = ax.scatter(x, y, c=mu, cmap=plt.cm.jet)
    ax.set_xlabel("x (mm)")
    ax.set_ylabel("y (mm)")
    ax.set_aspect('equal')
    cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
    cbar.set_ticks([np.min(mu), (np.min(mu)+np.max(mu))/2., np.max(mu)])
    cbar.ax.set_title('Monitor units')
    ax.grid(True)
    ax.legend()
    if save_plot is not None:
        fig.savefig(save_plot, format='png')

def plot_DVH(DVHs, save_plot=None, legend=None, xlim=(0,100)):
    fig, ax = plt.subplots(1, 1)
    if type(DVHs) is list:
        for i, dvh in enumerate(DVHs):
            label = legend[i] if legend is not None else dvh.name
            ax.plot(dvh.histogram[0], dvh.histogram[1], label=label, linewidth=2)
        dvh_type = type(DVHs[0])
    else:
        label = legend if legend is not None else DVHs.name
        ax.plot(DVHs.histogram[0], DVHs.histogram[1], label=label, linewidth=2)
        dvh_type = type(DVHs)
    if dvh_type is DVH:
        ax.set_title("DVH")
        ax.set_xlabel("Dose (Gy)")
        ax.set_ylabel("Volume (%)")
        ax.grid(True)
        ax.set_ylim((0, 100))
        ax.set_xlim(xlim)
    elif dvh_type is DRVH:
        ax.set_title("Percentile-95 DRVH")
        ax.set_xlabel("Dose rate (Gy/s)")
        ax.set_ylabel("Volume (%)")
        ax.grid(True)
        ax.set_ylim((0, 100))
        ax.set_xlim(xlim)
    ax.legend()
    if save_plot is not None:
        fig.savefig(save_plot, format='png')
    plt.show()

def plot_CT_with_dose(ct, roi, dose, save_plot=None, prescription=8, show="vertical", show_dose=True):
    # TODO: ensure that all images have the same grid size
    dx = ct.spacing[0]/2.
    dy = ct.spacing[1]/2.
    endx = ct.origin[0] + (ct.gridSize[0]-1)*ct.spacing[0]
    endy = ct.origin[1] + (ct.gridSize[1]-1)*ct.spacing[1]

    if isinstance(roi, ROIContour):
        roi = roi.getBinaryMask(ct.origin, ct.gridSize, ct.spacing)
    roiCenterOfMass = roi.centerOfMass
    roiCenterOfMassInVoxels = ct.getVoxelIndexFromPosition(roiCenterOfMass)
    fig, ax = plt.subplots(1, 1)
    dose_to_plot = dose.imageArray.copy()
    dose_to_plot[dose_to_plot<0.1] = np.nan
    dose_to_plot[dose_to_plot>prescription+1] = np.nan
    roi_to_plot = roi.getBinaryContourMask().imageArray.astype(float)
    roi_to_plot[roi_to_plot==0.] = np.nan
    
    if show == "vertical":
        extent = [ct.origin[0]-dx, endx+dx, endy+dy, ct.origin[1]-dy]
        ax.imshow(ct.imageArray[:,:,roiCenterOfMassInVoxels[2]].T, cmap='gray', extent=extent, origin="upper")
        if show_dose: ax.imshow(dose_to_plot[:,:,roiCenterOfMassInVoxels[2]].T, cmap='jet', alpha=0.5, extent=extent, origin="upper")
        ax.imshow(roi_to_plot[:,:,roiCenterOfMassInVoxels[2]].T, interpolation='None', extent=extent, origin="upper")
    else:
        extent = [ct.origin[1]-dy, endy+dy, endx+dx, ct.origin[0]-dx]
        ax.imshow(ct.imageArray[:,:,roiCenterOfMassInVoxels[2]], cmap='gray', extent=extent, origin="upper")
        if show_dose: ax.imshow(dose_to_plot[:,:,roiCenterOfMassInVoxels[2]], cmap='jet', alpha=0.5, extent=extent, origin="upper")
        ax.imshow(roi_to_plot[:,:,roiCenterOfMassInVoxels[2]], interpolation='None', extent=extent, origin="upper")        

    if save_plot is not None:
        fig.savefig(save_plot, format='png')

    
def plot_depth_dose_and_CT(ct, doses:Sequence[DoseImage], roi:ROIMask, delta_measure = 0, save_plot=None, ylim=12, average_across_voxel=0):
    dx = ct.spacing[0]/2.
    dy = ct.spacing[1]/2.
    endx = ct.origin[0] + (ct.gridSize[0]-1)*ct.spacing[0]
    endy = ct.origin[1] + (ct.gridSize[1]-1)*ct.spacing[1]

    if isinstance(roi, ROIContour):
        roi = roi.getBinaryMask(ct.origin, ct.gridSize, ct.spacing)
    centerOfMass = roi.centerOfMass
    centerOfMass_voxel = ct.getVoxelIndexFromPosition(centerOfMass)

    roi_to_plot = roi.getBinaryContourMask().imageArray.astype(float)
    roi_to_plot[roi_to_plot==0.] = np.nan
    voxel_depth_target = np.flatnonzero(roi_to_plot[centerOfMass_voxel[0],:,centerOfMass_voxel[2]]==1)
    assert len(voxel_depth_target)==2
    target_start = ct.origin[1] + voxel_depth_target[0] * ct.spacing[1]
    target_end = ct.origin[1] + voxel_depth_target[1] * ct.spacing[1]

    fig, ax = plt.subplots(2, 1, sharex=True, figsize=(10,5), layout='constrained')
    
    for dose in doses:
        if average_across_voxel == 0:
            profile_dose = dose.imageArray[centerOfMass_voxel[0], :, centerOfMass_voxel[2]]
        else:
            profile_dose = np.zeros_like(dose.imageArray[centerOfMass_voxel[0], :, centerOfMass_voxel[2]])
            for i in range(-average_across_voxel,average_across_voxel+1):
                profile_dose += dose.imageArray[centerOfMass_voxel[0]+i, :, centerOfMass_voxel[2]+i]
            profile_dose /= (2*average_across_voxel + 1)
        x = np.linspace(dose.origin[1] - dose.spacing[1]/2, dose.origin[1] + dose.spacing[1] * dose.gridSize[1] + dose.spacing[1]/2 , dose.gridSize[1])
        ax[0].plot(x, profile_dose, label=dose.name)
    ax[0].set_title("Depth-dose profile")
    ax[0].set_ylabel("Dose [Gy]")
    ax[0].axvline(x=target_start, color='r')
    ax[0].axvline(x=target_end, color='r')
    ax[0].annotate(f"{profile_dose[voxel_depth_target[0]]:.1f}", (target_start+.5, profile_dose[voxel_depth_target[0]]+.5))
    ax[0].annotate(f"{profile_dose[voxel_depth_target[1]+1]:.1f}", (target_end+.5, profile_dose[voxel_depth_target[1]+1]+.5))
    if np.isscalar(ylim):
        ylim_tuple = (ylim-2, ylim+2)
        ax[0].set_ylim(ylim_tuple)
    else:
        ylim_tuple = ylim
        ax[0].set_ylim(ylim)
    ax[0].grid(True)
    if type(doses) is list: ax[0].legend()

    # CT
    extent = [ct.origin[1]-dy, endy+dy, endx+dx, ct.origin[0]-dx]
    ax[1].imshow(ct.imageArray[:,:,centerOfMass_voxel[2]], cmap='gray', extent=extent, origin="upper")  
    dose_array = dose.imageArray[:,:,centerOfMass_voxel[2]].copy()
    dose_array[dose_array<ylim_tuple[0]] = np.nan
    dose_array[dose_array>ylim_tuple[1]] = np.nan
    dose_plot = ax[1].imshow(dose_array, interpolation='nearest', cmap='jet', extent=extent, origin='upper', alpha=0.6, vmin=ylim_tuple[0], vmax=ylim_tuple[1])
    ax[1].imshow(roi_to_plot[:,:,centerOfMass_voxel[2]], interpolation='None', extent=extent, origin="upper") 
    ax[1].set_xlabel("Position [mm]")
    ax[1].set_aspect('auto')
    fig.colorbar(dose_plot, ax=ax[1])

    if save_plot is not None:
        fig.savefig(save_plot, format='png')
