import copy
import os
import platform
import shutil
from typing import Optional

import numpy as np
from opentps.core.data import SparseBeamlets
from opentps.core.data.images import CTImage, ROIMask, DoseImage
from opentps.core.data.plan import RTPlan, PlanIonBeam, PlanIonLayer
from opentps.core.io import mcsquareIO
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps.core.processing.imageProcessing import imageTransform3D
from opentps.core.processing.rangeEnergy import energyToRangeMM, rangeMMToEnergy
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.DoseCalculation.MCsquare.mcsquareFlashConfig import \
    MCsquareFlashConfig
from opentps_flash.core.processing.DoseCalculation.fluenceCalculator import FluenceCalculatorBEV


class Beamlets():
    def __init__(self):
        self.sparseBeamlets:SparseBeamlets = None
        self.referencePlan:RTPlan = None

class FluenceBasedMCsquareDoseCalculator(MCsquareDoseCalculator):
    HU_AIR = -1024
    SPOT_WEIGHT_THRESHOLD = 1.

    def __init__(self):
        super().__init__()

        self.fluenceCalculator = FluenceCalculatorBEV()
        self.fluenceCalculator.beamModel = self.beamModel

        self._distToIsocenter = 0.0

    @property
    def distanceToIsocenter(self) -> float:
        return self._distToIsocenter

    @distanceToIsocenter.setter
    def distanceToIsocenter(self, dist: float):
        self._distToIsocenter = dist

    def computeDose(self, ct:CTImage, plan: RTPlan) -> DoseImage:
        self._ct = ct
        self._plan = plan
        return super().computeDose(ct, self._fluencePlan(plan))

    def computeBeamlets(self, ct:CTImage, plan:RTPlan, roi:Optional[ROIMask]=None, deltaR:float=0.) -> Beamlets:
        self._ct = CTImage.fromImage3D(ct, patient=None)
        self._roi = ROIMask.fromImage3D(roi, patient=None)
        self._plan = self._fluencePlan(plan, deltaR)

        self._config = self._noSpotSizeConfig

        self._writeFilesToSimuDir()
        self._cleanDir(self._workDir)
        self._startMCsquare()

        beamlets = Beamlets()
        beamlets.sparseBeamlets = self._importBeamlets()
        beamlets.referencePlan = copy.deepcopy(self._plan)

        return beamlets

    def _fluencePlan(self, plan:RTPlan, deltaR:float=0.) -> RTPlan:
        newPlan = RTPlan()

        for beam in plan:
            newBeam = PlanIonBeam()
            newBeam.gantryAngle = beam.gantryAngle
            newBeam.couchAngle = beam.couchAngle
            newBeam.isocenterPosition = np.array(beam.isocenterPosition)
            # TODO: Can there be any range shifter?

            if newBeam.gantryAngle==0:
                ctBEV = imageTransformBEV.gantry0ToBEV(self._ct)
                isocenterBEV = imageTransformBEV.gantry0CoordinateToBEV(self._ct, newBeam.isocenterPosition)
            else:
                ctBEV = imageTransform3D.dicomToIECGantry(self._ct, newBeam, self.HU_AIR, cropROI=self._roi, cropDim0=True, cropDim1=True, cropDim2=False)
                isocenterBEV = imageTransform3D.dicomCoordinate2iecGantry(newBeam, newBeam.isocenterPosition)

            for layer in beam:
                fluenceCalculator = self.fluenceCalculator
                fluenceCalculator.beamModel = self.beamModel
                fluence = fluenceCalculator.layerFluenceAtNozzle(layer, ctBEV, isocenterBEV)
                fluence = fluence.imageArray

                fluence[fluence<self.SPOT_WEIGHT_THRESHOLD] = 0.
                #fluence = fluence*layer.meterset/np.sum(fluence)

                newLayer = PlanIonLayer(nominalEnergy = rangeMMToEnergy(energyToRangeMM(layer.nominalEnergy) + deltaR))

                fluenceX, fluenceY = np.meshgrid(range(fluence.shape[0]), range(fluence.shape[1]))

                for i in range(fluenceX.shape[0]):
                    for j in range(fluenceX.shape[1]):
                        if (fluence[fluenceX[i, j], fluenceY[i, j]]>0.):
                            posNozzle = ctBEV.getPositionFromVoxelIndex((fluenceX[i, j], fluenceY[i, j], 0))
                            pos0Nozzle = posNozzle[0] - isocenterBEV[0]
                            pos1Nozzle = -posNozzle[1] + isocenterBEV[1]

                            pos0Iso = pos0Nozzle*self.beamModel.smx/(self.beamModel.smx-self.beamModel.nozzle_isocenter)
                            pos1Iso = pos1Nozzle*self.beamModel.smy/(self.beamModel.smy-self.beamModel.nozzle_isocenter)

                            newLayer.appendSpot(pos0Iso, pos1Iso, fluence[fluenceX[i, j], fluenceY[i, j]])

                newBeam.appendLayer(newLayer)
            newPlan.appendBeam(newBeam)
        return newPlan

    @property
    def _noSpotSizeConfig(self) -> MCsquareFlashConfig:
        config = MCsquareFlashConfig()

        config["Num_Primaries"] = self._nbPrimaries
        config["WorkDir"] = self._mcsquareSimuDir
        config["CT_File"] = self._ctName
        config["ScannerDirectory"] = self._scannerFolder  # ??? Required???
        config["HU_Density_Conversion_File"] = os.path.join(self._scannerFolder, "HU_Density_Conversion.txt")
        config["HU_Material_Conversion_File"] = os.path.join(self._scannerFolder, "HU_Material_Conversion.txt")
        config["BDL_Machine_Parameter_File"] = self._bdlFilePath
        config["BDL_Plan_File"] = self._planFilePath

        config["Dose_to_Water_conversion"] = "OnlineSPR"
        config["Compute_stat_uncertainty"] = False
        config["Beamlet_Mode"] = True
        config["Beamlet_Parallelization"] = True
        config["Dose_MHD_Output"] = False
        config["Dose_Sparse_Output"] = True

        config["Dose_Sparse_Threshold"] = 20000

        config["NoSpotSize"] = True

        print(config)
        return config

    def _writeFilesToSimuDir(self):
        self._cleanDir(self._materialFolder)
        self._cleanDir(self._scannerFolder)

        mcsquareIO.writeCT(self._ct, self._ctFilePath)
        mcsquareIO.writePlan(self._plan, self._planFilePath, self._ct, self._beamModel)
        mcsquareIO.writeCTCalibrationAndBDL(self._ctCalibration, self._scannerFolder, self._materialFolder,
                                            self._beamModel, self._bdlFilePath)
        mcsquareIO.writeConfig(self._config, self._configFilePath)
        self._writeBin() # We export the FLASH specific version of MCsquare

    def _writeBin(self):
        destFolder = self._mcsquareSimuDir

        import opentps_flash.core.processing.DoseCalculation.MCsquare as MCsquareModule
        mcsquarePath = str(MCsquareModule.__path__[0])

        if (platform.system() == "Linux"):
            source_path = os.path.join(mcsquarePath, "MCsquare")
            destination_path = os.path.join(destFolder, "MCsquare")
            shutil.copyfile(source_path, destination_path)  # copy file
            shutil.copymode(source_path, destination_path)  # copy permissions

            source_path = os.path.join(mcsquarePath, "MCsquare_linux")
            destination_path = os.path.join(destFolder, "MCsquare_linux")
            shutil.copyfile(source_path, destination_path)
            shutil.copymode(source_path, destination_path)
        else:
            raise Exception("Error: Operating system " + platform.system() + " is not supported by MCsquare.")
