import logging
import math
from typing import Optional

import numpy as np
from opentps.core.data.images import CTImage, DoseImage, RSPImage, ROIMask
from opentps.core.data.plan import PlanIonBeam
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.DoseCalculation.analyticalNoScatteringBEV import \
    AnalyticalNoScatteringBEV
from opentps_flash.core.processing.DoseCalculation.fluenceCalculator import FluenceCalculatorBEV
from scipy.ndimage import gaussian_filter
from scipy.optimize import fminbound


logger = logging.getLogger(__name__)


def indexOfNearest(array, values):
    array = np.asarray(array)
    arrayLen = len(array)

    valuesScalar = np.isscalar(values)
    if np.isscalar(values):
        values = np.array([values])

    array = np.reshape(array, (1, len(array)))
    values = np.reshape(values, (len(values), 1))

    array = np.tile(array, (len(values), 1))
    values = np.tile(values, (1, arrayLen))

    idx = np.argmin(np.abs(array  - values), axis=1)

    if valuesScalar:
        return idx[0]
    else:
        return idx

class AnalyticalBEV(AnalyticalNoScatteringBEV):
    MIN_FLUENCE = 0.

    def __int__(self):
        super().__init__()

        self._sigmaX = None
        self._sigmaY = None

    def _computeDoseForBeam2(self, ctBEV:CTImage, beam:PlanIonBeam, roiBEV:Optional[ROIMask]) -> DoseImage:
        fluenceCalculator = FluenceCalculatorBEV()
        fluenceCalculator.beamModel = self.beamModel

        rspImage = RSPImage.fromCT(ctBEV, self.ctCalibration)

        doseImageBEV = DoseImage.fromImage3D(rspImage)
        doseImageArray = np.zeros(doseImageBEV.gridSize)

        wetBeforeCT = self._wetBeforeCT(beam)
        if np.isscalar(wetBeforeCT):
            wetBeforeCT = wetBeforeCT*np.ones((doseImageBEV.gridSize[0], doseImageBEV.gridSize[1]))

        for layer in beam:
            fluence0 = self._fluence(layer, ctBEV, beam.isocenterPosition)
            fluence0 = fluence0.imageArray

            rspArray = rspImage.imageArray
            rspArray[:, :, 0] = rspArray[:, :, 0] + wetBeforeCT + energyToRangeMM(self.referenceEnergy) - energyToRangeMM(layer.nominalEnergy)

            oldWEPL = 0
            for k in range(rspArray.shape[2]):
                logger.info(k)

                if k>0:
                    newWEPL = oldWEPL + np.mean(rspArray[:, :, k-1])
                    oldWEPL = newWEPL
                else:
                    newWEPL = oldWEPL

                sigmaX_k = np.sqrt(np.interp(newWEPL, self._referenceIDDX, self._sigmaX)**2 - self._sigmaX[0]**2)
                sigmaY_k = np.sqrt(np.interp(newWEPL, self._referenceIDDX, self._sigmaY)**2 - self._sigmaY[0]**2)

                if sigmaX_k>0 or sigmaY_k>0:
                    newFluence_k = gaussian_filter(fluence0, [sigmaX_k/doseImageBEV.spacing[0], sigmaY_k/doseImageBEV.spacing[1]], mode='constant', cval=0., truncate=4)
                else:
                    newFluence_k = fluence0

                integDose = np.interp(newWEPL, self._referenceIDDX, self._referenceIDD)
                doseImageArray[:, :, k] += newFluence_k * integDose * self.beamModel.computeMU2Protons(layer.nominalEnergy) / self.beamModel.computeMU2Protons(self.referenceEnergy)

        doseImageBEV.imageArray = doseImageArray

        return doseImageBEV

    def _computeDoseForBeam(self, ctBEV:CTImage, beam:PlanIonBeam, roiBEV:Optional[ROIMask]) -> DoseImage:
        fluenceCalculator = FluenceCalculatorBEV()
        fluenceCalculator.beamModel = self.beamModel

        rspImage = RSPImage.fromCT(ctBEV, self.ctCalibration)

        doseImageBEV = DoseImage.fromImage3D(rspImage)
        doseImageArray = np.zeros(doseImageBEV.gridSize)

        wetBeforeCT = self._wetBeforeCT(beam)
        if np.isscalar(wetBeforeCT):
            wetBeforeCT = wetBeforeCT*np.ones((doseImageBEV.gridSize[0], doseImageBEV.gridSize[1]))

        rangeStep = 0.5
        weplVec =  np.arange(0, energyToRangeMM(self.referenceEnergy) + 10, rangeStep) # 10 is just a margin

        for layer in beam:
            fluence0 = self._fluence(layer, ctBEV, beam.isocenterPosition)

            fluenceWEPL = np.zeros((fluence0.gridSize[0], fluence0.gridSize[1], len(weplVec)))
            fluenceWEPL[:, :, 0] = fluence0.imageArray

            rspArray = rspImage.imageArray
            rspArray[:, :, 0] = rspArray[:, :, 0] + wetBeforeCT + energyToRangeMM(self.referenceEnergy) - energyToRangeMM(layer.nominalEnergy)

            for k in range(rspArray.shape[2]):
                newFluenceWEPL = np.zeros(fluenceWEPL.shape)
                logger.info(k)

                for oldWEPLInd, oldWEPL in enumerate(weplVec):
                    newFluence_k = fluenceWEPL[:, :, oldWEPLInd]

                    if np.sum(newFluence_k)<=0:
                        continue

                    if k>0:
                        newWEPL = oldWEPL + rspArray[:, :, k-1]
                        newWEPLMax = np.max(newWEPL[newFluence_k>0])
                    else:
                        newWEPL = oldWEPL
                        newWEPLMax = oldWEPL

                    oldSigma2 = np.interp(oldWEPL, self._referenceIDDX, self._sigmaX*self._sigmaX)
                    newSigma2 = np.interp(newWEPLMax, self._referenceIDDX, self._sigmaX*self._sigmaX)

                    s2 = newSigma2 - oldSigma2
                    if s2 <= 0:
                        sigmaX_k = 0
                    else:
                        sigmaX_k = math.sqrt(s2)

                    oldSigma2 = np.interp(oldWEPL, self._referenceIDDX, self._sigmaY*self._sigmaY)
                    newSigma2 = np.interp(newWEPLMax, self._referenceIDDX, self._sigmaY*self._sigmaY)

                    s2 = newSigma2 - oldSigma2
                    if s2 <= 0:
                        sigmaY_k = 0
                    else:
                        sigmaY_k = math.sqrt(s2)

                    if sigmaX_k>0 or sigmaY_k>0:
                        logger.info((newWEPLMax, sigmaX_k, sigmaY_k))
                        newFluence_k_air = np.array(newFluence_k)
                        newFluence_k_air[rspArray[:, :, k-1]>=0.2] = 0
                        newFluence_k_notAir = np.array(newFluence_k)
                        newFluence_k_notAir[rspArray[:, :, k-1]<0.2] = 0
                        newFluence_k_notAir = gaussian_filter(newFluence_k_notAir, [sigmaX_k/doseImageBEV.spacing[0], sigmaY_k/doseImageBEV.spacing[1]], mode='constant', cval=0., truncate=4)
                        newFluence_k = newFluence_k_air + newFluence_k_notAir

                    nFIndices = np.indices(newFluence_k.shape)
                    newFluence_k = newFluence_k.flatten()
                    newWEPLInd = indexOfNearest(weplVec, newWEPL.flatten())
                    newFluenceWEPL[nFIndices[0].flatten(), nFIndices[1].flatten(), newWEPLInd] += newFluence_k

                fluenceWEPL = newFluenceWEPL
                for weplInd, wepl in enumerate(weplVec):
                    newFluence_k = fluenceWEPL[:, :, weplInd]

                    if np.sum(newFluence_k) <= 0:
                        continue

                    integDose = np.interp(wepl, self._referenceIDDX, self._referenceIDD)
                    doseImageArray[:, :, k] += newFluence_k * integDose * self.beamModel.computeMU2Protons(layer.nominalEnergy) / self.beamModel.computeMU2Protons(self.referenceEnergy)

        doseImageBEV.imageArray = doseImageArray

        return doseImageBEV

    def _fluence(self, layer, ctBEV, isocenterBEV):
        fluenceCalculator = FluenceCalculatorBEV()
        fluenceCalculator.beamModel = self.beamModel
        fluenceImage = fluenceCalculator.layerFluenceAtNozzle(layer, ctBEV, isocenterBEV)

        #fluenceImage.imageArray = gaussian_filter(fluenceImage.imageArray, [self._sigmaX[0], self._sigmaY[0]], mode='constant', cval=0.,
        #                                          truncate=4)

        return fluenceImage
    def _computeReferenceIDD(self):
        super()._computeReferenceIDD()

        self._computeReferenceSigmas()

    def _computeReferenceSigmas(self):
        refDoseBEV = imageTransformBEV.gantry0ToBEV(self._referenceDose)

        self._x = np.array(range(refDoseBEV.gridSize[0]))
        self._x0 = (refDoseBEV.gridSize[0]-1.)/2.

        sigmaX = 3*np.ones(refDoseBEV.gridSize[2])
        sigmaY = 3*np.ones(refDoseBEV.gridSize[2])

        for k in range(refDoseBEV.gridSize[2]):
            xVal = refDoseBEV.imageArray[:, int(np.round(self._x0)), k]
            xVal = xVal/np.sum(xVal)
            yVal = refDoseBEV.imageArray[int(np.round(self._x0)), :, k]
            yVal = yVal / np.sum(yVal)

            self._gaussVal = xVal
            res = fminbound(self.gaussMinFunc, 0, 20)
            sigmaX[k] = res

            self._gaussVal = yVal
            res = fminbound(self.gaussMinFunc, 0, 20)
            sigmaY[k] = res

        self._sigmaX = np.round(sigmaX*5)/5
        self._sigmaY = np.round(sigmaY*5)/5

    def gauss(self, x, sigma):
        x = self._x
        val =  np.exp(-(x - self._x0)**2 / (2*sigma**2)) / (sigma*math.sqrt(2*math.pi))
        return val

    def gaussMinFunc(self, sigma):
        x = self._x
        val = np.exp(-(x - self._x0) ** 2 / (2 * sigma ** 2)) / (sigma * math.sqrt(2 * math.pi))
        return np.sum((val-self._gaussVal)*(val-self._gaussVal))
