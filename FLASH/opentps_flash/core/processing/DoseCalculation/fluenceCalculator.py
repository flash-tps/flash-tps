from typing import Optional, Sequence

import numpy as np
from opentps.core.data.MCsquare import BDL
from opentps.core.data.images import Image3D, ROIMask, Image2D
from opentps.core.data.plan import PlanIonLayer, PlanIonBeam
from opentps.core.processing.imageProcessing import imageTransform3D
from scipy.ndimage import gaussian_filter


class FluenceCalculator:
    def __init__(self):
        self.beamModel:BDL = None

    def layerFluenceAtNozzle(self, layer:PlanIonLayer, ct:Image3D, beam:PlanIonBeam, roi:Optional[ROIMask]=None) -> Image2D:
        fluenceImage = self._pointSpreadFluenceAtNozzle(layer, ct, beam, roi)

        sigmaX, sigmaY = self.beamModel.spotSizes(layer.nominalEnergy)
        sigmaX /= fluenceImage.spacing[0]
        sigmaY /= fluenceImage.spacing[1]

        # Cannot explain this
        #sigmaX = sigmaX*(self.beamModel.smx-self.beamModel.nozzle_isocenter)/self.beamModel.smx
        #sigmaY = sigmaY*(self.beamModel.smy-self.beamModel.nozzle_isocenter)/self.beamModel.smy

        fluenceImage.imageArray = gaussian_filter(fluenceImage.imageArray, [sigmaX, sigmaY], mode='constant', cval=0., truncate=4)

        return fluenceImage

    def _pointSpreadFluenceAtNozzle(self, layer:PlanIonLayer, ct:Image3D, beam:PlanIonBeam, roi:Optional[ROIMask]) -> Image2D:
        ctBEV = imageTransform3D.dicomToIECGantry(ct, beam, fillValue=-1024., cropROI=roi, cropDim0=True, cropDim1=True, cropDim2=False)
        isocenterBEV = imageTransform3D.dicomCoordinate2iecGantry(beam, beam.isocenterPosition)

        return self.pointSpreadFluenceAtNozzleBEV(layer, ctBEV, isocenterBEV)

    def pointSpreadFluenceAtNozzleBEV(self, layer:PlanIonLayer, ctBEV:Image3D, isocenterBEV:Sequence[float]) -> Image2D:
        fluence = np.zeros((ctBEV.imageArray.shape[0], ctBEV.imageArray.shape[1]))

        for i, x in enumerate(layer.spotX):
            y = layer.spotY[i]
            spotWeight = layer.spotMUs[i]

            xAtNozzle = x*(self.beamModel.smx-self.beamModel.nozzle_isocenter)/self.beamModel.smx
            yAtNozzle = y*(self.beamModel.smy-self.beamModel.nozzle_isocenter)/self.beamModel.smy

            ind = ctBEV.getVoxelIndexFromPosition(((isocenterBEV[0] + xAtNozzle), (isocenterBEV[1] - yAtNozzle - ctBEV.spacing[1]), isocenterBEV[2]))
            fluence[ind[0], ind[1]] = spotWeight

        fluence / (ctBEV.spacing[0] * ctBEV.spacing[1])

        return Image2D(imageArray=fluence, spacing=ctBEV.spacing[:-1], origin=ctBEV.origin[:-1])

class FluenceCalculatorBEV(FluenceCalculator):
    def layerFluenceAtNozzle(self, layer:PlanIonLayer, ctBEV:Image3D, isocenterBEV:Sequence[float]) -> Image2D:
        fluenceImage = self.pointSpreadFluenceAtNozzleBEV(layer, ctBEV, isocenterBEV)

        sigmaX, sigmaY = self.beamModel.spotSizes(layer.nominalEnergy)
        sigmaX /= fluenceImage.spacing[0]
        sigmaY /= fluenceImage.spacing[1]

        # Cannot explain this
        #sigmaX = sigmaX*(self.beamModel.smx-self.beamModel.nozzle_isocenter)/self.beamModel.smx
        #sigmaY = sigmaY*(self.beamModel.smy-self.beamModel.nozzle_isocenter)/self.beamModel.smy

        fluenceImage.imageArray = gaussian_filter(fluenceImage.imageArray, [sigmaX, sigmaY], mode='constant', cval=0., truncate=4)

        return fluenceImage
