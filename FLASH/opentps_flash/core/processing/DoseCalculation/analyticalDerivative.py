
from typing import Optional

import numpy as np
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import CTImage, ROIMask, DoseImage, RSPImage
from scipy.interpolate import interpolate


class AnalyticalDerivative:
    def __init__(self, ctCalibration:AbstractCTCalibration):
        self.ctCalibration = ctCalibration

    def computeDerivative(self, ctBEV:CTImage, referenceDoseBEV:DoseImage, deltaR=1., roiBEV:Optional[ROIMask]=None) -> DoseImage:
        rsp = RSPImage.fromCT(ctBEV, self.ctCalibration)

        cumRSP = rsp.computeCumulativeWEPL(roi=roiBEV)
        cumRSPArray = cumRSP.imageArray

        doseImageBEV = DoseImage.fromImage3D(ctBEV, patient=None)
        doseImageArray = np.zeros(doseImageBEV.imageArray.shape)

        for i in range(doseImageBEV.imageArray.shape[0]):
            for j in range(doseImageBEV.imageArray.shape[1]):
                f = interpolate.interp1d(np.squeeze(cumRSPArray[i, j, :]), np.squeeze(referenceDoseBEV.imageArray[i, j, :]), kind='linear',
                                             fill_value='extrapolate', assume_sorted=True)
                doseImageArray[i, j, :] = f(np.squeeze(cumRSPArray[i, j, :]+deltaR))

        doseImageBEV.imageArray = (referenceDoseBEV.imageArray - doseImageArray)/deltaR

        return doseImageBEV
