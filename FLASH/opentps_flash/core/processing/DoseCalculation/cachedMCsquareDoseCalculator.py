import logging
from typing import Optional

from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.io import mcsquareIO
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator

logger = logging.getLogger(__name__)


class CachedMCsquareDoseCalculator(MCsquareDoseCalculator):
    _staticVars = {"_currentCalibration": None}

    @property
    def _currentCalibration(self) -> Optional[AbstractCTCalibration]:
        return self._staticVars["_currentCalibration"]

    @_currentCalibration.setter
    def _currentCalibration(self, ctCalibration:Optional[AbstractCTCalibration]):
        self._staticVars["_currentCalibration"] = ctCalibration

    def _writeFilesToSimuDir(self):
        if not (self._currentCalibration == self.ctCalibration):
            super()._writeFilesToSimuDir()
            self._currentCalibration = self.ctCalibration
            return
        else:
            logger.info("CT calibration not exported. Cached CT calibration will be used.")

        mcsquareIO.writeCT(self._ct, self._ctFilePath, self.overwriteOutsideROI)
        mcsquareIO.writePlan(self._plan, self._planFilePath, self._ct, self._beamModel)

        mcsquareIO.writeConfig(self._config, self._configFilePath)
        mcsquareIO.writeBin(self._mcsquareSimuDir)
