import logging
from typing import Optional

import numpy as np
from opentps.core.data.images import CTImage, ROIMask, DoseImage, RSPImage
from opentps.core.data.plan import PlanIonBeam
from opentps_flash.core.processing.DoseCalculation.analyticalNoScattering import AnalyticalNoScattering
from opentps_flash.core.processing.DoseCalculation.fluenceCalculator import FluenceCalculatorBEV
from scipy.interpolate import interpolate


logger = logging.getLogger(__name__)


class AnalyticalNoScatteringBEV(AnalyticalNoScattering):
    WEPL_THRESHOLD = 0.3

    def __init__(self):
        super().__init__()

        self.fluenceCalculator = FluenceCalculatorBEV()
        self.fluenceCalculator.beamModel = self.beamModel

    def _computeDoseForBeam(self, ctBEV:CTImage, beam:PlanIonBeam, roiBEV:Optional[ROIMask]) -> DoseImage:
        fluenceCalculator = self.fluenceCalculator
        fluenceCalculator.beamModel = self.beamModel

        rspImage = RSPImage.fromCT(ctBEV, self.ctCalibration)

        cumRSPArray = np.cumsum(rspImage.imageArray, axis=2) * rspImage.spacing[2]
        indexOfFirstSignificantWEPL = self._indexOfFirstSignificantWEPL(cumRSPArray)
        logger.info("Skipping " + str(indexOfFirstSignificantWEPL) + " empty layers in analytical simulation.")
        cumRSPArray = cumRSPArray[:, :, indexOfFirstSignificantWEPL:]

        doseImageBEV = DoseImage.fromImage3D(rspImage, patient=None)
        doseImageArray = np.zeros(doseImageBEV.imageArray.shape)

        wetBeforeCT = self._wetBeforeCT(beam)

        for layer in beam:
            fluence = fluenceCalculator.layerFluenceAtNozzle(layer, ctBEV, beam.isocenterPosition)
            fluence = fluence.imageArray

            layerDose, layerDeriv = self._doseOnReferenceIDDGrid(wetBeforeCT, layer.nominalEnergy)

            for i in range(wetBeforeCT.shape[0]):
                for j in range(wetBeforeCT.shape[1]):
                    f = interpolate.interp1d(self._referenceIDDX, np.squeeze(layerDose[i, j, :]), kind='linear',
                                             fill_value='extrapolate', assume_sorted=True)
                    doseImageArray[i, j, indexOfFirstSignificantWEPL:] += fluence[i, j] * f(np.squeeze(cumRSPArray[i, j, :]))

        doseImageBEV.imageArray = doseImageArray

        return doseImageBEV

    def _indexOfFirstSignificantWEPL(self, cumRSP:np):
        meanCumRSP = np.mean(cumRSP, axis=0)
        meanCumRSP = np.mean(meanCumRSP, axis=0)

        index = 0
        for i in range(len(meanCumRSP)):
            if meanCumRSP[i]>self.WEPL_THRESHOLD:
                index = i
                break

        return index