import copy

import numpy as np

from opentps.core.data.images import CTImage, DoseImage, ROIMask
from opentps.core.data.plan import RTPlan
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.data.ctWithCEM import CTBEVWithCEM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.Utils import imagePreprocessing


class CEMDoseCalculator(MCsquareDoseCalculator):
    def computeDose(self, ct:CTImage, plan:RTPlan) -> DoseImage:
        for i, beam in enumerate(plan):
            ctCEM = self._computeCTBEV(ct, beam)

            ct0 = imageTransformBEV.bevToGantry0(ctCEM)

            ct2 = imageTransform3D.iecGantryToDicom(ctCEM, beam, fillValue=-1024.)
            ct2.name = 'CT with CEM'
            ct2.patient = ct.patient

            doseImage0 = super().computeDose(ct0, self._planGantry0(beam, ctCEM))
            doseImageBEV = imageTransformBEV.gantry0ToBEV(doseImage0)
            doseImageRes = imageTransform3D.iecGantryToDicom(doseImageBEV, beam)

            if i==0:
                doseImage = doseImageRes
            else:
                doseImage.imageArray = doseImage.imageArray + doseImageRes.imageArray

        return doseImage

    def _computeCTBEV(self, ct:CTImage, beam:CEMBeam) -> CTBEVWithCEM:
        ctBEV = imageTransform3D.dicomToIECGantry(ct, beam, fillValue=-1024.)

        imagePreprocessing.padImageForCEM(ctBEV, beam, self.beamModel, inPlace=True)

        ctCEM = CTBEVWithCEM(ctBEV)
        ctCEM.update([beam.cem, beam.aperture, beam.flashRangeShifter], self.ctCalibration)

        return ctCEM

    def _planGantry0(self, beam:CEMBeam, ctCEM:CTBEVWithCEM) -> RTPlan:
        planGantry0 = RTPlan()

        beam = copy.deepcopy(beam)
        beam.gantryAngle = 0.
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(ctCEM, beam.isocenterPosition)

        planGantry0.appendBeam(beam)

        return planGantry0
