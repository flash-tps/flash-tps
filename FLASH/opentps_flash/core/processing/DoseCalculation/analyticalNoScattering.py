
from typing import Union, Tuple, Sequence, Optional

import numpy as np
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.MCsquare import BDL
from opentps.core.data.images import CTImage, ROIMask, DoseImage, RSPImage
from opentps.core.data.plan import RTPlan, PlanIonBeam, PlanIonLayer
from opentps.core.processing.doseCalculation.abstractDoseCalculator import AbstractDoseCalculator
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.processing.DoseCalculation.cachedMCsquareDoseCalculator import \
    CachedMCsquareDoseCalculator
from opentps_flash.core.processing.DoseCalculation.fluenceCalculator import FluenceCalculator
from scipy.interpolate import interpolate


class AnalyticalNoScattering(AbstractDoseCalculator):
    def __init__(self):
        super().__init__()

        self._beamModel = None
        self._ctCalibration = None
        self.referenceEnergy = 226 # MeV

        self._referenceIDDEnergy = None
        self._referenceIDDX = None
        self._referenceIDD = None
        self._referenceDose = None

    @property
    def beamModel(self) -> BDL:
        return self._beamModel

    @beamModel.setter
    def beamModel(self, bdl:BDL):
        self._beamModel = bdl

    @property
    def ctCalibration(self) -> AbstractCTCalibration:
        return self._ctCalibration

    @ctCalibration.setter
    def ctCalibration(self, calibration:AbstractCTCalibration):
        self._ctCalibration = calibration

    def computeDose(self, ct:CTImage, plan:RTPlan, roi:Optional[ROIMask]=None) -> DoseImage:
        outImage = DoseImage.fromImage3D(ct, patient=None)
        outImage.imageArray = np.zeros(outImage.imageArray.shape)

        for doseImage in self.computeDosePerBeam(ct, plan, roi):
            outImage.imageArray = outImage.imageArray + doseImage.imageArray

        return outImage

    def computeDosePerBeam(self, ct:CTImage, plan:RTPlan, roi:Optional[ROIMask]=None) -> Sequence[DoseImage]:
        self._computeReferenceIDD()

        doseImages = []
        for beam in plan:
            doseImageDicom = self._computeDoseForBeam(ct, beam, roi)
            doseImages.append(doseImageDicom)

        return doseImages

    def _computeDoseForBeam(self, ct:CTImage, beam:PlanIonBeam, roi:Optional[ROIMask]) -> DoseImage:
        fluenceCalculator = FluenceCalculator()
        fluenceCalculator.beamModel = self.beamModel

        rsp = RSPImage.fromCT(ct, self.ctCalibration)

        cumRSP = rsp.computeCumulativeWEPL(beam, roi=roi)
        cumRSP = imageTransform3D.dicomToIECGantry(cumRSP, beam, fillValue=0., cropROI=roi, cropDim0=True, cropDim1=True, cropDim2=False)

        doseImageBEV = DoseImage.fromImage3D(cumRSP, patient=None)
        doseImageArray = np.zeros(doseImageBEV.imageArray.shape)

        wetBeforeCT = self._wetBeforeCT(beam)

        for layer in beam:
            fluence = fluenceCalculator.layerFluenceAtNozzle(layer, ct, beam, roi)
            fluence = fluence.imageArray

            layerDose, layerDeriv = self._doseOnReferenceIDDGrid(wetBeforeCT, layer.nominalEnergy)

            for i in range(wetBeforeCT.shape[0]):
                for j in range(wetBeforeCT.shape[1]):
                    f = interpolate.interp1d(self._referenceIDDX, np.squeeze(layerDose[i, j, :]), kind='linear',
                                             fill_value='extrapolate', assume_sorted=True)
                    doseImageArray[i, j, :] += fluence[i, j] * f(np.squeeze(cumRSP.imageArray[i, j, :]))*cumRSP.spacing[2]

        doseImageBEV.imageArray = doseImageArray

        doseImageDicom = imageTransform3D.iecGantryToDicom(doseImageBEV, beam, fillValue=0)
        resampler3D.resampleImage3DOnImage3D(doseImageDicom, ct, inPlace=True, fillValue=0.)

        return doseImageDicom

    def _wetBeforeCT(self, beam:PlanIonBeam) -> Union[float, np.ndarray]:
        wet = 0.

        if not beam.rangeShifter is None:
            wet += beam.rangeShifter.WET

        if isinstance(beam, CEMBeam):
            wet += beam.cem.imageArray

        return wet

    def _doseOnReferenceIDDGrid(self, wetBeforeCT:np.ndarray, energy:float) -> Tuple[np.ndarray, np.ndarray]:
        wetBeforeCT = wetBeforeCT + energyToRangeMM(self.referenceEnergy) - energyToRangeMM(energy)

        layerDose = np.zeros((wetBeforeCT.shape[0], wetBeforeCT.shape[1], self._shiftReferenceIDD(0).shape[0]))
        layerDeriv = np.zeros(layerDose.shape)

        for i in range(wetBeforeCT.shape[0]):
            for j in range(wetBeforeCT.shape[1]):
                layerDose[i, j, :] = self._shiftReferenceIDD(wetBeforeCT[i, j])
                layerDeriv[i, j, :] = self._shiftDerivIDD(wetBeforeCT[i, j])

        layerDose = layerDose * self.beamModel.computeMU2Protons(energy) / self.beamModel.computeMU2Protons(self.referenceEnergy)
        layerDeriv = layerDeriv * self.beamModel.computeMU2Protons(energy) / self.beamModel.computeMU2Protons(self.referenceEnergy)

        return layerDose, layerDeriv

    def _computeReferenceIDD(self):
        if self._referenceIDDEnergy == self.referenceEnergy:
            return

        ctLength = round(energyToRangeMM(self.referenceEnergy)) + 10 # 10 is just amargin
        if not ctLength%2:
            ctLength += 1

        huWater = self.ctCalibration.convertRSP2HU(1.)
        data = huWater * np.ones((ctLength, ctLength, ctLength))
        spacing = (1., 1., 1.)
        origin = (0., 0., 0.)

        ct = CTImage(imageArray=data, spacing=spacing, origin=origin)

        mu = 1.
        plan = RTPlan()
        beam = PlanIonBeam()
        beam.gantryAngle = 0.
        beam.isocenterPosition = np.array((ctLength, ctLength, ctLength))/2. - 0.5
        layer = PlanIonLayer()
        layer.nominalEnergy = self.referenceEnergy
        layer.appendSpot(0, 0, mu)
        beam.appendLayer(layer)
        plan.appendBeam(beam)

        doseCalculator = CachedMCsquareDoseCalculator()
        doseCalculator.beamModel = self.beamModel
        doseCalculator.ctCalibration = self.ctCalibration
        doseCalculator.nbPrimaries = 5e6

        doseImage = doseCalculator.computeDose(ct, plan)
        self._referenceDose = doseImage

        refIDD = np.squeeze(np.sum(np.sum(doseImage.imageArray, 2), 0)) / mu

        # Useful for when we have to extrapolate:
        refIDD[0] = refIDD[1]
        refIDD[-1] = 0
        refIDD[-2] = 0

        self._referenceIDDX = np.array(range(refIDD.shape[0])) + 1.
        self._referenceIDD = refIDD

        depth10 = np.arange(0, np.max(self._referenceIDDX), 0.1) + 1.
        f = interpolate.interp1d(self._referenceIDDX, refIDD, kind='linear', fill_value='extrapolate', assume_sorted=True)
        ref10 = f(depth10)
        f = interpolate.interp1d(depth10[:-1], (ref10[1:]-ref10[:-1])/(depth10[1]-depth10[0]), kind='linear', fill_value='extrapolate', assume_sorted=True)
        derivIDD = f(self._referenceIDDX)

        self._referenceIDDEnergy = self.referenceEnergy
        self._referenceIDDFunction = interpolate.interp1d(self._referenceIDDX, refIDD, kind='linear', fill_value='extrapolate', assume_sorted=True)
        self._derivIDDFunction = interpolate.interp1d(self._referenceIDDX, derivIDD, kind='linear', fill_value='extrapolate', assume_sorted=True)

    def _shiftReferenceIDD(self, wet):
        return self._referenceIDDFunction(self._referenceIDDX+wet)

    def _shiftDerivIDD(self, wet):
        return self._derivIDDFunction(self._referenceIDDX+wet)