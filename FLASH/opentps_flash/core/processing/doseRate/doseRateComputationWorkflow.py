from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps.core.processing.planDeliverySimulation.simpleBeamDeliveryTimings import SimpleBeamDeliveryTimings
from opentps_flash.core.data.drvh import DRVH
from opentps_flash.core.processing.doseRate.conventionalDeliveryTimings import ConventionalDeliveryTimings
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps_flash.core.processing.doseRate.flashPulseDeliveryTimings import FlashPulseDeliveryTimings
from opentps_flash.core.processing.doseRate.doseAveragedDoseRateCalculator import DoseAveragedDoseRateCalculator
from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator
from opentps_flash.core.processing.doseRate.maxPercentileDoseRateCalculator import MaxPercentileDoseRateCalculator
from opentps_flash.core.processing.doseRate.maxPercentileDRPriorInterpolate import MaxPercentileDoseRateCalculatorNoInterpolation
from opentps_flash.core.processing.doseRate.pbsDoseRateCalculator import PBSDoseRateCalculator
from opentps_flash.core.utils.plots import plot_DVH


class DoseRateComputationWorkflow:
    def __init__(self, plan, ct, roi, bdl, ctCalibration, deliveryModel, doseRateCalculator, current=250e-9):
        self.current = current
        self.nbPrimaries = 1e5
        self.percentile = 0.95
        self.minNumberOfPulses = 3
        self.PBSdrThreshold = 0.01

        self.plan = plan
        self.ct = ct
        self.ROIs = roi if type(roi) is list else [roi]

        self.beamModel = bdl
        self.ctCalibration = ctCalibration
        self.timingModel = None

        self._deliveryModel = deliveryModel
        self._doseRateCalculator = doseRateCalculator


    @property
    def deliveryModel(self):
        return self._deliveryModel

    @deliveryModel.setter
    def deliveryModel(self, model):
        if type(model) is str:
            if model=="CONTINOUS_BEAM_FLASH":
                self._deliveryModel = FlashDeliveryTimings()
                self._deliveryModel.bdl = self.beamModel
                self._deliveryModel.current = self.current
            elif model=="PULSED_BEAM_FLASH":
                self._deliveryModel = FlashPulseDeliveryTimings()
                self._deliveryModel.bdl = self.beamModel
                self._deliveryModel.current = self.current
                self._deliveryModel.minNumberOfPulses = self.minNumberOfPulses
            elif model=="CONTINOUS_BEAM_CONV":
                if self.timingModel is None:
                    self.timingModel = SimpleBeamDeliveryTimings(self.plan) # only for continous beam
                self._deliveryModel = ConventionalDeliveryTimings(self.timingModel)
            elif model=="PULSED_BEAM_CONV":
                raise NotImplementedError(f"Model {model} not yet implemented. To be implemented in SimpleBeamDeliveryTimings")
            else:
                raise NotImplementedError(f"Model {model} not implemented. Choose between CONTINOUS_BEAM_FLASH, PULSED_BEAM_FLASH, CONTINUOUS_BEAM_CONV, PULSED_BEAM_CONV")
        else:
            self._deliveryModel = model

    @property
    def doseRateCalculator(self):
        return self._doseRateCalculator

    @doseRateCalculator.setter
    def doseRateCalculator(self, calculator):
        if type(calculator) is str:
            if self.deliveryModel is None: raise ValueError("You need to set the deliveryModel prior to the dose rate calculator.")
            if calculator=="DADR":
                self._doseRateCalculator = DoseAveragedDoseRateCalculator(self.deliveryModel)
            elif calculator=="PBSDR":
                self._doseRateCalculator = PBSDoseRateCalculator(self.deliveryModel)
                self._doseRateCalculator.threshold = self.PBSdrThreshold
            elif calculator=="PDR":
                self._doseRateCalculator = PercentileDoseRateCalculator(self.deliveryModel)
                self._doseRateCalculator.percentile = self.percentile
            elif calculator=="MPDR":
                self._doseRateCalculator = MaxPercentileDoseRateCalculator(self.deliveryModel)
                self._doseRateCalculator.percentile = self.percentile
            elif calculator=="MPDRNoInterpolation":
                self._doseRateCalculator = MaxPercentileDoseRateCalculatorNoInterpolation(self.deliveryModel)
                self._doseRateCalculator.percentile = self.percentile
            else:
                raise NotImplementedError(f"The type of dose rate {calculator} is not implemented. Choose between DADR, PBSDR, PDR, MPDR")
        else:
            self._doseRateCalculator = calculator

    def initializeDoseCalculator(self):
        mc2 = MCsquareDoseCalculator()
        mc2.beamModel = self.beamModel
        mc2.ctCalibration = self.ctCalibration
        mc2.nbPrimaries = self.nbPrimaries
        self.doseCalculator = mc2

    def computeBeamlets(self):
        self.beamlets = self.doseCalculator.computeBeamlets(self.ct, self.plan, roi=self.ROIs)

    def run(self):
        # Set models
        self.deliveryModel = self._deliveryModel
        self.doseRateCalculator = self._doseRateCalculator

        if not hasattr(self, 'doseCalculator'):
            self.initializeDoseCalculator()
            self.computeBeamlets()
        self.dr = self.doseRateCalculator.computeDoseRate(self.ct, self.plan, roi=self.ROIs, beamlets=self.beamlets)
        self.dr.name = 'Dose rate'
        return self.dr
    
    def computeDRVH(self, dose):
        self.DRVHs = []
        for roi in self.ROIs:
            drvh = DRVH(roiMask=roi, dose=dose, doseRate=self.dr, thresholdDose=1)
            self.DRVHs.append(drvh)
        return self.DRVHs
    
    def plotDRVH(self):
        plot_DVH(self.DRVHs, xlim=(0,100))



