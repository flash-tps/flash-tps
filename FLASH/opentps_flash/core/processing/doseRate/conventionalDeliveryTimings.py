import numpy as np

from opentps.core.data.plan import PlanIonLayer
from opentps.core.data.plan._rtPlan import RTPlan
from opentps.core.io import mcsquareIO
from opentps.core.data.MCsquare import BDL
from opentps_flash.core.processing.flashConfig import FLASHConfig
from opentps.core.processing.planDeliverySimulation.scanAlgoBeamDeliveryTimings import ScanAlgoBeamDeliveryTimings
from opentps.core.data.plan._planIonBeam import PlanIonBeam

class ConventionalDeliveryTimings:
    """
    Conventional Delivery Timings
    """

    def __init__(self, timingModel):
        """
        Parameters
        ----------
        timingModel: ScanAlgoBeamDeliveryTimings or simpleBeamDeliveryTimings
        """
        self.timingModel = timingModel

    def getSpotTimings(self, layer:PlanIonLayer, returnNewPlan=False):
        if not hasattr(self, 'plan_with_timings'):
            plan = RTPlan()
            plan.appendBeam(PlanIonBeam())
            plan.beams[0].appendLayer(layer.copy())
            self.timingModel.plan = plan
            self.initial_positions = plan.spotXY
            self.plan_with_timings = self.timingModel.getPBSTimings(sort_spots="true")
        irradiationTime = self.plan_with_timings[0][0].spotIrradiationDurations - 990e-6 # irradiation last 10us not 1ms
        startTime = self.plan_with_timings[0][0].spotTimings
        scanningTime = np.zeros(len(startTime))
        scanningTime[1:] = np.diff(startTime) - irradiationTime[:-1] # scanningTime[i] time to go from spot i-1 to spot i

        return startTime, irradiationTime, scanningTime
    
    def getNewSpotIndexesFromInitialSpotIndexes(self, plan:RTPlan):
        """
        Returns
        -------
        Indexes of initial spots from the new spot sequence

        Example
        -------
        plan.spotXY == [[1,2],[3,4],[5,6]]
        self.plan_with_timings.spotXY == [[1,2],[3,4],[5,6],[1,2],[5,6]]
        Indexes == [0,1,2,0,2]
        """
        assert hasattr(self, 'plan_with_timings')
        exist, where = plan[0][0].spotDefinedInXY(self.plan_with_timings[0][0].spotX, self.plan_with_timings[0][0].spotY)
        assert np.all(exist)==True
        return where



