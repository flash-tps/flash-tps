import numpy as np

from opentps.core.data.plan import PlanIonLayer
from opentps.core.data.plan._rtPlan import RTPlan
from opentps.core.io import mcsquareIO
from opentps.core.data.MCsquare import BDL
from opentps_flash.core.processing.flashConfig import FLASHConfig
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings

class FlashPulseDeliveryTimings(FlashDeliveryTimings):
    """
    Flash POne Delivery Timings
    """
    def __init__(self):
        super().__init__()
        self.current = 150e-9 # max current (can vary below)
        self.maxDistanceBetweenSpot: float = 8. # mm
        self.minNumberOfPulses = 4
        self.pulseIrradiationDuration = 10e-6 # 10us
        self.pulseTime = 1e-3 # 1ms

    def getSpotTimings(self, layer:PlanIonLayer):
        mu2protons = self.bdl.computeMU2Protons(layer.nominalEnergy)
        charges = layer.spotMUs * mu2protons * 1.602176634e-19
        irradiationTimeAtMaxCurrent = charges / self.current
        numberOfPulsesNeeded = np.ceil(irradiationTimeAtMaxCurrent * 1000)
        numberOfPulsesNeeded = np.maximum(self.minNumberOfPulses, numberOfPulsesNeeded)
        irradiationTime = (numberOfPulsesNeeded - 1) * self.pulseTime + self.pulseIrradiationDuration # since irradiation time of last pulse is 10us

        x = layer.spotX
        y = layer.spotY

        spotDist = np.maximum(np.abs(np.diff(x)), np.abs(np.diff(y)))
        epsilon_distance = 0.01 # 0.01mm to avoid numerical issues 
        scanningTime = np.ceil((spotDist - epsilon_distance) / self.maxDistanceBetweenSpot) * self.pulseTime
        scanningTime -= self.pulseIrradiationDuration # correction of the scanning time to account for 990us of iddle after irradiation
        scanningTime = np.concatenate(
            (np.array([0]), scanningTime))  # scanningTime[i] time to go from spot i-1 to spot i

        startTime = np.cumsum(scanningTime) + np.cumsum(irradiationTime) - irradiationTime

        return startTime, irradiationTime, scanningTime
