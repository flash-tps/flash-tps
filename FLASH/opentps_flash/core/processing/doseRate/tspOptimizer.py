from math import sin, cos, pi

import numpy as np
from python_tsp.heuristics import solve_tsp_simulated_annealing, solve_tsp_local_search
from opentps.core.data.images import ROIMask
from opentps.core.data.plan import PlanIonLayer
from opentps_flash.core.data.doseRate import InstantaneousDose
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps.core.data.images._doseImage import DoseImage
from opentps.core.processing.imageProcessing import resampler3D


def _scanAlgoOrder(layer, angle=0):
    x = layer.spotX
    y = layer.spotY

    rotMat = np.array([[cos(angle), -sin(angle)], [sin(angle), cos(angle)]])
    oldCoord = np.row_stack((np.array(x).ravel(), np.array(y).ravel()))
    newCoord = rotMat@oldCoord
    x = np.around(newCoord[0, :], 2)
    y = np.around(newCoord[1, :], 2)

    coord = np.column_stack((x, y)).astype(float)
    order = np.argsort(coord.view('f8,f8'), order=['f1', 'f0'],
                       axis=0).ravel()  # sort according to y then x
    coord = coord[order]
    _, ind_unique = np.unique(coord[:, 1], return_index=True)  # unique y's
    n_unique = len(ind_unique)
    if n_unique > 1:
        for i in range(1, n_unique):
            if i == n_unique - 1:
                ind_last_x_at_current_y = coord.shape[0]
            else:
                ind_last_x_at_current_y = ind_unique[i + 1] - 1
            if ind_unique[i] == ind_last_x_at_current_y:  # only 1 spot for current y coord
                continue

            coord_last_x_at_current_y = coord[ind_last_x_at_current_y - 1, 0]
            ind_previous = ind_unique[i] - 1
            coord_previous = coord[ind_previous, 0]
            ind_first_x_at_current_y = ind_unique[i]
            coord_first_x_at_current_y = coord[ind_first_x_at_current_y, 0]

            # Check closest point to coord_previous
            if np.abs(coord_previous - coord_first_x_at_current_y) > np.abs(
                    coord_previous - coord_last_x_at_current_y):
                # Need to inverse the order of the spot irradiated for those coordinates:
                order[ind_first_x_at_current_y:ind_last_x_at_current_y + 1] = order[
                                                                              ind_first_x_at_current_y:ind_last_x_at_current_y + 1][
                                                                              ::-1]
                coord[ind_first_x_at_current_y:ind_last_x_at_current_y + 1] = coord[
                                                                              ind_first_x_at_current_y:ind_last_x_at_current_y + 1][
                                                                              ::-1]
    return order

class TSPOptimizer():
    def __init__(self, model:FlashDeliveryTimings):
        self.deliveryModel = model
        self.doseThreshold = 1
        self.percentile = 0.95
        self.testAllStartingSpot = True

    def getOptimalOrdering(self, layer:PlanIonLayer, dosePerSpot:InstantaneousDose, roi:ROIMask):
        nSpots = dosePerSpot.imageArray.shape[1]
        fullDosePerVoxel = np.array(np.sum(dosePerSpot.imageArray, axis=1))

        if len(fullDosePerVoxel) != np.product(roi.gridSize):
            # Need to resample ROI on dosePerSpot
            totalDose = np.reshape(fullDosePerVoxel, dosePerSpot.doseGridSize, order='F')
            totalDose = np.flip(totalDose, 0)
            totalDose = np.flip(totalDose, 1)
            totalDose = DoseImage(imageArray=totalDose, origin=dosePerSpot.origin, spacing=dosePerSpot.spacing,
                              angles=dosePerSpot.angles)
            roi = resampler3D.resampleImage3DOnImage3D(roi, totalDose , inPlace=False, fillValue=0.)
            del totalDose

        voxelsToConsider = fullDosePerVoxel > self.doseThreshold
        voxelsToConsider = np.logical_and(voxelsToConsider.ravel(), roi.imageArray.flatten())

        dosePerSpot = dosePerSpot.imageArray
        dosePerSpot = dosePerSpot[voxelsToConsider, :]

        nVoxels = dosePerSpot.shape[0]

        originalSequence = np.arange(0, nSpots)
        orderScan = _scanAlgoOrder(layer)
        sumPerSpot = np.sum(dosePerSpot, axis=0).ravel()
        dosePerSpot[dosePerSpot< self.doseThreshold * (1 - self.percentile)] = 0 # TEST!!!!
        spotsToConsider = sumPerSpot >= self.doseThreshold * (1 - self.percentile) * nVoxels * (1 - self.percentile) # * self.doseThreshold
        spotToOptimize = originalSequence[np.array(spotsToConsider).ravel()]  # We might want to use another doseThreshold for that

        transitionMatrix = self._transitionMatrix(spotToOptimize, dosePerSpot, layer)
        permutation, distance = solve_tsp_simulated_annealing(transitionMatrix, alpha=0.95, verbose=True)
        permutation, distance = solve_tsp_local_search(transitionMatrix, x0=permutation, perturbation_scheme="ps3", verbose=True)
        permutation = np.array(permutation)[1:]-1 # We must remove 1 because transitionMatrix has a fictional spot at pos 0

        spotNotToOptimize = orderScan.tolist()
        for s in spotToOptimize:
            spotNotToOptimize.remove(s)

        permutation = spotToOptimize[permutation.ravel()]
        if (np.array(spotNotToOptimize).size>0):
            permutation = np.concatenate((permutation, np.array(spotNotToOptimize).ravel()))

        return permutation

    def _transitionMatrix(self, spotToOptimize, dosePerSpot, layer):
        _, irradiationTime, _ = self.deliveryModel.getSpotTimings(layer)

        x = np.array(layer.spotX)
        y = np.array(layer.spotY)

        nSpots = len(spotToOptimize)
        nVoxels = dosePerSpot.shape[0]

        transitionMatrix = np.zeros((nSpots+1, nSpots+1))
        for spot1Ind in range(nSpots):
            spot1 = spotToOptimize[spot1Ind]

            w1 = np.sum(dosePerSpot[:, spot1].todense().ravel()>0)/nVoxels
            if w1 <= 0:
                w1 = 1.
            transitionMatrix[0, spot1Ind+1] = (irradiationTime[spot1])/w1

            for spot2Ind in np.arange(spot1Ind + 1, nSpots):
                spot2 = spotToOptimize[spot2Ind]
                w2 = np.sum(dosePerSpot[:, spot2].todense().ravel()>0)/nVoxels

                spotDist = np.sqrt((x[spot1] - x[spot2]) ** 2 + (y[spot1] - y[spot2]) ** 2)
                scanningTime = spotDist / self.deliveryModel.scanningSpeed

                if w2 <= 0:
                    w2 = 1.

                transitionMatrix[spot1Ind+1, spot2Ind+1] = (scanningTime + irradiationTime[spot2]) / w2
                transitionMatrix[spot2Ind+1, spot1Ind+1] = (scanningTime + irradiationTime[spot1]) / w1

        return transitionMatrix
