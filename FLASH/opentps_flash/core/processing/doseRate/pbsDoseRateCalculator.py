import logging

import numpy as np

from opentps.core.data import SparseBeamlets
from opentps.core.data.images import Image3D
from opentps.core.data.plan import RTPlan
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings

from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator


logger = logging.getLogger(__name__)


class PBSDoseRateCalculator(PercentileDoseRateCalculator):
    """
    Varian dose rate
    """
    def __init__(self, deliveryModel, threshold=0.01):
        super().__init__(deliveryModel)
        self.threshold = threshold

    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets) -> Image3D:
        roi = self._getDoseInfluenceROI(doseInfluence, plan.spotMUs) # 3D Dose image with 1s at ROI
        dosePerSpot = self._getFullDosePerSpot(doseInfluence, plan, roi) # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number

        dr, _, _, _, _ = self.computeDoseRateFromDosePerSpot(plan[0][0], dosePerSpot)

        outImage = Image3D.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage

    def computeDoseRateFromDosePerSpot(self, layer, dosePerSpot):
        startTime, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(layer)

        fullDose = np.sum(dosePerSpot, axis=1)  # cumulated dose at the end of the delivery
        fullDose = np.reshape(fullDose, (len(fullDose), 1))  # size ROI voxel, 1

        totalTime = np.cumsum(irradiationTime) + np.cumsum(scanningTime)

        cumDose = np.cumsum(dosePerSpot, axis=1)  # size ROI voxel * spots number
        # percDose = cumDose / fullDose # size ROI voxel * spots number
        # p = (1 - self.percentile) / 2
        nVoxels = cumDose.shape[0]
        startTimeIndex = np.argmax(cumDose >= self.threshold, axis=1)
        endTimeIndex = np.argmax(cumDose >= fullDose - self.threshold, axis=1)

        # for each voxel, interpolate
        startTime = totalTime[startTimeIndex - 1] + scanningTime[startTimeIndex] + irradiationTime[startTimeIndex] / \
                    (cumDose[range(nVoxels), startTimeIndex.reshape(-1)] - cumDose[
                        range(nVoxels), startTimeIndex.reshape(-1) - 1]) * \
                    (self.threshold - cumDose[range(nVoxels), startTimeIndex.reshape(-1) - 1])

        startDose = cumDose[range(nVoxels), startTimeIndex - 1] + (cumDose[range(nVoxels), startTimeIndex] - cumDose[range(nVoxels), startTimeIndex - 1]) / \
                    (cumDose[range(nVoxels), startTimeIndex.reshape(-1)] - cumDose[
                        range(nVoxels), startTimeIndex.reshape(-1) - 1]) * \
                    (self.threshold - cumDose[range(nVoxels), startTimeIndex.reshape(-1) - 1])

        # Correct start time of the voxel where at the first spot, percDose > p
        startTime[startTimeIndex == 0] = totalTime[0]

        stopTime = totalTime[endTimeIndex - 1] + scanningTime[endTimeIndex] + irradiationTime[endTimeIndex] / \
                   (cumDose[range(nVoxels), endTimeIndex.reshape(-1)] - cumDose[
                       range(nVoxels), endTimeIndex.reshape(-1) - 1]) * \
                   (fullDose.reshape(-1) - self.threshold - cumDose[range(nVoxels), endTimeIndex.reshape(-1) - 1])

        stopDose = cumDose[range(nVoxels), endTimeIndex - 1] + (cumDose[range(nVoxels), endTimeIndex] - cumDose[range(nVoxels), endTimeIndex - 1]) / \
                   (cumDose[range(nVoxels), endTimeIndex.reshape(-1)] - cumDose[
                       range(nVoxels), endTimeIndex.reshape(-1) - 1]) * \
                   (fullDose.reshape(-1) - self.threshold - cumDose[range(nVoxels), endTimeIndex.reshape(-1) - 1])

        spotTiming = stopTime - startTime
        spotTiming = spotTiming.reshape(-1, 1)

        tnonzero = np.flatnonzero(spotTiming > 0)

        dr = np.zeros((nVoxels, 1))
        dr[tnonzero] = (fullDose[tnonzero] - 2 * self.threshold) / spotTiming[tnonzero]

        return dr, startTime, stopTime, startDose, stopDose