import numpy as np
import scipy.sparse as sp
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps_flash.core.processing.doseRate.abstractDoseRateCalculator import AbstractDoseRateCalculator
from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator
from opentps.core.data.plan._rtPlan import RTPlan
from opentps.core.data import SparseBeamlets
from opentps.core.data.images import Image3D, ROIMask

try:
    import sparse_dot_mkl
    use_MKL = 0
except:
    use_MKL = 0

class DoseAveragedDoseRateCalculator(PercentileDoseRateCalculator):
    def __init__(self, deliveryModel):
        super().__init__(deliveryModel)

    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets):
        startTime, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(plan[0][0])

        roi = self._getDoseInfluenceROI(doseInfluence, plan.spotMUs) # 3D Dose image with 1s at ROI
        dosePerSpot = self._getFullDosePerSpot(doseInfluence, plan, roi) # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number

        invIrradiationTime = np.zeros_like(irradiationTime)
        nonzero = irradiationTime > 0
        invIrradiationTime[nonzero] = 1/irradiationTime[nonzero]
        doseRatePerSpot = dosePerSpot * sp.diags(invIrradiationTime.astype(np.float32)) # size ROI voxel * spots number

        # doseRatePerSpot = sp.diags(1/irradiationTime.astype(np.float32), format='csc') * dosePerSpot.transpose()
        # doseRatePerSpot = doseRatePerSpot.transpose()

        dadr = np.asarray(np.sum(sp.csc_matrix.multiply(dosePerSpot, doseRatePerSpot), axis=1) / np.sum(dosePerSpot, axis=1))

        outImage = Image3D.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dadr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage


    def _getFullDosePerSpot(self, doseInfluence:SparseBeamlets, plan:RTPlan, roi:ROIMask) -> np.ndarray:
        doseInfluenceMat = doseInfluence.toSparseMatrix()

        roiArray = roi.imageArray

        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        if np.count_nonzero(roiArray)<=0:
            raise Exception("Dose matrix is empty")

        layer = plan[0][0]
        weights = layer.spotMUs

        dosePerSpot = doseInfluenceMat[roiArray.astype(bool),:]

        if use_MKL == 1:
            dosePerSpot = sparse_dot_mkl.dot_product_mkl(
                dosePerSpot, sp.diags(weights.astype(np.float32), format='csc'))
        else:
            dosePerSpot = sp.csc_matrix.dot(
                dosePerSpot, sp.diags(weights.astype(np.float32), format='csc'))

        return dosePerSpot