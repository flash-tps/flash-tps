import logging

import numpy as np
from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator
import scipy
import scipy.signal as signal
from opentps.core.data import SparseBeamlets
from opentps.core.data.images import CTImage, Image3D, ROIMask, DoseImage
from opentps.core.processing.doseCalculation.abstractDoseInfluenceCalculator import AbstractDoseInfluenceCalculator
from opentps.core.data.plan import RTPlan


import scipy.sparse as sp


try:
    import sparse_dot_mkl
    use_MKL = 1
except:
    use_MKL = 0


logger = logging.getLogger(__name__)


class MaxPercentileDoseRateCalculatorNoInterpolation(PercentileDoseRateCalculator):
    def __init__(self, deliveryModel):
        super().__init__(deliveryModel)
        self.windowWithStep = 1

    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets) -> Image3D:
        roi = self._getDoseInfluenceROI(doseInfluence, plan.spotMUs)  # 3D Dose image with 1s at ROI

        startTimes, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(plan[0][0])
        if hasattr(self.deliveryModel, 'plan_with_timings'):
            index_spots = self.deliveryModel.getNewSpotIndexesFromInitialSpotIndexes(plan)
            planToCompute = self.deliveryModel.plan_with_timings
        else:
            index_spots = None
            planToCompute = plan
        dosePerSpot = self._getFullDosePerSpot(doseInfluence, planToCompute,
                                               roi, index_spots)  # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number

        dr = self.computeDoseRateFromDosePerSpot(startTimes, irradiationTime, scanningTime, dosePerSpot)

        outImage = DoseImage.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage


    def computeDoseRateFromDosePerSpot(self, startTimes, irradiationTime, scanningTime, dosePerSpot) -> Image3D:
        spotNb = dosePerSpot.shape[1]
        fullDose = np.sum(dosePerSpot, axis=1) # cumulated dose at the end of the delivery
        fullDose = np.reshape(fullDose, (len(fullDose), 1)) # size ROI voxel, 1

        dr = np.zeros((dosePerSpot.shape[0], 1))

        for windowWidth in np.arange(spotNb, 0, -self.windowWithStep):
            filt = np.ones(windowWidth)

            number_of_windows = len(irradiationTime) - windowWidth + 1
            totalTime = signal.convolve(irradiationTime+scanningTime, filt, mode='valid')\
                        - scanningTime[:number_of_windows] # per starting spot (when starting at spot 1, when starting at spot 2, etc.); size windowWidth # TotalTime[i] define as total time from spot i to spot i+windowWidth

            doseInIntervals = signal.convolve(dosePerSpot, filt.reshape(1,-1), mode='valid')

            doseRate = doseInIntervals / np.reshape(totalTime, (1, len(totalTime))) # size ROI voxel * windowwidth

            dosePerc = doseInIntervals < (self.percentile * fullDose) # size ROI voxel * windowwidth

            doseRate[dosePerc] = 0 # dose rate not defined if self.percentile of total dose not received in that voxel

            maxDoseRate = np.max(doseRate, axis=1) # max across starting spot
            maxDoseRate = np.reshape(maxDoseRate, (len(maxDoseRate), 1)) # size ROI voxel * 1

            cond = dr < maxDoseRate
            dr[cond] = maxDoseRate[cond] # if current dose rate is higher --> replace in dr

            logger.info((windowWidth, np.sum(np.logical_not(dosePerc))))
            #If all dose intervals are smaller than self.percentile% of total dose there is no need to further decrease the interval
            if np.sum(np.logical_not(dosePerc).astype(int)) == 0:
                break

        return dr

        outImage = Image3D.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage


if __name__=="__main__":
    from scipy import signal
    arr = np.array([[1, 2, 3, 4, 5], [2, 3, 4, 5, 6]])

    windowWidth = 2
    filt = np.ones(windowWidth)
    n = np.max([arr.shape[1], len(filt)]) + 1
    doseInIntervals = np.abs(np.fft.ifft(np.fft.fft(arr, n=n) * np.fft.fft(filt, n=n))).astype(int)

    doseInIntervals = doseInIntervals[:, 1:]
    print(doseInIntervals)

    filt2D = np.zeros((3, windowWidth + 2))
    filt2D[1, 1:1 + windowWidth] = 1

    doseInIntervals = scipy.ndimage.convolve(arr, filt2D, mode='constant', cval=0)

    print(doseInIntervals)