import logging
import numpy as np
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings

logger = logging.getLogger(__name__)


class AbstractDoseRateCalculator:
    def __init__(self, deliveryModel=None):
        self.deliveryModel = deliveryModel
