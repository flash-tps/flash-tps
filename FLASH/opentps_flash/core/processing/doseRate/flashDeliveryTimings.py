import numpy as np

from opentps.core.data.plan import PlanIonLayer
from opentps.core.data.plan._rtPlan import RTPlan
from opentps.core.io import mcsquareIO
from opentps.core.data.MCsquare import BDL
from opentps_flash.core.processing.flashConfig import FLASHConfig

class FlashDeliveryTimings:
    """
    Flash Delivery Timings
    """
    def __init__(self):
        self.scanningSpeed: float = 8000. # mm/s
        self.bdl: BDL = mcsquareIO.readBDL(FLASHConfig().bdlFile)
        self.current: float = 350e-9

    def getSpotTimings(self, layer:PlanIonLayer):
        mu2protons = self.bdl.computeMU2Protons(layer.nominalEnergy)
        charges = layer.spotMUs * mu2protons * 1.602176634e-19

        irradiationTime = np.maximum(250e-6, charges / self.current)  # per spot, cannot go down 250us
        x = layer.spotX
        y = layer.spotY

        spotDist = np.sqrt(np.diff(x) * np.diff(x) + np.diff(y) * np.diff(y))
        scanningTime = spotDist / self.scanningSpeed
        scanningTime = np.concatenate(
            (np.array([0]), scanningTime))  # scanningTime[i] time to go from spot i-1 to spot i

        startTime = np.cumsum(scanningTime) + np.cumsum(irradiationTime) - irradiationTime

        return startTime, irradiationTime, scanningTime



    def setPBSTimings(self, plan:RTPlan, sort_spots=True):
        """
        Add timings for each spot in the plan:
        OUTPUT:
            RTPlan with timings
        """

        if np.any(plan.spotMUs<0.01):
            print("Warning: Plan contains spots MU < 0.01 --> Delivery timings might not be accurate.")

        if sort_spots:
            plan.reorderPlan()

        startTime, irradiationDuration, _ = self.getSpotTimings(plan[0][0])

        plan[0][0]._irradiationDuration = irradiationDuration
        plan[0][0]._startTime = startTime
