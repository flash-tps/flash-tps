import logging

import numpy as np
from opentps.core.data import SparseBeamlets
from opentps.core.data.MCsquare import BDL
from opentps.core.data.images import CTImage, Image3D, ROIMask
from opentps.core.data.plan import RTPlan, PlanIonLayer
from opentps.core.processing.doseCalculation.abstractDoseInfluenceCalculator import AbstractDoseInfluenceCalculator
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps_flash.core.processing.doseRate.abstractDoseRateCalculator import AbstractDoseRateCalculator

import scipy.sparse as sp

from opentps_flash.core.data.doseRate import InstantaneousDoseRate

try:
    import sparse_dot_mkl
    use_MKL = 0
except:
    use_MKL = 0


logger = logging.getLogger(__name__)


class InstantaneousDoseRateCalculator(AbstractDoseRateCalculator):
    DOSE_THRESHOLD = 1.

    def __init__(self, deliveryModel):
        super().__init__(deliveryModel)
        self.doseCalculator:AbstractDoseInfluenceCalculator = None


    def computeDoseRate(self, ct:CTImage, plan:RTPlan, roi=None, beamlets=None):
        if beamlets is None:
            beamlets = self.doseCalculator.computeBeamlets(ct, plan, roi=roi)

        return self.computeDoseRateFromDoseInfluence(plan, beamlets)


    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets):
        startTime, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(plan[0][0])

        dosePerSpot = self._getFullDosePerSpot(doseInfluence, plan) # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number
        doseRatePerSpot = dosePerSpot

        dr = InstantaneousDoseRate()
        doseRatePerSpot = sp.diags(1/irradiationTime.astype(np.float32), format='csc') * doseRatePerSpot.transpose()
        doseRatePerSpot = doseRatePerSpot.transpose()
        dr.origin = doseInfluence.doseOrigin
        dr.spacing = doseInfluence.doseSpacing
        dr.doseGridSize = doseInfluence.doseGridSize
        dr.imageArray = doseRatePerSpot
        dr.irradiationTime = irradiationTime
        dr.startTime = startTime

        return dr

    def _getFullDosePerSpot(self, doseInfluence:SparseBeamlets, plan:RTPlan) -> np.ndarray:
        doseInfluenceMat = doseInfluence.toSparseMatrix()

        indArray = np.ravel_multi_index(np.nonzero(np.ones(doseInfluence.doseGridSize)), dims=doseInfluence.doseGridSize)
        indArray = np.reshape(indArray, (doseInfluence.doseGridSize[0], doseInfluence.doseGridSize[1], doseInfluence.doseGridSize[2]))
        indArray = np.flip(indArray, 1)
        indArray = np.flip(indArray, 0)
        indArray = indArray.flatten(order='F')
        indArray = np.argsort(indArray)

        layer = plan[0][0]
        weights = layer.spotMUs

        dosePerSpot = doseInfluenceMat

        if use_MKL == 1:
            dosePerSpot = sparse_dot_mkl.dot_product_mkl(
                dosePerSpot, sp.diags(weights.astype(np.float32), format='csc'))
        else:
            dosePerSpot = sp.csc_matrix.dot(
                dosePerSpot, sp.diags(weights.astype(np.float32), format='csc'))

        return dosePerSpot[indArray, :]
