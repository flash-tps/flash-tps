import logging

import numpy as np
import scipy.signal as signal

from opentps.core.data import SparseBeamlets
from opentps.core.data.images import Image3D, DoseImage
from opentps.core.data.plan import RTPlan

from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator


logger = logging.getLogger(__name__)


class MaxPercentileDoseRateCalculator(PercentileDoseRateCalculator):
    def __init__(self, deliveryModel):
        super().__init__(deliveryModel)
        self.windowWithStep = 1

    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets) -> Image3D:
        roi = self._getDoseInfluenceROI(doseInfluence, plan.spotMUs)  # 3D Dose image with 1s at ROI

        startTimes, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(plan[0][0])
        if hasattr(self.deliveryModel, 'plan_with_timings'):
            index_spots = self.deliveryModel.getNewSpotIndexesFromInitialSpotIndexes(plan)
            planToCompute = self.deliveryModel.plan_with_timings
        else:
            index_spots = None
            planToCompute = plan
        dosePerSpot = self._getFullDosePerSpot(doseInfluence, planToCompute,
                                               roi, index_spots)  # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number

        dr, _, _, _, _ = self.computeDoseRateFromDosePerSpot(startTimes, irradiationTime, scanningTime, dosePerSpot)

        outImage = DoseImage.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage

    def computeDoseRateFromDosePerSpot(self, startTimes, irradiationTime, scanningTime, dosePerSpot):
        # startTimes, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(layer)
        spotNb = dosePerSpot.shape[1]
        fullDose = np.sum(dosePerSpot, axis=1)  # cumulated dose at the end of the delivery
        fullDose = np.reshape(fullDose, (len(fullDose), 1))  # size ROI voxel, 1

        cumDose = np.cumsum(dosePerSpot, axis=1)  # size ROI voxel * spots number
        nVoxels = cumDose.shape[0]

        dr = np.zeros((dosePerSpot.shape[0], 1))
        bestStartingIndex = np.zeros((dosePerSpot.shape[0], 1))
        bestWW = np.zeros((dosePerSpot.shape[0], 1))
        bestDoseInInterval = np.zeros((dosePerSpot.shape[0], 1))

        for windowWidth in np.arange(spotNb, 0, -self.windowWithStep):
            filt = np.ones(windowWidth)

            totalTime = signal.convolve(irradiationTime + scanningTime, filt, mode='valid')
            totalTime -= scanningTime[:len(totalTime)]

            doseInIntervals = signal.convolve(dosePerSpot, filt.reshape(1, -1), mode='valid')

            doseRate = doseInIntervals / np.reshape(totalTime, (1, len(totalTime)))  # size ROI voxel * windowwidth

            dosePerc = doseInIntervals < (self.percentile * fullDose)  # size ROI voxel * windowwidth

            doseRate[dosePerc] = 0  # dose rate not defined if self.percentile of total dose not received in that voxel

            maxDoseRate = np.max(doseRate, axis=1)  # max across starting spot
            minDose = doseInIntervals[range(nVoxels), np.argmax(doseRate, axis=1)]
            maxDoseRate = np.reshape(maxDoseRate, (len(maxDoseRate), 1))  # size ROI voxel * 1

            cond = dr < maxDoseRate
            dr[cond] = maxDoseRate[cond]  # if current dose rate is higher --> replace in dr
            bestStartingIndex[cond] = np.argmax(doseRate, axis=1)[cond.ravel()]
            bestWW[cond] = windowWidth
            bestDoseInInterval[cond] = minDose[cond.ravel()]

            logger.info((windowWidth, np.sum(np.logical_not(dosePerc))))
            # If all dose intervals are smaller than self.percentile% of total dose there is no need to further decrease the interval
            if np.sum(np.logical_not(dosePerc).astype(int)) == 0:
                break

        bestStartingIndex = bestStartingIndex.ravel().astype(int)
        bestWW = bestWW.ravel().astype(int)

        if bestWW.size==1:
            bestStartingIndex = bestStartingIndex[0]
            bestWW = bestWW[0]

        startTime1 = startTimes[bestStartingIndex]
        startDose1 = cumDose[range(nVoxels), bestStartingIndex - 1]
        stopTime2 = startTimes[bestStartingIndex + bestWW - 1] + irradiationTime[bestStartingIndex + bestWW - 1]
        stopDose2 = cumDose[range(nVoxels), bestStartingIndex + bestWW-1]

        deltaDose = bestDoseInInterval - self.percentile * fullDose

        fact = deltaDose.ravel() / (cumDose[range(nVoxels), bestStartingIndex.reshape(-1)] - cumDose[range(nVoxels), bestStartingIndex.reshape(-1) - 1])
        fact[fact<0] = 0
        startTime2 = startTimes[bestStartingIndex] + irradiationTime[bestStartingIndex] * fact
        startDose2 = cumDose[range(nVoxels), bestStartingIndex - 1] + (cumDose[range(nVoxels), bestStartingIndex] - cumDose[range(nVoxels), bestStartingIndex - 1])*fact

        fact = 1 - deltaDose.ravel() / (cumDose[range(nVoxels), (bestStartingIndex+bestWW-1).reshape(-1)] - cumDose[range(nVoxels), (bestStartingIndex+bestWW-2).reshape(-1)])
        fact[fact < 0] = 0
        stopTime1 = startTimes[bestStartingIndex + bestWW - 1] + irradiationTime[bestStartingIndex + bestWW - 1] * fact
        stopDose1 = cumDose[range(nVoxels), bestStartingIndex+ bestWW - 2] + (cumDose[range(nVoxels), bestStartingIndex-1] - cumDose[range(nVoxels), bestStartingIndex - 2]) * fact

        startTime1 = np.reshape(startTime1, (nVoxels, 1))
        stopTime1 = np.reshape(stopTime1, (nVoxels, 1))
        stopTime2 = np.reshape(stopTime2, (nVoxels, 1))
        startTime2 = np.reshape(startTime2, (nVoxels, 1))
        startDose1 = np.reshape(startDose1, (nVoxels, 1))
        startDose2 = np.reshape(startDose2, (nVoxels, 1))
        stopDose1 = np.reshape(stopDose1, (nVoxels, 1))
        stopDose2 = np.reshape(stopDose2, (nVoxels, 1))

        startTimes = np.hstack((startTime1, startTime2))
        stopTimes = np.hstack((stopTime1, stopTime2))
        startDoses = np.hstack((startDose1, startDose2))
        stopDoses = np.hstack((stopDose1, stopDose2))

        deltaTimes = np.hstack((stopTime1-startTime1, stopTime2-startTime2))

        argMin = np.argmin(deltaTimes, axis=1)
        startTime = startTimes[range(nVoxels), argMin]
        stopTime = stopTimes[range(nVoxels), argMin]
        startDose = startDoses[range(nVoxels), argMin]
        stopDose = stopDoses[range(nVoxels), argMin]

        return dr, startTime, stopTime, startDose, stopDose
