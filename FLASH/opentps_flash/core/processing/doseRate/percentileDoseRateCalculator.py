import logging

import numpy as np
from opentps.core.data import SparseBeamlets
from opentps.core.data.images import CTImage, Image3D, ROIMask, DoseImage
from opentps.core.data.plan import RTPlan
from opentps.core.processing.doseCalculation.abstractDoseInfluenceCalculator import AbstractDoseInfluenceCalculator
from opentps_flash.core.processing.doseRate.abstractDoseRateCalculator import AbstractDoseRateCalculator
from opentps_flash.core.processing.Utils.doseComputation import computeDoseFromBeamlets

import scipy.sparse as sp


try:
    import sparse_dot_mkl
    use_MKL = 0
except:
    use_MKL = 0


logger = logging.getLogger(__name__)


class PercentileDoseRateCalculator(AbstractDoseRateCalculator):
    DOSE_THRESHOLD = 1.

    def __init__(self, deliveryModel):
        super().__init__(deliveryModel)
        self.doseCalculator:AbstractDoseInfluenceCalculator = None
        self.percentile:float = 0.95


    def computeDoseRate(self, ct:CTImage, plan:RTPlan, roi=None, beamlets=None) -> Image3D:
        if beamlets is None:
            beamlets = self.doseCalculator.computeBeamlets(ct, plan, roi=roi)

        return self.computeDoseRateFromDoseInfluence(plan, beamlets)


    def computeDoseRateFromDoseInfluence(self, plan:RTPlan, doseInfluence:SparseBeamlets) -> Image3D:
        roi = self._getDoseInfluenceROI(doseInfluence, plan.spotMUs) # 3D Dose image with 1s at ROI
        dosePerSpot = self._getFullDosePerSpot(doseInfluence, plan, roi) # Dose (Gy) delivered to the voxel |vxl| by spot number |spot|: size ROI voxel * spots number

        dr, _, _, _, _ = self.computeDoseRateFromDosePerSpot(plan[0][0], dosePerSpot)

        outImage = DoseImage.fromImage3D(roi, patient=None)
        outImageArray = np.zeros(outImage.imageArray.shape)
        outImageArray = outImageArray.flatten()

        roiArray = roi.imageArray
        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        outImageArray[roiArray] = dr.flatten()
        outImageArray = np.reshape(outImageArray, roi.gridSize, order='F')
        outImageArray = np.flip(outImageArray, 0)
        outImageArray = np.flip(outImageArray, 1)

        outImage.imageArray = outImageArray # Dose image of size roi with nonzero values at nonzero roi values

        return outImage

    def computeDoseRateFromDosePerSpot(self, layer, dosePerSpot):
        startTime, irradiationTime, scanningTime = self.deliveryModel.getSpotTimings(layer)

        fullDose = np.sum(dosePerSpot, axis=1)  # cumulated dose at the end of the delivery
        fullDose = np.reshape(fullDose, (len(fullDose), 1))  # size ROI voxel, 1

        totalTime = np.cumsum(irradiationTime) + np.cumsum(scanningTime)

        cumDose = np.cumsum(dosePerSpot, axis=1)  # size ROI voxel * spots number
        percDose = cumDose / fullDose  # size ROI voxel * spots number
        p = (1 - self.percentile) / 2
        nVoxels = cumDose.shape[0]
        startTimeIndex = np.argmax(percDose >= p, axis=1)
        endTimeIndex = np.argmax(percDose >= self.percentile + p, axis=1)

        # for each voxel, interpolate
        startTime = totalTime[startTimeIndex - 1] + scanningTime[startTimeIndex] + irradiationTime[startTimeIndex] / \
                    (percDose[range(nVoxels), startTimeIndex.reshape(-1)] - percDose[
                        range(nVoxels), startTimeIndex.reshape(-1) - 1]) * \
                    (p - percDose[range(nVoxels), startTimeIndex.reshape(-1) - 1])

        startDose = cumDose[range(nVoxels), startTimeIndex - 1] + (cumDose[range(nVoxels), startTimeIndex] - cumDose[range(nVoxels), startTimeIndex - 1]) / \
                    (percDose[range(nVoxels), startTimeIndex.reshape(-1)] - percDose[
                        range(nVoxels), startTimeIndex.reshape(-1) - 1]) * \
                    (p - percDose[range(nVoxels), startTimeIndex.reshape(-1) - 1])

        # Correct start time of the voxel where at the first spot, percDose > p
        startTime[startTimeIndex == 0] = totalTime[0]

        stopTime = totalTime[endTimeIndex - 1] + scanningTime[endTimeIndex] + irradiationTime[endTimeIndex] / \
                   (percDose[range(nVoxels), endTimeIndex.reshape(-1)] - percDose[
                       range(nVoxels), endTimeIndex.reshape(-1) - 1]) * \
                   (self.percentile + p - percDose[range(nVoxels), endTimeIndex.reshape(-1) - 1])

        stopDose = cumDose[range(nVoxels), endTimeIndex - 1] + (cumDose[range(nVoxels), endTimeIndex] - cumDose[range(nVoxels), endTimeIndex - 1]) / \
                   (percDose[range(nVoxels), endTimeIndex.reshape(-1)] - percDose[
                       range(nVoxels), endTimeIndex.reshape(-1) - 1]) * \
                   (self.percentile + p - percDose[range(nVoxels), endTimeIndex.reshape(-1) - 1])

        spotTiming = stopTime - startTime
        spotTiming = spotTiming.reshape(-1, 1)

        tnonzero = np.flatnonzero(spotTiming > 0)

        dr = np.zeros((nVoxels, 1))
        dr[tnonzero] = self.percentile * fullDose[tnonzero] / spotTiming[tnonzero]

        return dr, startTime, stopTime, startDose, stopDose


    def _getDoseInfluenceROI(self, doseInfluence:SparseBeamlets, weights:np.ndarray) -> ROIMask:
        fullDose = computeDoseFromBeamlets(doseInfluence, weights)

        roiArray = np.zeros(fullDose.gridSize)
        roiArray[fullDose.imageArray>=self.DOSE_THRESHOLD] = 1

        roi = ROIMask.fromImage3D(fullDose, patient=None)
        roi.imageArray = roiArray.astype(bool)

        return roi

    def _getFullDosePerSpot(self, doseInfluence:SparseBeamlets, plan:RTPlan, roi:ROIMask, index_spots=None) -> np.ndarray:
        doseInfluenceMat = doseInfluence.toSparseMatrix()

        roiArray = roi.imageArray

        roiArray = np.flip(roiArray, 1)
        roiArray = np.flip(roiArray, 0)
        roiArray = roiArray.flatten(order='F')

        if np.count_nonzero(roiArray)<=0:
            raise Exception("Dose matrix is empty")

        layer = plan[0][0]
        weights = layer.spotMUs

        dosePerSpot = doseInfluenceMat[roiArray.astype(bool),:]

        if doseInfluenceMat.shape[1] != plan.numberOfSpots:
            print("doseInfluenceMat.shape[1] != plan.numberOfSpots")
            if index_spots is None:
                raise ValueError(f"Beamlet matrix has {doseInfluenceMat.shape[1]} columns while there are {plan.numberOfSpots} spots. You should provide the indexes of the spots corresponding to the beamlet matrix columns.")
            weight_matrix = np.zeros((doseInfluenceMat.shape[1], plan.numberOfSpots))
            for i in range(plan.numberOfSpots):
                weight_matrix[index_spots[i],i] = weights[i]
            weight_matrix = sp.csc_matrix(weight_matrix.astype(np.float32))
        else:
            weight_matrix = sp.diags(weights.astype(np.float32), format='csc')

        if use_MKL == 1:
            dosePerSpot = sparse_dot_mkl.dot_product_mkl(
                dosePerSpot, weight_matrix)
        else:
            dosePerSpot = sp.csc_matrix.dot(
                dosePerSpot, weight_matrix)

        return np.asarray(dosePerSpot.todense())
