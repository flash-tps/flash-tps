from enum import Enum
from math import cos, pi
from typing import Sequence
import math

import numpy as np
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import CTImage, ROIMask, Image3D
from opentps.core.data.plan import PlanIonBeam, PlanIonLayer, RTPlan
from opentps.core.processing.imageProcessing import imageTransform3D
from opentps.core.processing.rangeEnergy import rangeMMToEnergy
from opentps.core.processing.planOptimization.planInitializer import BeamInitializer
from opentps.core.processing.C_libraries.libRayTracing_wrapper import transport_spots_to_target

class BeamInitializerBEV:
    class GridTypes(Enum):
        SQUARE = 'SQUARE'
        HEXAGONAL = 'HEXAGONAL'
        DEFAULT = 'HEXAGONAL'

    def __init__(self):
        self.spotSpacing = 5.
        self.layerSpacing = 2.
        self.targetMargin = 0.
        self.gridType = self.GridTypes.DEFAULT

        self.calibration:AbstractCTCalibration=None

    def initializeBeam(self, beam:PlanIonBeam, ctBEV:CTImage, targetMaskBEV:ROIMask):
        #TODO Range shifter
        from opentps.core.data.images import RSPImage
        roiDilated = ROIMask.fromImage3D(targetMaskBEV, patient=None)
        roiDilated.dilateMask(radius=self.targetMargin)

        rspImage = RSPImage.fromCT(ctBEV, self.calibration, energy=100.)

        if beam.isocenterPosition is None:
            beam.isocenterPosition = targetMaskBEV.centerOfMass

        cumRSPBEV = RSPImage.fromImage3D(rspImage, patient=None)
        cumRSPArray =  np.cumsum(cumRSPBEV.imageArray, axis=2)*cumRSPBEV.spacing[2]
        cumRSPArray[np.logical_not(roiDilated.imageArray.astype(bool))]= 0
        cumRSPBEV.imageArray = cumRSPArray

        maxWEPL = cumRSPBEV.imageArray.max()
        minWEPL = cumRSPBEV.imageArray[cumRSPBEV.imageArray > 0.].min()

        rangeLayers = np.arange(minWEPL-self.layerSpacing, maxWEPL+self.layerSpacing, self.layerSpacing)
        energyLayers = rangeMMToEnergy(rangeLayers)

        weplMeV = rangeMMToEnergy(cumRSPBEV.imageArray)

        if self.gridType == self.GridTypes.HEXAGONAL:
            spotGridX, spotGridY = self._defineHexagSpotGridAroundIsocenter(self.spotSpacing, cumRSPBEV, beam.isocenterPosition)
        else:
            spotGridX, spotGridY = self._defineSquaredSpotGridAroundIsocenter(self.spotSpacing, cumRSPBEV, beam.isocenterPosition)

        coordGridX, coordGridY = self._pixelCoordinatedWrtIsocenter(cumRSPBEV, beam.isocenterPosition)

        spotGridX = spotGridX.flatten()
        spotGridY = spotGridY.flatten()

        for l, energy in enumerate(energyLayers):
            if energy<=0.:
                continue
            elif energy==energyLayers[0]:
                layerMask = weplMeV <= energy
            elif energy==energyLayers[-1]:
                layerMask = weplMeV > energy
            else:
                layerMask = np.logical_and(weplMeV>energy, weplMeV<=energyLayers[l+1])

            layerMask = np.sum(layerMask, axis=2).astype(bool)

            layerMask = np.logical_and(layerMask, np.sum(roiDilated.imageArray, axis=2).astype(bool))

            coordX = coordGridX[layerMask]
            coordY = coordGridY[layerMask]

            coordX = coordX.flatten()
            coordY = coordY.flatten()

            x2 = np.matlib.repmat(np.reshape(spotGridX, (spotGridX.shape[0], 1)), 1, coordX.shape[0]) \
                 - np.matlib.repmat(np.transpose(coordX), spotGridX.shape[0], 1)
            y2 = np.matlib.repmat(np.reshape(spotGridY, (spotGridY.shape[0], 1)), 1, coordY.shape[0]) \
                 - np.matlib.repmat(np.transpose(coordY), spotGridY.shape[0], 1)

            ind = (x2*x2+y2*y2).argmin(axis=0)

            spotPosCandidates = np.unique(np.array(list(zip(spotGridX[ind], -spotGridY[ind]))), axis=0)
            if spotPosCandidates.shape[0]==0:
                continue

            layer = PlanIonLayer(energy)
            for i in range(spotPosCandidates.shape[0]):
                spotPos = spotPosCandidates[i, :]
                layer.appendSpot(spotPos[0], spotPos[1], 1.)
            beam.appendLayer(layer)

    def _defineSquaredSpotGridAroundIsocenter(self, spotSpacing: float, imageBEV: Image3D, isocenterBEV: Sequence[float]):
        origin = imageBEV.origin
        end = imageBEV.origin + imageBEV.spacing * imageBEV.imageArray.shape

        spotGridSpacing = [spotSpacing, spotSpacing]

        xFromIsoToOrigin = np.arange(isocenterBEV[0], origin[0], -spotGridSpacing[0])
        xFromOriginToIso = np.flipud(xFromIsoToOrigin)
        xFromIsoToEnd = np.arange(isocenterBEV[0] + spotGridSpacing[0], end[0], spotGridSpacing[0])
        yFromIsoToOrigin = np.arange(isocenterBEV[1], origin[1], -spotGridSpacing[1])
        yFromOriginToIso = np.flipud(yFromIsoToOrigin)
        yFromIsoToEnd = np.arange(isocenterBEV[1] + spotGridSpacing[1], end[1], spotGridSpacing[1])

        x = np.concatenate((xFromOriginToIso, xFromIsoToEnd))
        y = np.concatenate((yFromOriginToIso, yFromIsoToEnd))

        spotGridX, spotGridY = np.meshgrid(x, y)

        spotGridX = spotGridX - isocenterBEV[0]
        spotGridY = spotGridY - isocenterBEV[1]

        return spotGridX, spotGridY

    def _defineHexagSpotGridAroundIsocenter(self, spotSpacing: float, imageBEV: Image3D, isocenterBEV: Sequence[float]):
        origin = imageBEV.origin
        end = imageBEV.origin + imageBEV.spacing * imageBEV.imageArray.shape

        spotGridSpacing = [spotSpacing*cos(pi/6), spotSpacing/2.]

        xFromIsoToOrigin = np.arange(isocenterBEV[0], origin[0], -spotGridSpacing[0])
        xFromOriginToIso = np.flipud(xFromIsoToOrigin)
        xFromIsoToEnd = np.arange(isocenterBEV[0] + spotGridSpacing[0], end[0], spotGridSpacing[0])
        yFromIsoToOrigin = np.arange(isocenterBEV[1], origin[1], -spotGridSpacing[1])
        yFromOriginToIso = np.flipud(yFromIsoToOrigin)
        yFromIsoToEnd = np.arange(isocenterBEV[1] + spotGridSpacing[1], end[1], spotGridSpacing[1])

        x = np.concatenate((xFromOriginToIso, xFromIsoToEnd))
        y = np.concatenate((yFromOriginToIso, yFromIsoToEnd))

        spotGridX, spotGridY = np.meshgrid(x, y)

        spotGridX = spotGridX - isocenterBEV[0]
        spotGridY = spotGridY - isocenterBEV[1]

        isoInd0 = xFromOriginToIso.shape[0]  # index of isocenter
        isoInd1 = yFromOriginToIso.shape[0]  # index of isocenter

        hexagonalMask = np.zeros(spotGridX.shape)

        hexagonalMask[isoInd0%2::2, isoInd1%2::2] = 1 # Isocenter is here
        hexagonalMask[((isoInd0-1)%2+2)%2::2, ((isoInd1-1)%2+2)%2::2] = 1

        spotGridX = spotGridX[hexagonalMask.astype(bool)]
        spotGridY = spotGridY[hexagonalMask.astype(bool)]

        return spotGridX, spotGridY

    def _pixelCoordinatedWrtIsocenter(self, imageBEV: Image3D, isocenterBEV: Sequence[float]):
        origin = imageBEV.origin
        end = imageBEV.origin + imageBEV.spacing * imageBEV.imageArray.shape

        x = np.linspace(origin[0], end[0]-imageBEV.spacing[0], imageBEV.gridSize[0])
        y = np.linspace(origin[1], end[1]-imageBEV.spacing[1], imageBEV.gridSize[1])
        [coordGridX, coordGridY] = np.meshgrid(x, y)
        coordGridX = np.transpose(coordGridX)
        coordGridY = np.transpose(coordGridY)

        coordGridX = coordGridX - isocenterBEV[0]
        coordGridY = coordGridY - isocenterBEV[1]

        return coordGridX, coordGridY

class BeamInitializerDicom(BeamInitializer):
    class GridTypes(Enum):
        SQUARE = 'SQUARE'
        HEXAGONAL = 'HEXAGONAL'
        DEFAULT = 'HEXAGONAL'
    def __init__(self, nominalEnergy = 229, gridType=None):
        super().__init__()
        self.energy = nominalEnergy
        if gridType:
            self.gridType = gridType
        else:
            self.gridType = self.GridTypes.DEFAULT

    def initializeBeam(self, ct, targetMask):
        from opentps.core.data.images._rspImage import RSPImage
        roiDilated = ROIMask.fromImage3D(targetMask, patient=None)
        roiDilated.dilateMask(radius=self.targetMargin)

        targetMask = roiDilated

        rspImage = RSPImage.fromCT(ct, self.calibration, energy=100.)
        rspImage.patient = None

        imgBordersX = [rspImage.origin[0], rspImage.origin[0] + rspImage.gridSize[0] * rspImage.spacing[0]]
        imgBordersY = [rspImage.origin[1], rspImage.origin[1] + rspImage.gridSize[1] * rspImage.spacing[1]]
        imgBordersZ = [rspImage.origin[2], rspImage.origin[2] + rspImage.gridSize[2] * rspImage.spacing[2]]

        if self.beam.isocenterPosition is None:
            self.beam.isocenterPosition = targetMask.centerOfMass
        
        # generate hexagonal spot grid around isocenter
        if self.gridType == self.GridTypes.HEXAGONAL:
            spotGrid = self._defineHexagSpotGridAroundIsocenter()
        else:
            spotGrid = self._defineSquareSpotGridAroundIsocenter()
        numSpots = len(spotGrid["x"])

        # compute direction vector
        u, v, w = 1e-10, 1.0, 1e-10  # BEV to 3D coordinates
        [u, v, w] = self._rotateVector([u, v, w], math.radians(self.beam.gantryAngle), 'z')  # rotation for gantry angle
        [u, v, w] = self._rotateVector([u, v, w], math.radians(self.beam.couchAngle), 'y')  # rotation for couch angle

        # prepare raytracing: translate initial positions at the CT image border
        for s in range(numSpots):
            translation = np.array([1.0, 1.0, 1.0])
            translation[0] = (spotGrid["x"][s] - imgBordersX[int(u < 0)]) / u
            translation[1] = (spotGrid["y"][s] - imgBordersY[int(v < 0)]) / v
            translation[2] = (spotGrid["z"][s] - imgBordersZ[int(w < 0)]) / w
            translation = translation.min()
            spotGrid["x"][s] = spotGrid["x"][s] - translation * u
            spotGrid["y"][s] = spotGrid["y"][s] - translation * v
            spotGrid["z"][s] = spotGrid["z"][s] - translation * w

        # transport each spot until it reaches the target
        transport_spots_to_target(rspImage, targetMask, spotGrid, [u, v, w])

        # remove spots that didn't reach the target
        minWET = 9999999
        for s in range(numSpots - 1, -1, -1):
            if spotGrid["WET"][s] < 0:
                spotGrid["BEVx"].pop(s)
                spotGrid["BEVy"].pop(s)
                spotGrid["x"].pop(s)
                spotGrid["y"].pop(s)
                spotGrid["z"].pop(s)
                spotGrid["WET"].pop(s)

        # process valid spots
        numSpots = len(spotGrid["x"])
        layer = PlanIonLayer(self.energy)
        for s in range(numSpots):
            layer.appendSpot(spotGrid["BEVx"][s], spotGrid["BEVy"][s], 1.)

        self.beam.appendLayer(layer)


class PlanInitializer:
    def __init__(self, beamInitializer=BeamInitializerBEV()):
        self.ctCalibration:AbstractCTCalibration=None
        self.ct:CTImage=None
        self.plan:RTPlan=None
        self.targetMask:ROIMask=None
        
        self._beamInitializer = beamInitializer

    def initializePlan(self, spotSpacing:float, layerSpacing:float=None, targetMargin:float=0.):
        #TODO Range shifter

        self._beamInitializer.calibration = self.ctCalibration
        self._beamInitializer.spotSpacing = spotSpacing
        self._beamInitializer.layerSpacing = layerSpacing
        self._beamInitializer.targetMargin = targetMargin

        for beam in self.plan:
            beam.removeLayer(beam.layers)

            if not (beam.isocenterPosition is None):
                beam.isocenterPosition = self.targetMask.centerOfMass

            if isinstance(self._beamInitializer, BeamInitializerBEV):
                ctBEV = imageTransform3D.dicomToIECGantry(self.ct, beam, fillValue=-1024.,
                                                        cropROI=self.targetMask, cropDim0=True,
                                                        cropDim1=True, cropDim2=False)
                roiBEV = imageTransform3D.dicomToIECGantry(self.targetMask, beam, fillValue=0.,
                                                        cropROI=self.targetMask, cropDim0=True,
                                                        cropDim1=True, cropDim2=False)

                self._beamInitializer.beam = beam
                self._beamInitializer.initializeBeam(beam, ctBEV, roiBEV)
            elif isinstance(self._beamInitializer, BeamInitializerDicom):
                self._beamInitializer.beam = beam
                self._beamInitializer.initializeBeam(self.ct, self.targetMask)
            else:
                raise ValueError('BeamInitializer only defined for BeamInitializerBEV and BeamInitializerDicom')   
