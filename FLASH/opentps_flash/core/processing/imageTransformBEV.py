from typing import Sequence

import numpy as np
from opentps.core.data.images import Image3D


def bevToGantry0(imageBEV:Image3D) -> Image3D:
    outImage = imageBEV.__class__.fromImage3D(imageBEV, patient=None)
    outImage.imageArray = np.swapaxes(imageBEV.imageArray, 1, 2)
    outImage.imageArray = np.flip(outImage.imageArray, 2)

    outImage.origin = [imageBEV.origin[0], imageBEV.origin[2], imageBEV.origin[1]]
    outImage.spacing = [imageBEV.spacing[0], imageBEV.spacing[2], imageBEV.spacing[1]]

    return outImage

def bevCoordinateToGantry0(imageBEV:Image3D, coordinate:Sequence[float]) -> np.ndarray:
    outCoordinate = np.array(coordinate)
    outCoordinate[1] = coordinate[2]
    outCoordinate[2] = imageBEV.origin[1] + imageBEV.origin[1] + imageBEV.gridSizeInWorldUnit[1]-imageBEV.spacing[1] - coordinate[1]

    return outCoordinate

def gantry0ToBEV(image:Image3D) -> Image3D:
    outImage = image.__class__.fromImage3D(image, patient=None)
    outImage.imageArray = np.flip(image.imageArray, 2)
    outImage.imageArray = np.swapaxes(outImage.imageArray, 1, 2)

    outImage.origin = [image.origin[0], image.origin[2], image.origin[1]]
    outImage.spacing = [image.spacing[0], image.spacing[2], image.spacing[1]]

    return outImage

def gantry0CoordinateToBEV(imageGantry0:Image3D, coordinate:Sequence[float]) -> np.ndarray:
    outCoordinate = np.array(coordinate)
    outCoordinate[2] = coordinate[1]
    outCoordinate[1] = imageGantry0.origin[2] + imageGantry0.origin[2] + imageGantry0.gridSizeInWorldUnit[2]-imageGantry0.spacing[2] - coordinate[2]

    return outCoordinate
