import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interpolate


def fmf_let(let, let0, fmf0):
    def _sigmoid(x, l, x_off):
        return 0.5 - 1/(1+np.exp(-l*(x-x_off)))

    l = 0.005
    x_off = -500
    A = (1/fmf0-1)/(_sigmoid(let0, l, x_off)-_sigmoid(500, l, x_off))
    y_off = 1 - A * _sigmoid(500, l, x_off)
    sig = A * _sigmoid(let, l, x_off) + y_off

    return 1/sig

def fmf0_dose(dose, doseFMF, FMF):
    return interpolate.interp1d(np.array([0, doseFMF]), np.array([1, FMF]), kind='linear', fill_value='extrapolate')(dose)

def fmf_let_dose(let, dose, doseFMF, FMF):
    fmf0 = fmf0_dose(dose, doseFMF, FMF)
    return fmf_let(let, 10, fmf0) # I assume a LET of 10 kEV/um

def fmf_let_dose_dr(let, dose, doseRate, doseFMF, FMF, drFLASH=40):
    fmf = fmf_let_dose(let, dose, doseFMF, FMF)
    fmf[doseRate<drFLASH] = 1
    return fmf


if __name__ == "__main__":
    plt.legend(prop={'size': 22})
    plt.rcParams.update({'font.size': 22})
    plt.show()

    x = np.arange(0, 1000)
    plt.plot(x, 1/fmf_let(x, 0.1, 1/2))
    plt.plot(x, 1/fmf_let(x, 0.1, 1/1.85))
    plt.plot(x, 1/fmf_let(x, 0.1, 1/1.2))

    plt.xlabel('LET (keV/um)')
    plt.ylabel('DMF')

    plt.xscale("log")
    plt.grid(visible=True)

    plt.legend(prop={'size': 22})
    plt.rcParams.update({'font.size': 22})
    plt.show()



    let = np.arange(0, 50)
    plt.plot(let, 1/fmf_let_dose(let, 10, 10, 0.9), label="10 Gy")
    plt.plot(let, 1/fmf_let_dose(let, 5, 10, 0.9), label="5 Gy")

    plt.xlabel('LET (keV/um)')
    plt.ylabel('DMF')

    plt.legend(prop={'size': 22})
    plt.grid(visible=True)
    plt.show()



    dose = np.arange(0, 15)
    plt.plot(dose, 1/fmf0_dose(dose, 10, 0.9))

    plt.xlabel('Dose (Gy)')
    plt.ylabel('DMF')

    plt.legend(prop={'size': 22})
    plt.grid(visible=True)
    plt.show()