import functools
from typing import Optional, Sequence, Union

import matplotlib.pyplot as plt
import numpy as np
import scipy

from opentps.core.data import SparseBeamlets
from opentps.core.data.images import DoseImage
from opentps.core.data.plan import RTPlan
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps_flash.core.processing.CEMOptimization.planObjectives import AbstractDoseFidelityTerm
from scipy.optimize import minimize, Bounds
from opentps_flash.core.processing.Utils.doseComputation import computeDoseFromBeamlets


class PlanOptimizerObjectives:
    def __init__(self):
        self.objectiveWeights = []
        self.objectiveTerms = []

    def append(self, objective: AbstractDoseFidelityTerm, weight: float = 1.):
        self.objectiveWeights.append(weight)
        self.objectiveTerms.append(objective)

    def getValue(self, weights: np.ndarray) -> float:
        val = 0
        for i, objectiveTerm in enumerate(self.objectiveTerms):
            val += self.objectiveWeights[i] * objectiveTerm.getValue(weights)

        return val

    def getDerivative(self, weights: np.ndarray) -> np.ndarray:
        val = 0.
        for i, objectiveTerm in enumerate(self.objectiveTerms):
            val += self.objectiveWeights[i] * objectiveTerm.getDerivative(weights)

        return np.array(val).astype(float)

class PlanOptimizer:
    @staticmethod
    def run(objectives:PlanOptimizerObjectives, plan:RTPlan, w0:Optional[np.ndarray]=None, maxiter:float = 1000, bounds=(0.,9999.)):
        if w0 is None:
            w0 = PlanOptimizer._estimatew0(objectives.getValue, plan)

        w0[w0<=0] = 1

        #plan.spotMUs = PlanOptimizer._gd(objectives.getValue, objectives.getDerivative, w0)
        #return

        res = minimize(objectives.getValue, w0,
                        method='L-BFGS-B',
                        jac=objectives.getDerivative,
                        tol=None, callback=None,
                        options={'disp': True, 'maxcor': 10, 'ftol': 1e-6, 'gtol': 1e-6,
                           'maxfun': 15000, 'maxiter': maxiter, 'iprint': -1, 'maxls': 20, 'finite_diff_rel_step': None},
                        bounds=Bounds(bounds[0], bounds[1]))

        print("res",res)
        plan.spotMUs = res.x

    @staticmethod
    def _estimatew0(fun, plan, step=10.):
        maxIter = 100
        verbose = True

        x0 = np.ones(plan.spotMUs.shape)

        fVal = fun(x0)
        prevFVal = fVal
        x = x0
        prevX = x

        for i in range(maxIter):
            x += step
            x[x < 0] = 0
            fVal = fun(x)

            if verbose:
                print('Estimating w0 - Iter.: ' + str(i) + ' - FVal: ' + str(fVal))
            if fVal >= prevFVal:
                return prevX

            prevFVal = fVal
            prevX = x

        return x


    @staticmethod
    def _gd(fun, deriv, x0):
        maxIter = 100
        step = 50
        verbose = True
        absTol = 0.001

        fVal = fun(x0)
        prevFVal = fVal
        x = x0
        prevX = x

        d = np.array(deriv(x))
        step = PlanOptimizer._estimateStep(fun, x, d, step)

        print(step)

        for i in range(maxIter):
            d = np.array(deriv(x))
            lsFun = functools.partial(PlanOptimizer._lsFun, fun, x, d)

            if np.mod(i, 5)==0:
                step = scipy.optimize.fminbound(lsFun, step/2, step*2, args=(), xtol=1e-01, maxfun=5, full_output=0, disp=1)
                print(step)

            x = x - step*d
            x[x<0] = 0
            fVal = fun(x)

            if verbose:
                print('GD - Iter.: ' + str(i) + ' - FVal: ' + str(fVal))
            if fVal>=prevFVal or prevFVal-fVal<absTol:
                return prevX

            prevFVal = fVal
            prevX = x

        return x

    @staticmethod
    def _estimateStep(fun, x0, dir, step=10):
        maxIter = 100

        fVal = fun(x0)
        prevFVal = fVal
        x = x0
        cumStep = step

        for i in range(maxIter):
            x = x0 - cumStep * dir
            x[x < 0] = 0
            fVal = fun(x)

            if fVal >= prevFVal:
                return cumStep-step

            prevFVal = fVal
            cumStep += step

        return cumStep

    @staticmethod
    def _lsFun(fun, x, dir, step):
        x = x - step * dir
        x[x < 0] = 0
        return fun(x)

class PlanDoseCalculator:
    def __init__(self):
        self.beamModel = None
        self.ctCalibration = None
        self.ct = None
        self.plan = None
        self.roi = None
        self.nbPrimaries = 1e5
        self._doseCalculator = MCsquareDoseCalculator()

        self._weights:np.ndarray = None
        self._dose:DoseImage = None
        self._beamlets:SparseBeamlets = None
    
    @property
    def scoringVoxelSpacing(self) -> Sequence[float]:
        return self._doseCalculator.scoringVoxelSpacing
    
    @scoringVoxelSpacing.setter
    def scoringVoxelSpacing(self, spacing: Union[float, Sequence[float]]):
        self._doseCalculator.scoringVoxelSpacing = spacing
    
    @property
    def scoringGridSize(self) -> Sequence[float]:
        return self._doseCalculator.scoringGridSize
    
    @scoringGridSize.setter
    def scoringGridSize(self, gridSize:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringGridSize = gridSize

    @property
    def scoringOrigin(self) -> Sequence[float]:
        return self._doseCalculator.scoringOrigin
    
    @scoringOrigin.setter
    def scoringOrigin(self, origin:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringOrigin = origin

    def setScoringParameters(self, scoringGridSize:Optional[Sequence[int]]=None, scoringSpacing:Optional[Sequence[float]]=None,
                                scoringOrigin:Optional[Sequence[int]]=None, adapt_gridSize_to_new_spacing=False):
        """
        Sets the scoring parameters

        Parameters
        ----------
        scoringGridSize: Sequence[int]
            scoring grid size
        scoringSpacing: Sequence[float]
            scoring spacing
        scoringOrigin: Sequence[float]
            scoring origin
        adapt_gridSize_to_new_spacing: bool
            If True, automatically adapt the gridSize to the new spacing
        """
        self._doseCalculator.setScoringParameters(scoringGridSize, scoringSpacing,
                                scoringOrigin, adapt_gridSize_to_new_spacing)
            

    def kill(self):
        self._doseCalculator.kill()

    def computeDose(self, weights:np.ndarray) -> DoseImage:
        if not np.array_equal(weights, self._weights):
            self._weights = weights
            self._recomputeDose()

        return self._dose

    def computeBeamlets(self) -> SparseBeamlets:
        if self._beamlets is None:
            self._recomputeBeamlets()

        return self._beamlets

    def _recomputeBeamlets(self):
        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.nbPrimaries = self.nbPrimaries

        self._beamlets = self._doseCalculator.computeBeamlets(self.ct, self.plan) # , self.roi

    def _recomputeDose(self):
        if self._beamlets is None:
            self._recomputeBeamlets()

        self._dose = computeDoseFromBeamlets(self._beamlets, self._weights)
