import copy
from typing import Optional

import numpy as np

from opentps.core.data.plan import RTPlan, PlanDesign, ObjectivesList, FidObjective
from opentps.core.processing.planOptimization.planOptimization import IMPTPlanOptimizer
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.CEMOptimization import cemObjectives
from opentps_flash.core.processing.CEMOptimization.planOptimizer import PlanOptimizer, PlanOptimizerObjectives


class OpenTPSPlanOptimizer(PlanOptimizer):
    @staticmethod
    def run(objectives:PlanOptimizerObjectives, plan:RTPlan, w0:Optional[np.ndarray]=None):
        beam = plan[0]
        cem = beam.cem

        doseCalculator = objectives.objectiveTerms[0].doseCalculator
        ctBEV = doseCalculator.ctBEV
        ctGantry0 = imageTransformBEV.bevToGantry0(ctBEV)
        roiBEV = doseCalculator.roiBEV
        roiG0 = imageTransformBEV.bevToGantry0(roiBEV)

        planDesign = PlanDesign()
        planDesign.ct = ctGantry0
        planDesign.targetMask = roiG0
        planDesign.calibration = doseCalculator.ctCalibration
        planDesign.beamlets = doseCalculator.computeBeamletsGantry0(cem)

        planGantry0 = RTPlan()
        beam = copy.deepcopy(beam)
        beam.gantryAngle = 0
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(ctBEV, beam.isocenterPosition)
        planGantry0.appendBeam(beam)

        planGantry0.planDesign = planDesign

        planDesign.objectives = ObjectivesList()
        planDesign.objectives.setTarget(roiG0.name, 1.) # prescription does not matter
        planDesign.objectives.fidObjList = []

        for i, w in enumerate(objectives.objectiveWeights):
            obj = objectives.objectiveTerms[i]

            roiG0 = imageTransformBEV.bevToGantry0(obj.roiBEV)

            if isinstance(obj, cemObjectives.DoseMaxObjective):
                planDesign.objectives.addFidObjective(roiG0, FidObjective.Metrics.DMAX, obj.maxDose, w)
            elif isinstance(obj, cemObjectives.DoseMinObjective):
                planDesign.objectives.addFidObjective(roiG0, FidObjective.Metrics.DMIN, obj.minDose, w)

        solver = IMPTPlanOptimizer(method='Scipy-LBFGS', plan=planGantry0, maxit=1000)
        solver.threshold_MU = -1
        doseImage, ps = solver.optimize()

        plan.spotMUs = planGantry0.spotMUs
