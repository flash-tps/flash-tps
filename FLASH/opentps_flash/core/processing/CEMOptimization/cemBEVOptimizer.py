import copy
from typing import Tuple

import numpy as np
from scipy import ndimage

from opentps.core import Event
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import ROIMask, CTImage, RSPImage
from opentps.core.data.plan import RTPlan
from opentps.core.processing.imageProcessing import resampler3D, imageTransform3D
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.cem import CEM
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.data.ctWithCEM import CTBEVWithCEM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.CEMOptimization.cemBEVDoseCalculator import CEMBEVDoseCalculator
from opentps_flash.core.processing.CEMOptimization.cemBEVPreconditioningDoseCalculator import \
    CEMBEVPreconditioningDoseCalculator
from opentps_flash.core.processing.CEMOptimization.cemObjectives import CEMAbstractDoseFidelityTerm
from opentps_flash.core.processing.CEMOptimization.cemPlanInitializer import CEMBEVBeamInitializer
from opentps_flash.core.processing.CEMOptimization.planOptimizer import PlanOptimizerObjectives, \
    PlanOptimizer
from opentps_flash.core.processing.planInitializer import BeamInitializerDicom, PlanInitializer


class AbortedException(Exception):
    pass


class CEMBEVOptimizer:
    class _Objectives(PlanOptimizerObjectives):
        def __init__(self):
            super().__init__()

            self.objectiveWeights = []
            self.objectiveTerms: list[CEMAbstractDoseFidelityTerm] = []

            self._currentCEM = None

        @property
        def cem(self) -> CEM:
            return self._currentCEM

        @cem.setter
        def cem(self, cem: CEM):
            self._currentCEM = cem

        def kill(self):
            for objective in self.objectiveTerms:
                objective.doseCalculator.kill()

        def append(self, objective: CEMAbstractDoseFidelityTerm, weight: float = 1.):
            self.objectiveWeights.append(weight)
            self.objectiveTerms.append(objective)

        def getValue(self, weights: np.ndarray) -> float:
            val = 0
            for i, objectiveTerm in enumerate(self.objectiveTerms):
                val += self.objectiveWeights[i] * objectiveTerm.getValue(weights, self._currentCEM)

            return val

        def getDerivative(self, weights: np.ndarray) -> np.ndarray:
            val = 0.
            for i, objectiveTerm in enumerate(self.objectiveTerms):
                val += self.objectiveWeights[i] * objectiveTerm.getWeightDerivative(weights, self._currentCEM)

            return np.array(val).astype(float)

        def getCEMDerivative(self, weighs: np.ndarray) -> CEM:
            outCEM = None

            for i, objectiveTerm in enumerate(self.objectiveTerms):
                cem = objectiveTerm.getCEMDerivative(weighs, self._currentCEM)

                iVal = self.objectiveWeights[i] * cem.imageArray

                if outCEM is None:
                    outCEM = cem
                    outCEM.imageArray = iVal
                else:
                    outCEM.imageArray = outCEM.imageArray + iVal

            return outCEM

    def __init__(self):
        self.maxIterations = 50
        self.spotSpacing = 5
        self.absTol = 0.1
        self.bounds = (0,9999.)
        self.targetMargin = 0
        self.ctCalibration: AbstractCTCalibration = None
        self.minCEMThickness = 5.
        self.maxCEMThickness = None  # by default automatically computed
        self.enableMCDerivative = False

        self.planUpdateEvent = Event(RTPlan)
        self.doseBEVUpdateEvent = Event(object)
        self.fValEvent = Event(Tuple)

        self._abort = False
        self._ct: CTImage = None
        self._ctBEV: CTImage = None
        self._targetMaskBEV: ROIMask = None
        self._targetMask: ROIMask = None
        self._iteration = 0
        self._plan: RTPlan = None
        self._beam: CEMBeam = None
        self._beamInitializer = CEMBEVBeamInitializer()
        self._objectives = None
        self._newObjectives = self._Objectives()
        self._useNewObjectives = True
        self._maxStep = 5.  # TODO User should be able to set this . Or better we should estimate this from mean thickness of target. This is a very important/imapcting parameter!

    def appendObjective(self, objective: CEMAbstractDoseFidelityTerm, weight: float = 1.):
        self._newObjectives.append(objective, weight)

    def clearObjectives(self):
        self._newObjectives = self._Objectives()

    def updateObjectives(self):
        self._useNewObjectives = True

    def abort(self):
        self._abort = True
        self._objectives.kill()
    
    def initialization(self, beam: CEMBeam, ctBEV: CTImage, targetMaskBEV):
        self._abort = False

        self._beam = beam
        self._plan = RTPlan()
        self._plan.appendBeam(beam)

        self._ctBEV = ctBEV
        self._targetMaskBEV = targetMaskBEV

        self._initializeCEM()
        if len(self._beam.spotMUs) <= 0:
            self._initializePlan()        

        # Adapt objective ROI to scoring grid
        doseCalculator: CEMBEVDoseCalculator = self._newObjectives.objectiveTerms[0].doseCalculator
        doseCalculator._updateCTForBeamletsWithCEM(self._beam.cem)
        doseCalculator._doseCalculator.ct = doseCalculator._ctCEFForBeamlets
        for objective in self._newObjectives.objectiveTerms:
            objective._updateMaskVec(spacing=doseCalculator.scoringVoxelSpacing, gridSize=doseCalculator.scoringGridSize, origin=doseCalculator.scoringOrigin)


    def run(self, beam: CEMBeam, ctBEV: CTImage, targetMaskBEV):
        self.initialization(beam, ctBEV, targetMaskBEV)

        try:
            doseCalculator = self._newObjectives.objectiveTerms[0].doseCalculator
            spotMUs, cem = self._gd(self._beam.cem)  # actual optimization
        except Exception as e:
            raise e from e
        finally:
            self._abort = False

        self._beam.spotMUs = spotMUs
        self._setCEMInBeam(cem)

    def _initializeCEM(self):
        """
        TODO: Explain what happens here
        """

        refCEM = CEM.fromBeam(self._ctBEV, self._beam)
        if self._beam.cem is None:
            self._beam.cem = refCEM
        else:
            self._beam.cem = resampler3D.resampleImage3DOnImage3D(self._beam.cem, refCEM, inPlace=True)

        if np.max(self._beam.cem.imageArray) <= 0:
            meanWET = self._meanWETOfTarget()
            cemArray = energyToRangeMM(self._beam.layers[0].nominalEnergy) - meanWET

            cropMask = np.sum(self._targetMaskBEV.imageArray, 2)

            cemArray[np.logical_not(cropMask.astype(bool))] = 0

            self._beam.cem.imageArray = cemArray

    def _setCEMInBeam(self, cem: CEM):
        self._beam.cem.imageArray = cem.imageArray

    def _meanWETOfTarget(self) -> np.ndarray:
        ctBEV = CTBEVWithCEM(self._ctBEV)
        ctBEV.update([self._beam.flashRangeShifter], self.ctCalibration)

        rspPBEV = RSPImage.fromCT(ctBEV, self.ctCalibration)

        wepl = np.cumsum(rspPBEV.imageArray, axis=2) * rspPBEV.spacing[2]

        wepl[np.logical_not(self._targetMaskBEV.imageArray)] = np.nan
        weplMean = np.nanmean(wepl, axis=2)

        weplMean[np.isnan(weplMean)] = 0

        return weplMean

    def _maxCEMWET(self) -> np.ndarray:
        ctBEV = CTBEVWithCEM(self._ctBEV)
        ctBEV.update([self._beam.flashRangeShifter], self.ctCalibration)

        rspPBEV = RSPImage.fromCT(ctBEV, self.ctCalibration)

        wepl = np.cumsum(rspPBEV.imageArray, axis=2) * rspPBEV.spacing[2]

        wepl[np.logical_not(self._targetMaskBEV.imageArray)] = np.nan
        weplMin = np.nanmin(wepl)

        return energyToRangeMM(self._beam[0].nominalEnergy) - weplMin

    def _initializePlan(self):
        planInitializer = PlanInitializer(beamInitializer=BeamInitializerDicom(nominalEnergy=self._plan[0][0].nominalEnergy))
        planInitializer.ctCalibration = self.ctCalibration
        planInitializer.plan = self._plan
        planInitializer.targetMask = self._targetMask
        planInitializer.ct = self._ct
        planInitializer.initializePlan(spotSpacing=self.spotSpacing, layerSpacing=None, targetMargin = self.targetMargin)
        assert len(self._plan.beams)==1
        assert len(self._plan[0].layers)==1
        self._plan.reorderPlan()
        self._beam = self._plan[0] # to be sure

    def _gd(self, currentCEM: CEM, w0=None) -> Tuple[np.ndarray, CEM]:
        doseCalculator: CEMBEVDoseCalculator = self._newObjectives.objectiveTerms[0].doseCalculator
        cemBIS = copy.deepcopy(currentCEM)
        cemBIS.imageArray = np.zeros(currentCEM.gridSize)
        self._newObjectives.cem = cemBIS
        fVal = self._newObjectives.getValue(self._plan.spotMUs)

        self._preCondDC = CEMBEVPreconditioningDoseCalculator()
        self._preCondDC.beam = self._beam
        self._preCondDC.ctCalibration = doseCalculator.ctCalibration
        self._preCondDC.patient = doseCalculator.patient
        self._preCondDC.beamModel = doseCalculator.beamModel
        self._preCondDC.nbPrimaries = doseCalculator.nbPrimaries
        self._preCondDC.targetMaskBEV = self._targetMaskBEV
        self._preCondDC.ctBEV = self._ctBEV
        self._preCondDC.wetStep = 10
        self._preCondDC.scoringVoxelSpacing = doseCalculator.scoringVoxelSpacing
        self._preCondDC.scoringGridSize = doseCalculator.scoringGridSize
        self._preCondDC.scoringOrigin = doseCalculator.scoringOrigin

        for obj in self._newObjectives.objectiveTerms:
            obj.doseCalculator = self._preCondDC

        planIMPT = self._preCondDC.getIMPTPlan()
        w0 = planIMPT.spotMUs

        PlanOptimizer.run(self._newObjectives, planIMPT, w0=w0)

        dose = self._preCondDC.computeDose(planIMPT.spotMUs, None)
        dose = imageTransformBEV.gantry0ToBEV(dose)
        dose = imageTransform3D.iecGantryToDicom(dose, self._beam)
        dose.name = 'mother dose'
        # dose.patient = self._ctBEV.patient

        self._plan.spotMUs = self._preCondDC.getFLASHWeights(planIMPT.spotMUs)
        self._newObjectives.cem = currentCEM

        for obj in self._newObjectives.objectiveTerms:
            obj.doseCalculator = doseCalculator

        if self.bounds[0] == 0 :
            optimizePlan = False
        else:
            # Filter spots
            num_spots_before_simplify = self._plan.numberOfSpots
            ind_to_keep = self._plan.spotMUs >= self.bounds[0]
            self._newObjectives.objectiveTerms[0].doseCalculator._beamlets._sparseBeamlets = self._newObjectives.objectiveTerms[0].doseCalculator._beamlets._sparseBeamlets[:,ind_to_keep]
            self._plan.simplify(threshold=self.bounds[0]) # remove spots below self.bounds[0]
            print(f"Number of spots removed after filtering: {num_spots_before_simplify - self._plan.numberOfSpots}")
            optimizePlan = True

        doubleNbPrimaries = True
        for i in range(self.maxIterations):
            if self._useNewObjectives:
                self._objectives = self._newObjectives

                doseCalculator: CEMBEVDoseCalculator = self._objectives.objectiveTerms[0].doseCalculator
                self._objectives.cem = currentCEM

                if optimizePlan:
                    PlanOptimizer.run(self._objectives, self._plan, w0=self._plan.spotMUs, bounds=self.bounds)

                fVal = self._objectives.getValue(self._plan.spotMUs)
                print('Current fVal: ' + str(fVal))

                self.planUpdateEvent.emit(self._plan)
                doseImage = doseCalculator.computeDoseGantry0(self._plan.spotMUs, self._objectives.cem)

                self.doseBEVUpdateEvent.emit(imageTransformBEV.gantry0ToBEV(doseImage))

                self._useNewObjectives = False

            if self._abort:
                raise AbortedException()

            fValPrev = fVal
            prevCEMArray = np.array(currentCEM.imageArray)
            spotMUsPrev = np.array(self._plan.spotMUs)

            self._iteration = i

            # CEM opti --> 1 iteration
            direction = self._objectives.getCEMDerivative(self._plan.spotMUs)

            newArray = currentCEM.imageArray + self._step(direction) * direction.imageArray
            newArray[newArray <= self.minCEMThickness * currentCEM.rsp] = self.minCEMThickness * currentCEM.rsp
            maxWET = self._maxCEMWET() + 10  # 5 is a margin
            newArray[newArray > maxWET] = maxWET
            currentCEM.imageArray = newArray

            optimizePlan = True # TEST!!!!!!!!!
            # Spot optimization
            if optimizePlan:
                PlanOptimizer.run(self._objectives, self._plan, w0=self._plan.spotMUs, bounds=self.bounds) # access to beamlets via self._objectives.doseCalculator

            fVal = self._objectives.getValue(self._plan.spotMUs)
            print('Current fVal: ' + str(fVal))
            self.fValEvent.emit((self._iteration, fVal))
            self.planUpdateEvent.emit(self._plan)
            doseImage = doseCalculator.computeDoseGantry0(self._plan.spotMUs, self._objectives.cem)
            self.doseBEVUpdateEvent.emit(imageTransformBEV.gantry0ToBEV(doseImage))

            # if (fValPrev - fVal) < self.absTol and not optimizePlan:
            #     optimizePlan = True
            #     continue

            if (fValPrev - fVal) > 0 and (fValPrev - fVal) < self.absTol:
                break

            if (fValPrev - fVal) > 0 and (fValPrev - fVal) < 2*self.absTol and doubleNbPrimaries:
                doseCalculator.nbPrimaries *= 2
                doubleNbPrimaries = False

            if self._iteration > 2 and (fValPrev - fVal) < 0:
                self._plan.spotMUs = spotMUsPrev
                self._objectives.cem.imageArray = prevCEMArray

                fVal = fValPrev
                currentCEM.imageArray = prevCEMArray

                if self.enableMCDerivative:
                    doseCalculator.derivativeMode = doseCalculator.DerivativeModes.MC  # TEST!!!

                self._maxStep = self._maxStep / 2.

                if self._maxStep < 1.:
                    break

        return self._plan.spotMUs, currentCEM

    def _step(self, direction: CEM) -> float:
        percCEMVal = self._percCEMVal(direction, 90)
        if percCEMVal == 0:
            raise Exception('Derivative is zero everywhere')
        return self._maxStep / percCEMVal

    def _percCEMVal(self, cem: CEM, perc: float) -> float:
        nonNullValInd = np.sum(self._targetMaskBEV.imageArray, axis=2).astype(bool)
        nonNullValInd = ndimage.binary_erosion(nonNullValInd, structure=np.ones((5, 5))).astype(bool)
        return np.percentile(np.abs(cem.imageArray[nonNullValInd]), perc)

    def _maxCEMVal(self, cem: CEM) -> float:
        nonNullValInd = np.sum(self._targetMaskBEV.imageArray, axis=2).astype(bool)
        nonNullValInd = ndimage.binary_erosion(nonNullValInd, structure=np.ones((5, 5))).astype(bool)
        return np.max(np.abs(cem.imageArray[nonNullValInd]))

    def _medianCEMVal(self, cem: CEM) -> float:
        nonNullValInd = np.sum(self._targetMaskBEV.imageArray, axis=2).astype(bool)
        nonNullValInd = ndimage.binary_erosion(nonNullValInd, structure=np.ones((5, 5))).astype(bool)
        return np.median(np.abs(cem.imageArray[nonNullValInd]))
