import logging
from typing import Tuple, Optional

import numpy as np
from opentps.core import Event
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import ROIMask
from opentps.core.data.plan import RTPlan, PlanIonLayer
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.cem import Aperture
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.processing.CEMOptimization import cemObjectives
# from opentps_flash.core.processing.planInitializer import BeamInitializerDicom, PlanInitializer
from opentps.core.processing.planOptimization.planInitializer import PlanInitializer
from opentps_flash.core.processing.CEMOptimization.planOptimizer import PlanDoseCalculator, PlanOptimizer, PlanOptimizerObjectives
from opentps_flash.core.processing.Utils import objectivesPreprocessing
from opentps_flash.core.processing.CEMOptimization import planObjectives
from typing import Union, Sequence, Optional

logger = logging.getLogger(__name__)

class IMPTOptimizationWorkflow:
    def __init__(self):
        self.ctCalibration:AbstractCTCalibration = None
        self.beamModel = None
        self.gantryAngle = 0
        self.beamEnergy = 226
        self.ct = None
        self.objectives: PlanOptimizerObjectives = None
        self.spotSpacing = 5.
        self.layerSpacing = 5.
        self.bounds = (0.,9999.) # optimization bounds on MUs

        # Aperture options
        self.apertureToIsocenter = 100
        self.apertureDensity = 8.5
        self.apertureThickness = 60
        self.apertureClearance = 0
        self.apertureLatThickness = 30

        self.targetMargin = 2.

        self.doseUpdateEvent = Event(object)
        self.planUpdateEvent = Event(RTPlan)
        self.targetUpdateEvent = Event(ROIMask)
        self.fValEvent = Event(Tuple)

        self._plan = None
        self._targetROIBEV = None
        # self._globalROIBEV = None
        self._targetROI = None
        # self._globalROI = None
        self._finalDose = None
        self._ctBEV = None
        self._doseCalculator:PlanDoseCalculator = None

        # Scoring grid
        self.scoringVoxelSpacing = None
        self.scoringGridSize = None
        self.scoringOrigin = None
        self.adapt_gridSize_to_new_spacing = True

    def _handleDoseBEVUpdate(self, doseBEV):
        doseImage = imageTransform3D.iecGantryToDicom(doseBEV, self._plan[0]) #, cropROI=self._globalROI)
        resampler3D.resampleImage3DOnImage3D(doseImage, self.ct, inPlace=True)
        self.doseUpdateEvent.emit(doseImage)
    
    @property
    def finalDose(self):
        return self._finalDose

    @property
    def finalPlan(self):
        return self._plan
    
    def runPrepareOptimization(self, beam:Optional[CEMBeam]=None):
        patient = self.ct.patient

        self._computeTargetAndGlobalROIs() # in dicom

        self._initializePlan(beam) # create CEMBEam with emply layer
        self._initializeCTAndROIsBEV() # compute and store CT, globalROI, targetROI in BEV

        beam = self._plan.beams[0]

        self._plan.patient = patient
        self.planUpdateEvent.emit(self._plan)

        self._ctBEV.name = 'CT BEV'
        self._ctBEV.patient = patient

        self._initializeCTObjects() # Create and store BiComponentCEMBEV in beam.cem and aperture in beam.aperture

        self._spotPlacement()

        self._initializeDoseCalculator() # Create and store CEMBEVDoseCalculator in object

        self.setOptimizerObjectives() # Create and store CEMObjectives in cemOptimizer

        self.targetUpdateEvent.emit(self._targetROI)

    def run(self, beam:Optional[CEMBeam]=None) -> RTPlan:
        self.runPrepareOptimization(beam)

        # Optimize plan
        self.runOptimization()

        # Final dose
        self._computeFinalDose()
        self.doseUpdateEvent.emit(self._finalDose)

        return self._plan

    def _computeTargetAndGlobalROIs(self):
        """
        Compute and store Target ROI and Union of all other ROIs (target + OARs).
        Global ROI mask is then dilated by 3*spacing
        """
        self._targetROI, self._globalROI = objectivesPreprocessing.getTargetAndGlobalROIsFromObjectives(self.ct, self.objectives.objectiveTerms)

        # cropROIMargin = self.apertureLatThickness + self.apertureClearance
        # self._globalROI.dilateMask(radius=cropROIMargin)


    def _initializePlan(self, beam:Optional[CEMBeam]=None):
        """
        Initialize plan with a CEMBeam object that contains an empty layer
        """
        self._plan = RTPlan()
        if beam is None:
            beam = CEMBeam()
            beam.isocenterPosition = self._targetROI.centerOfMass
            beam.gantryAngle = self.gantryAngle
            layer = PlanIonLayer(nominalEnergy=self.beamEnergy)
            beam.appendLayer(layer)
        self._plan.appendBeam(beam)

    def _spotPlacement(self):
        # Plan initializer
        planInitializer = PlanInitializer()
        planInitializer.ctCalibration = self.ctCalibration
        planInitializer.ct = self.ct
        planInitializer.plan = self._plan
        planInitializer.targetMask = self._targetROI
        planInitializer.placeSpots(spotSpacing=self.spotSpacing, layerSpacing=self.layerSpacing, targetMargin = self.targetMargin)
        self._plan.reorderPlan()

    def _initializeCTAndROIsBEV(self):
        """
        Compute and store the CT in BEV in self._ctBEV, globalROI in BEV (self._globalROIBEV) and targetROI in BEV (self._targetROIBEV)
        """
        self._ctBEV = imageTransform3D.dicomToIECGantry(self.ct, self._plan[0], fillValue=-1024)
        self._targetROIBEV = imageTransform3D.dicomToIECGantry(self._targetROI, self._plan[0], fillValue=0)

    def _initializeCTObjects(self):
        """
        Create CEM, apeture and RS, and add them to plan
        """

        beam = self._plan.beams[0]

        if beam.aperture is None:
            apertureRSP = self.ctCalibration.convertMassDensity2RSP(self.apertureDensity)
            aperture = Aperture.fromMaskBEV(self._targetROIBEV, beam, apertureRSP*self.apertureThickness, lateralThickness=self.apertureLatThickness, clearance=self.apertureClearance)
            aperture.rsp = apertureRSP
            aperture.spikeOrientation = aperture.SpikeOrientations.FACING_NOZZLE
            aperture.baseToIsocenter = self.apertureToIsocenter
            beam.aperture = aperture
        else:
            beam.aperture = resampler3D.resampleImage3DOnImage3D(beam.aperture, self._ctBEV, inPlace=True)

        apertureROI = imageTransform3D.iecGantryToDicom(aperture.computeROIBEV(self._ctBEV), beam)
        apertureROI = resampler3D.resampleImage3DOnImage3D(apertureROI, self.ct)
        self.ct.imageArray[apertureROI.imageArray] = self.ctCalibration.convertMassDensity2HU(self.apertureDensity)

    def _initializeDoseCalculator(self):
        """
        Create and store CEMBEVDoseCalculator in object
        """
        # Dose calculator
        self._doseCalculator = PlanDoseCalculator()
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ct = self.ct
        self._doseCalculator.nbPrimaries = 1e5
        self._doseCalculator.plan = self._plan
        self._doseCalculator.patient = self.ct.patient
        self._doseCalculator.setScoringParameters(scoringGridSize=self.scoringGridSize, scoringSpacing=self.scoringVoxelSpacing,
                                                  scoringOrigin=self.scoringOrigin, adapt_gridSize_to_new_spacing=self.adapt_gridSize_to_new_spacing)



    def setOptimizerObjectives(self):
        for objective in self.objectives.objectiveTerms:
            objective.doseCalculator = self._doseCalculator
    
    def resampleOptimizerObjectivesToScoringGrid(self):
        # check if scoring grid different
        for objective in self.objectives.objectiveTerms:
            objective._updateMaskVec(spacing=self.scoringVoxelSpacing, gridSize=self.scoringGridSize, origin=self.scoringOrigin)

    def runOptimization(self):
        # Compute beamlets
        beamlets = self._doseCalculator.computeBeamlets()

        # Resample ROI of objectives to scoring grid if necessary
        self.resampleOptimizerObjectivesToScoringGrid()

        if self.bounds[0]==0:
            PlanOptimizer.run(self.objectives, self._plan, np.ones_like(self._plan.spotMUs), bounds=self.bounds)
        else:
            PlanOptimizer.run(self.objectives, self._plan, np.ones_like(self._plan.spotMUs), bounds=(0,self.bounds[1]))
            logger.info(f"Number of spots after 1st opti: {self._plan.numberOfSpots}")
            ind_to_keep = self._plan.spotMUs >= self.bounds[0]
            beamlets._sparseBeamlets = beamlets._sparseBeamlets[:,ind_to_keep]
            self._plan.simplify(threshold=self.bounds[0]) # remove spots below self.bounds[0]
            logger.info(f"Number of spots before 2nd opti: {self._plan.numberOfSpots}")
            PlanOptimizer.run(self.objectives, self._plan, self._plan.spotMUs, bounds=self.bounds)

    def _computeFinalDose(self):
        # Compute final dose
        self._doseCalculator._doseCalculator.nbPrimaries = 2e7
        self._finalDose = self._doseCalculator._doseCalculator.computeDose(self.ct, self._plan)
        self._finalDose.name = "Final dose"
        self._finalDose.patient = self.ct.patient
