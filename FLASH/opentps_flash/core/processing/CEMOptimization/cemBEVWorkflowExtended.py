from opentps_flash.core.processing.CEMOptimization.cemBEVWorkflow import SingleBeamCEMBEVOptimizationWorkflow

from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.aperture import ApertureBEV
from opentps_flash.core.data.cem import BiComponentCEMBEV, CEM
from opentps_flash.core.processing.Utils import objectivesPreprocessing
from opentps_flash.core.processing.planInitializer import BeamInitializerBEV
class SingleBeamCEMBEVOptimizationWorkflowExtended(SingleBeamCEMBEVOptimizationWorkflow):
    def __init__(self):
        super().__init__()

        self.aperture = None
        self.aperture_length = None
        self.aperture_lateral_thickness = 10.
        self.distance_aperture_isocenter = None
        self.rangeShifterToCEM_distance = None # keep RSToIsoDist or this one
        self.rangeShifterLateralMargin = 15
        self.spikeOrientation =  CEM.SpikeOrientations.FACING_NOZZLE
        self.componentOrder = BiComponentCEMBEV.ComponentOrdering.NOZZLE_CEM_RS
        self.cemToIsocenterType = CEM.CEMToIsocenterTypes.DISTAL_TO_ISOCENTER
        self.apertureToIsocenterType = CEM.CEMToIsocenterTypes.PROXIMAL_TO_ISOCENTER
        self._cemOptimizer._beamInitializer.gridType = BeamInitializerBEV.GridTypes.HEXAGONAL

    @property
    def gridType(self):
        return self._cemOptimizer._beamInitializer.gridType

    @gridType.setter
    def gridType(self, type: BeamInitializerBEV.GridTypes):
        self._cemOptimizer._beamInitializer.gridType = type

    def _initializeCTObjects(self):
        """
        Create and store BiComponentCEMBEV in beam.cem and aperture in beam.aperture
        """
        beam = self._plan.beams[0]
        cem = BiComponentCEMBEV.fromCTBEV(self._ctBEV, beam, targetMaskBEV=self._targetROIBEV)

        cem.cemToIsocenterType = self.cemToIsocenterType
        cem.spikeOrientation = self.spikeOrientation

        cem.cemRSP = self.cemRSP
        cem.rangeShifterRSP = self.rangeShifterRSP
        if self.rangeShifterToCEM_distance is not None:
            cem.rangeShifterToCEM = self.rangeShifterToCEM_distance # Free space between CEM and RS
        cem.rangeShifterLateralMargin = self.rangeShifterLateralMargin
        cem.componentOrder = self.componentOrder
        beam.cem = cem

        if self.aperture is not None:
            beam.aperture = self.aperture
        else:
            aperture = ApertureBEV.fromCTBEV(self._ctBEV, beam, self._targetROIBEV, lateralThickness=self.aperture_lateral_thickness, clearance=self.apertureClearance)
            aperture.rsp = self.apertureRSP
            aperture.cemToIsocenterType = self.apertureToIsocenterType
            if self.aperture_length is None:
                aperture.wet = energyToRangeMM(self.beamEnergy) + 10.  # 10 is a margin
            else:
                aperture.wet = self.aperture_length * aperture.rsp
            if self.distance_aperture_isocenter is not None:
                aperture._referenceBeam.apertureToIsocenter = self.distance_aperture_isocenter + aperture.wet / aperture.rsp # aperture proximal part to isocenter distance 
            beam.aperture = aperture


    def _computeTargetAndGlobalROIs(self):
        """
        Compute and store Target ROI and Union of all other ROIs (target + OARs).
        Global ROI mask is then dilated by aperture lateral thickness + clearance
        """
        self._targetROI, self._globalROI = objectivesPreprocessing.getTargetAndGlobalROIsFromObjectives(self.ct, [obj.objectiveTerm for obj in self.objectives])

        cropROIMargin = self.aperture_lateral_thickness + self.apertureClearance
        self._globalROI.dilateMask(radius=cropROIMargin)