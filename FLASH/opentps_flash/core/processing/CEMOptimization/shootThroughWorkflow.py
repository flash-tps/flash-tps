import logging
from typing import Tuple, Optional
import copy

import numpy as np
from opentps.core import Event
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import ROIMask
from opentps.core.data.plan import RTPlan, PlanIonLayer
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.cem import Aperture
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.processing.CEMOptimization import cemObjectives
from opentps_flash.core.processing.planInitializer import BeamInitializerDicom, PlanInitializer
from opentps_flash.core.processing.CEMOptimization.planOptimizer import PlanDoseCalculator, PlanOptimizer, PlanOptimizerObjectives
from opentps_flash.core.processing.Utils import objectivesPreprocessing, imagePreprocessing
from opentps_flash.core.processing.CEMOptimization import planObjectives
from typing import Union, Sequence, Optional
from opentps_flash.core.processing import imageTransformBEV

logger = logging.getLogger(__name__)

class ShootThroughOptimizationWorkflow:
    def __init__(self):
        self.ctCalibration:AbstractCTCalibration = None
        self.beamModel = None
        self.gantryAngle = 0
        self.beamEnergy = 226
        self.ct = None
        self.objectives: PlanOptimizerObjectives = None
        self.spotSpacing = 5.
        self.bounds = (0.,9999.) # optimization bounds on MUs
        self._scoringVoxelSpacing = None
        self.gridType = BeamInitializerDicom.GridTypes.HEXAGONAL

        # Aperture options
        self.enableAperture = True
        self.apertureToIsocenter = 100
        self.apertureDensity = 8.5
        self.apertureThickness = 60
        self.apertureClearance = 0
        self.apertureLatThickness = 30

        self.targetMargin = 2.

        self.doseUpdateEvent = Event(object)
        self.planUpdateEvent = Event(RTPlan)
        self.targetUpdateEvent = Event(ROIMask)
        self.fValEvent = Event(Tuple)

        self.nbPrimariesPerBeamlet = 1e5
        self.nbPrimariesFinalDose = 2e7

        self._plan = None
        self._targetROIBEV = None
        # self._globalROIBEV = None
        self._targetROI = None
        # self._globalROI = None
        self._finalDose = None
        self._ctBEV = None
        self._doseCalculator:PlanDoseCalculator = None

        # Scoring grid
        self.scoringVoxelSpacing = None
        self.scoringGridSize = None
        self.scoringOrigin = None
        self.adapt_gridSize_to_new_spacing = True

    def _handleDoseBEVUpdate(self, doseBEV):
        doseImage = imageTransform3D.iecGantryToDicom(doseBEV, self._plan[0]) #, cropROI=self._globalROI)
        resampler3D.resampleImage3DOnImage3D(doseImage, self.ct, inPlace=True)
        self.doseUpdateEvent.emit(doseImage)
    
    @property
    def finalDose(self):
        return self._finalDose

    @property
    def finalPlan(self):
        return self._plan
    
    def runPrepareOptimization(self, beam:Optional[CEMBeam]=None):
        patient = self.ct.patient

        self._computeTargetAndGlobalROIs() # in dicom

        self._initializePlan(beam) # create CEMBEam with emply layer
        self._initializeCTAndROIsBEV() # compute and store CT, globalROI, targetROI in BEV

        beam = self._plan.beams[0]

        self._plan.patient = patient
        self.planUpdateEvent.emit(self._plan)

        if self.enableAperture:
            self._initializeCTObjects() # Create and store BiComponentCEMBEV in beam.cem and aperture in beam.aperture
        
        self._ctBEV.name = 'CT BEV'
        # self._ctBEV.patient = patient

        self._spotPlacement()

        planGantry0 = RTPlan()
        beamG0 = copy.deepcopy(beam)
        beamG0.gantryAngle = 0.
        beamG0.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, beam.isocenterPosition)
        planGantry0.appendBeam(beamG0)

        self.planGantry0 = planGantry0

        self._initializeDoseCalculator() # Create and store CEMBEVDoseCalculator in object

        self.setOptimizerObjectives() # Create and store CEMObjectives in cemOptimizer

        self.targetUpdateEvent.emit(self._targetROI)

    def run(self, beam:Optional[CEMBeam]=None) -> RTPlan:
        self.runPrepareOptimization(beam)

        # Optimize plan
        self.runOptimization()

        # Final dose
        self._computeFinalDose()
        self.doseUpdateEvent.emit(self._finalDose)

        return self._plan
    
    def enableApertureAndComputeDose(self):
        # TODO: Modify to compute dose in Gantry0
        self._initializeCTObjects()
        self._doseCalculator._doseCalculator.nbPrimaries = self.nbPrimariesFinalDose
        doseWithAperture = self._doseCalculator._doseCalculator.computeDose(self.ct, self._plan)
        doseWithAperture.name = "Dose with aperture"
        doseWithAperture.patient = self.ct.patient
        return doseWithAperture


    def _computeTargetAndGlobalROIs(self):
        """
        Compute and store Target ROI and Union of all other ROIs (target + OARs).
        Global ROI mask is then dilated by 3*spacing
        """
        self._targetROI, self._globalROI = objectivesPreprocessing.getTargetAndGlobalROIsFromObjectives(self.ct, self.objectives.objectiveTerms)


    def _initializePlan(self, beam:Optional[CEMBeam]=None):
        """
        Initialize plan with a CEMBeam object that contains an empty layer
        """
        self._plan = RTPlan()
        if beam is None:
            beam = CEMBeam()
            beam.isocenterPosition = self._targetROI.centerOfMass
            beam.gantryAngle = self.gantryAngle
            layer = PlanIonLayer(nominalEnergy=self.beamEnergy)
            beam.appendLayer(layer)
        self._plan.appendBeam(beam)

    def _spotPlacement(self):
        # Plan initializer
        planInitializer = PlanInitializer(beamInitializer=BeamInitializerDicom(nominalEnergy=self.beamEnergy, gridType=self.gridType))
        planInitializer.ctCalibration = self.ctCalibration
        plan = RTPlan()
        plan.appendBeam(self._plan[0])
        planInitializer.plan = plan
        planInitializer.targetMask = self._targetROI
        planInitializer.ct = self.ct
        planInitializer.initializePlan(spotSpacing=self.spotSpacing, layerSpacing=None, targetMargin = self.targetMargin)
        assert self._plan[0][0].nominalEnergy == self.beamEnergy
        assert len(self._plan.beams)==1
        assert len(self._plan[0].layers)==1
        self._plan.reorderPlan()

    def _initializeCTAndROIsBEV(self):
        """
        Compute and store the CT in BEV in self._ctBEV, globalROI in BEV (self._globalROIBEV) and targetROI in BEV (self._targetROIBEV)
        """
        self._globalROIBEV = imageTransform3D.dicomToIECGantry(self._globalROI, self._plan[0], fillValue=0,
                                                               cropDim0=True, cropDim1=True, cropDim2=False)

        self._targetROIBEV = imageTransform3D.dicomToIECGantry(self._targetROI, self._plan[0], fillValue=0,
                                                               cropDim0=True, cropDim1=True, cropDim2=False)

        self._ctBEV = imageTransform3D.dicomToIECGantry(self.ct, self._plan.beams[0], fillValue=-1024.,
                                                        cropDim0=True, cropDim1=True, cropDim2=False)
        
    def _initializeCTObjects(self):
        """
        Create CEM, apeture and RS, and add them to plan
        """

        beam = self._plan.beams[0]

        if beam.aperture is None:
            apertureRSP = self.ctCalibration.convertMassDensity2RSP(self.apertureDensity)
            aperture = Aperture.fromMaskBEV(self._targetROIBEV, beam, apertureRSP*self.apertureThickness, lateralThickness=self.apertureLatThickness, clearance=self.apertureClearance)
            aperture.rsp = apertureRSP
            aperture.spikeOrientation = aperture.SpikeOrientations.FACING_NOZZLE
            aperture.baseToIsocenter = self.apertureToIsocenter
            beam.aperture = aperture
        else:
            beam.aperture = resampler3D.resampleImage3DOnImage3D(beam.aperture, self._ctBEV, inPlace=True)

        self._ctBEV = imagePreprocessing.padImageForDevice(self._ctBEV, beam, aperture)
        apertureBEV = aperture.computeROIBEV(self._ctBEV)
        self._ctBEV.imageArray[apertureBEV.imageArray] = self.ctCalibration.convertMassDensity2HU(self.apertureDensity)

        self.ctCEM = imageTransform3D.iecGantryToDicom(self._ctBEV, self._plan.beams[0], fillValue=-1000, cropDim0=True, cropDim1=False, cropDim2=True)
        self.ctCEM.name = 'CT with CEM'
        self.ctCEM.patient = self.ct.patient

        ctGantry0 = imageTransformBEV.bevToGantry0(self._ctBEV)
        self.ctGantry0 = ctGantry0
        self.ctGantry0.name = 'CT Gantry0'
        # self.ctGantry0.patient = self.ct.patient

    def _initializeDoseCalculator(self):
        """
        Create and store CEMBEVDoseCalculator in object
        """
        # Dose calculator
        self._doseCalculator = PlanDoseCalculator()
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ct = self.ctGantry0
        self._doseCalculator.nbPrimaries = self.nbPrimariesPerBeamlet
        self._doseCalculator.plan = self.planGantry0
        self._doseCalculator.roi = imageTransformBEV.bevToGantry0(self._globalROIBEV)
        # TODO: Scoring parameters are based on Gantry0 space, it is not obvious that it is the case
        self._doseCalculator.setScoringParameters(scoringGridSize=self.scoringGridSize, scoringSpacing=self.scoringVoxelSpacing,
                                                  scoringOrigin=self.scoringOrigin, adapt_gridSize_to_new_spacing=self.adapt_gridSize_to_new_spacing)



    def setOptimizerObjectives(self):
        for objective in self.objectives.objectiveTerms:
            objective.doseCalculator = self._doseCalculator
            roiBEV = imageTransform3D.dicomToIECGantry(objective.roi, self._plan[0], cropROI=self._globalROI, cropDim0=True, cropDim1=True, cropDim2=False)
            resampler3D.resampleImage3DOnImage3D(roiBEV, self._ctBEV, inPlace=True) # To be sure
            objective.roi = imageTransformBEV.bevToGantry0(roiBEV)
    
    def resampleOptimizerObjectivesToScoringGrid(self):
        for objective in self.objectives.objectiveTerms:
            objective._updateMaskVec(spacing=self._doseCalculator.scoringVoxelSpacing, gridSize=self._doseCalculator.scoringGridSize, origin=self._doseCalculator.scoringOrigin)

    def runOptimization(self):
        # Compute beamlets
        beamlets = self._doseCalculator.computeBeamlets()

        # Resample ROI of objectives to scoring grid if necessary
        self.resampleOptimizerObjectivesToScoringGrid()

        if self.bounds[0]==0:
            PlanOptimizer.run(self.objectives, self.planGantry0, np.ones_like(self.planGantry0.spotMUs), bounds=self.bounds)
        else:
            PlanOptimizer.run(self.objectives, self.planGantry0, np.ones_like(self.planGantry0.spotMUs), bounds=(0,self.bounds[1]))
            logger.info(f"Number of spots after 1st opti: {self.planGantry0.numberOfSpots}")
            ind_to_keep = self.planGantry0.spotMUs >= self.bounds[0]
            beamlets._sparseBeamlets = beamlets._sparseBeamlets[:,ind_to_keep]
            self.planGantry0.simplify(threshold=self.bounds[0]) # remove spots below self.bounds[0]
            logger.info(f"Number of spots before 2nd opti: {self.planGantry0.numberOfSpots}")
            PlanOptimizer.run(self.objectives, self.planGantry0, self.planGantry0.spotMUs, bounds=self.bounds)
        
        beam_iso = self._plan.beams[0].isocenterPosition.copy()
        self._plan._beams[0] = copy.deepcopy(self.planGantry0[0]) #self.planGantry0
        self._plan._beams[0].isocenterPosition = beam_iso


    def _computeFinalDose(self):
        # Compute final dose
        self._doseCalculator._doseCalculator.nbPrimaries = self.nbPrimariesFinalDose
        doseG0 = self._doseCalculator._doseCalculator.computeDose(self.ctGantry0, self.planGantry0)
        if self.scoringVoxelSpacing is not None:
            # If we do not resample the dose to ct Gantry0, there are sometimes issues when converting to BEV
            resampler3D.resampleImage3DOnImage3D(doseG0, self.ctGantry0, inPlace=True)
        doseG0.name = 'dose gantry 0'
        # doseG0.patient = self.ct.patient
        self.planGantry0.name = "plan g0"
        # self.planGantry0.patient = self.ct.patient
        doseImageBEV = imageTransformBEV.gantry0ToBEV(doseG0)
        self._finalDose = imageTransform3D.iecGantryToDicom(doseImageBEV, self._plan.beams[0])
        self._finalDose.name = "Final dose"
        self._finalDose.patient = self.ct.patient
