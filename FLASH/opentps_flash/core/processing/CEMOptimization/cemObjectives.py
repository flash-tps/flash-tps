import copy
from abc import abstractmethod
from typing import Union, Sequence
import scipy.sparse as sp

import numpy as np
from opentps.core.data.images import ROIMask, DoseImage, Image3D
from opentps.core.data._roiContour import ROIContour
from opentps_flash.core.data.cem import CEM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.DoseCalculation.fluenceBasedMCsquareDoseCalculator import Beamlets
from opentps.core.processing.imageProcessing import resampler3D


class CEMAbstractDoseFidelityTerm:
    def __init__(self, roi:ROIMask=None):
        self.roi = roi
        self.maskVec = None
        self._doseCalculator = None

    def abort(self):
        self._doseCalculator.kill()

    def _updateMaskVec(self, spacing:Sequence[float], gridSize:Sequence[int], origin:Sequence[float]):
        if isinstance(self.roi, ROIContour):
            mask = self.roi.getBinaryMask(origin=origin, gridSize=gridSize, spacing=spacing)
        elif isinstance(self.roi, ROIMask):
            mask = self.roi
            if not (np.array_equal(mask.gridSize, gridSize) and
                np.allclose(mask.origin, origin, atol=0.01) and
                np.allclose(mask.spacing, spacing, atol=0.01)):
                mask = resampler3D.resampleImage3D(self.roi, gridSize=gridSize, spacing=spacing, origin=origin)
        else:
            raise Exception(self.roi.__class__.__name__ + ' is not a supported class for roi')

        self.maskVec = np.flip(mask.imageArray, (0, 1))
        self.maskVec = np.ndarray.flatten(self.maskVec, 'F').astype('bool')


    @property
    def doseCalculator(self):
        return self._doseCalculator

    @doseCalculator.setter
    def doseCalculator(self, dc):
        self._doseCalculator = dc

    @abstractmethod
    def getValue(self, weights: np.ndarray, cem:CEM) -> float:
        raise NotImplementedError()

    @abstractmethod
    def getCEMDerivative(self, weights:np.ndarray, cem:CEM) -> np.ndarray:
        raise NotImplementedError()

    @abstractmethod
    def getWeightDerivative(self, weights:np.ndarray, cem:CEM) -> np.ndarray:
        raise NotImplementedError()

    def _multiplyWithDerivative(self, diffGantry0: DoseImage, derivativeG0: Union[Beamlets, DoseImage]) -> np.ndarray:
        if isinstance(derivativeG0, Beamlets):
            return self._multiplyWithDerivative_Beamlets(diffGantry0, derivativeG0)
        elif isinstance(derivativeG0, Image3D):
            return self._multiplyWithDerivative_DoseImage(diffGantry0, derivativeG0)
        else:
            raise ValueError('Derivative cannot be of type ' + str(type(derivativeG0)))

    def _multiplyWithDerivative_Beamlets(self, diffGantry0: DoseImage, derivative: Beamlets) -> np.ndarray:
        diffVal = diffGantry0.imageArray

        diffVal = np.flip(diffVal, 1)
        diffVal = np.flip(diffVal, 0)
        diffVal = diffVal.flatten(order='F')
        diffVal = np.transpose(diffVal)

        derivativeMat = derivative.sparseBeamlets.toSparseMatrix()
        derivativePlan = derivative.referencePlan
        originalBeam = self.doseCalculator.beam

        productRes = diffVal @ derivativeMat

        productInd = 0
        beam = derivativePlan[0]
        beamSubproduct = np.zeros(originalBeam.cem.imageArray.shape)

        ctBEV = imageTransformBEV.gantry0ToBEV(diffGantry0)
        isocenterBEV = imageTransformBEV.gantry0CoordinateToBEV(diffGantry0, beam.isocenterPosition)

        beamModel = self.doseCalculator.beamModel
        for layer in beam:
            pos0Nozzle = np.array(layer.spotX) * (
                        beamModel.smx - beamModel.nozzle_isocenter) / beamModel.smx
            pos1Nozzle = np.array(layer.spotY) * (
                        beamModel.smx - beamModel.nozzle_isocenter) / beamModel.smy

            pos0Nozzle += isocenterBEV[0]
            pos1Nozzle = isocenterBEV[1] - pos1Nozzle

            for i, pos0 in enumerate(pos0Nozzle):
                pos1 = pos1Nozzle[i]

                # TODO Is it normal that we have out of bounds?
                try:
                    vIndex = ctBEV.getVoxelIndexFromPosition([pos0, pos1, 0])
                    beamSubproduct[vIndex[0], vIndex[1]] = productRes[productInd]
                except:
                    pass
                productInd += 1

        return beamSubproduct

    def _multiplyWithDerivative_DoseImage(self, diffGantry0: DoseImage, derivative: DoseImage) -> np.ndarray:
        diffBEV = imageTransformBEV.gantry0ToBEV(diffGantry0)
        derivative = imageTransformBEV.gantry0ToBEV(derivative)

        derivativeProd = np.sum(derivative.imageArray * diffBEV.imageArray, axis=2)

        return derivativeProd


class DoseMaxObjective(CEMAbstractDoseFidelityTerm):
    def __init__(self, roi:ROIMask, maxDose:float, doseCalculator=None):
        super().__init__(roi)
        self._maxDose:float = maxDose
        self.doseCalculator = doseCalculator

    @property
    def maxDose(self):
        return self._maxDose

    @maxDose.setter
    def maxDose(self, dose):
        self._maxDose = dose

    @property
    def _roiVoxels(self):
        return np.count_nonzero(self.roi.imageArray.astype(int))
    
    def getValue(self, weights:np.ndarray, cem:CEM) -> float:
        beamlets = self.doseCalculator.computeBeamlets(cem).toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.mean(np.maximum(0, doseTotal - self.maxDose) ** 2)
        return f
    
    def getWeightDerivative(self, weights:np.ndarray, cem:CEM) -> np.ndarray:
        beamlets = self.doseCalculator.computeBeamlets(cem).toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.maximum(0, doseTotal - self.maxDose)
        df = 2 / len(f) * np.transpose(f) @ beamlets
        return df

    def getCEMDerivative(self, weights:np.ndarray, cem:CEM) -> CEM:
        doseImage = self.doseCalculator.computeDose(weights, cem)
        if not doseImage.hasSameGrid(self.roi):
            doseImage = resampler3D.resampleImage3DOnImage3D(doseImage, self.roi, inPlace=False, fillValue=0.)

        dose = np.array(doseImage.imageArray)
        dose[np.logical_not(self.roi.imageArray.astype(bool))] = 0.

        diff = np.maximum(0., dose - self.maxDose)
        diff *= 2. / self._roiVoxels

        diffImage = DoseImage.fromImage3D(doseImage, patient=None)
        diffImage.imageArray = diff

        diff = self._multiplyWithDerivative(diffImage, self.doseCalculator.computeDerivative(weights, cem))

        outCEM = copy.deepcopy(cem)
        outCEM.imageArray = diff
        return outCEM


class DoseMinObjective(CEMAbstractDoseFidelityTerm):
    def __init__(self, roi:ROIMask, minDose:float, doseCalculator=None):
        super().__init__(roi)
        self._minDose: float = minDose
        self.doseCalculator = doseCalculator

    @property
    def minDose(self):
        return self._minDose

    @minDose.setter
    def minDose(self, dose):
        self._minDose = dose

    @property
    def _roiVoxels(self):
        return np.count_nonzero(self.roi.imageArray)
    
    def getValue(self, weights:np.ndarray, cem:CEM) -> float:
        beamlets = self.doseCalculator.computeBeamlets(cem).toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.mean(np.minimum(0, doseTotal - self.minDose) ** 2)
        return f
    
    def getWeightDerivative(self, weights:np.ndarray, cem:CEM) -> np.ndarray:
        beamlets = self.doseCalculator.computeBeamlets(cem).toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.minimum(0, doseTotal - self.minDose)
        df = 2 / len(f) * np.transpose(f) @ beamlets
        return df

    def getCEMDerivative(self, weights:np.ndarray, cem:CEM) -> CEM:
        doseImage = self.doseCalculator.computeDose(weights, cem)
        if not doseImage.hasSameGrid(self.roi):
            doseImage = resampler3D.resampleImage3DOnImage3D(doseImage, self.roi, inPlace=False, fillValue=0.)

        dose = np.array(doseImage.imageArray)
        dose[np.logical_not(self.roi.imageArray.astype(bool))] = self.minDose

        diff = np.maximum(0., self.minDose-dose)
        diff *= -2. / self._roiVoxels

        diffImage = DoseImage.fromImage3D(doseImage, patient=None)
        diffImage.imageArray = diff

        diff = self._multiplyWithDerivative(diffImage, self.doseCalculator.computeDerivative(weights, cem))

        outCEM = copy.deepcopy(cem)
        outCEM.imageArray = diff
        return outCEM
