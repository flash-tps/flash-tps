from typing import Tuple, Optional

import numpy as np
from opentps.core import Event
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import ROIMask, RSPImage
from opentps.core.data.plan import RTPlan, PlanIonLayer
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D
from opentps.core.processing.rangeEnergy import energyToRangeMM
from opentps_flash.core.data.cem import CEM, Aperture, RangeShifter
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.CEMOptimization import cemObjectives
from opentps_flash.core.processing.CEMOptimization.cemBEVDoseCalculator import CEMBEVDoseCalculator
from opentps_flash.core.processing.CEMOptimization.cemBEVOptimizer import CEMBEVOptimizer
from opentps_flash.core.processing.DoseCalculation.CEMDoseCaculator import CEMDoseCalculator
from opentps_flash.core.processing.Utils import objectivesPreprocessing, imagePreprocessing
from opentps_flash.core.processing.planInitializer import BeamInitializerBEV
from typing import Union, Sequence, Optional
import logging
logger = logging.getLogger(__name__)


class Objective:
    def __init__(self, objectiveTerm=None, weight=1.):
        self.weight = weight
        self.objectiveTerm: cemObjectives.CEMAbstractDoseFidelityTerm = objectiveTerm

class SingleBeamCEMBEVOptimizationWorkflow:
    def __init__(self):
        self.ctCalibration:AbstractCTCalibration = None
        self.beamModel = None
        self.gantryAngle = 0
        self.beamEnergy = 226
        self.ct = None
        self.objectives: list[Objective] = []
        self.spotSpacing = 5.
        self.maxIteration = 50
        self.absTol = 0.01
        self.targetMargin = 0
        self.bounds = (0.,9999.) # optimization bounds on MUs

        # CEM options
        self.cemToIsocenter = 400 # Base to isocenter
        self.cemSpikeOrientation = CEM.SpikeOrientations.FACING_PATIENT
        self.cemDensity = 1.2
        self.cemMinThickness = 5
        self.maxCEMThickness = None

        # Aperture options
        self.apertureToIsocenter = 100
        self.apertureDensity = 8.5
        self.apertureThickness = 60
        self.apertureClearance = 0
        self.apertureLatThickness = 30

        # Range shifter options
        self.rangeShifterToIsocenter = 170
        self.rangeShifterDensity = 2.7
        self.rangeShifterMargin = 20
        self.minSlabThickness = None

        self.doseUpdateEvent = Event(object)
        self.planUpdateEvent = Event(RTPlan)
        self.targetUpdateEvent = Event(ROIMask)
        self.fValEvent = Event(Tuple)

        self._cemOptimizer = CEMBEVOptimizer()
        self._cemOptimizer.doseBEVUpdateEvent.connect(self._handleDoseBEVUpdate)
        self._cemOptimizer.planUpdateEvent.connect(self.planUpdateEvent.emit)
        self._cemOptimizer.fValEvent.connect(self.fValEvent.emit)
        self._cemOptimizer._beamInitializer.gridType = BeamInitializerBEV.GridTypes.DEFAULT
        
        self.nbPrimariesPerBeamlet = 1e5
        self.nbPrimariesFinalDose = 1e8

        self._plan = None
        self._targetROIBEV = None
        self._globalROIBEV = None
        self._targetROI = None
        self._globalROI = None
        self._finalDose = None
        self._ctBEV = None
        self._doseCalculator:CEMBEVDoseCalculator = None
        self.ctCEM = None

        # Scoring grid
        self.scoringVoxelSpacing = None
        self.scoringGridSize = None
        self.scoringOrigin = None
        self.adapt_gridSize_to_new_spacing = True

    def _handleDoseBEVUpdate(self, doseBEV):
        doseImage = imageTransform3D.iecGantryToDicom(doseBEV, self._plan[0]) #, cropROI=self._globalROI)
        resampler3D.resampleImage3DOnImage3D(doseImage, self.ct, inPlace=True)
        self.doseUpdateEvent.emit(doseImage)

    @property
    def finalDose(self):
        return self._finalDose

    @property
    def finalPlan(self):
        return self._plan
    
    @property
    def gridType(self):
        return self._cemOptimizer._beamInitializer.gridType

    @gridType.setter
    def gridType(self, type: BeamInitializerBEV.GridTypes):
        self._cemOptimizer._beamInitializer.gridType = type

    def abort(self):
        self._cemOptimizer.abort()
    
    def initialization(self, beam:Optional[CEMBeam]=None):
        patient = self.ct.patient

        self._computeTargetAndGlobalROIs() # in dicom

        self._initializePlan(beam) # create CEMBEam with emply layer
        self._initializeCTAndROIsBEV() # compute and store CT, globalROI, targetROI in BEV

        beam = self._plan.beams[0]
        beam.cemToIsocenter = self.cemToIsocenter

        self._plan.patient = patient
        self.planUpdateEvent.emit(self._plan)


        self._ctBEV.name = 'CT BEV'
        # self._ctBEV.patient = patient

        self._initializeCTObjects() # Create and store BiComponentCEMBEV in beam.cem and aperture in beam.aperture

        self._initializeCEMOptimizer() # Create and store CEMBEVDoseCalculator in object + create and store CEMObjectives in self.cemOptimizer

        self.targetUpdateEvent.emit(self._targetROI)        

    def run(self, beam:Optional[CEMBeam]=None) -> RTPlan:
        self.initialization(beam)

        self._cemOptimizer.run(self._plan[0], self._ctBEV, self._targetROIBEV) #

        # Final dose
        self._computeFinalDose()
        self._finalDose.patient = self.ct.patient
        self._finalDose.name = 'Final dose'
        self.doseUpdateEvent.emit(self._finalDose)

        return self._plan

    def _computeTargetAndGlobalROIs(self):
        """
        Compute and store Target ROI and Union of all other ROIs (target + OARs).
        Global ROI mask is then dilated by 3*spacing
        """
        self._targetROI, self._globalROI = objectivesPreprocessing.getTargetAndGlobalROIsFromObjectives(self.ct, [obj.objectiveTerm for obj in self.objectives])

        cropROIMargin = self.apertureLatThickness + self.apertureClearance
        self._globalROI.dilateMask(radius=cropROIMargin)


    def _initializePlan(self, beam:Optional[CEMBeam]=None):
        """
        Initialize plan with a CEMBeam object that contains an empty layer
        """
        self._plan = RTPlan()

        if beam is None:
            beam = CEMBeam()
            beam.isocenterPosition = self._targetROI.centerOfMass
            beam.gantryAngle = self.gantryAngle

            layer = PlanIonLayer(nominalEnergy=self.beamEnergy)
            beam.appendLayer(layer)

        self._plan.appendBeam(beam)

    def _initializeCTAndROIsBEV(self):
        """
        Compute and store the CT in BEV in self._ctBEV, globalROI in BEV (self._globalROIBEV) and targetROI in BEV (self._targetROIBEV)
        """
        # Computations are much faster if we crop the CT as much as we can => cropROI=self._globalROI
        self._globalROIBEV = imageTransform3D.dicomToIECGantry(self._globalROI, self._plan[0], fillValue=0, cropROI=self._globalROI,
                                                               cropDim0=True, cropDim1=True, cropDim2=False)

        self._targetROIBEV = imageTransform3D.dicomToIECGantry(self._targetROI, self._plan[0], fillValue=0, cropROI=self._globalROI,
                                                               cropDim0=True, cropDim1=True, cropDim2=False)

        self._ctBEV = imageTransform3D.dicomToIECGantry(self.ct, self._plan.beams[0], fillValue=-1024.,
                                                        cropROI=self._globalROI, cropDim0=True, cropDim1=True, cropDim2=False)

        imagePreprocessing.padImageForCEM(self._ctBEV, self._plan[0], self.beamModel, inPlace=True)
        resampler3D.resampleImage3DOnImage3D(self._globalROIBEV, self._ctBEV, inPlace=True)
        resampler3D.resampleImage3DOnImage3D(self._targetROIBEV, self._ctBEV, inPlace=True)

    def _initializeCTObjects(self):
        """
        Create CEM, apeture and RS, and add them to plan
        """

        beam = self._plan.beams[0]

        refCEM = CEM.fromBeam(self._ctBEV, beam)
        refCEM.spikeOrientation = self.cemSpikeOrientation
        refCEM.rsp = self.ctCalibration.convertMassDensity2RSP(self.cemDensity)
        refCEM.baseToIsocenter = self.cemToIsocenter

        if refCEM.baseToIsocenter > self.beamModel.nozzle_isocenter:
            logger.warning(f"CEM is inside the nozzle: Base of CEM is {refCEM.baseToIsocenter}mm from isoncenter while nozzle to isocenter is {self.beamModel.nozzle_isocenter}mm from iso.")

        if beam.cem is None:
            beam.cem = refCEM
        else:
            beam.cem = resampler3D.resampleImage3DOnImage3D(beam.cem, refCEM, inPlace=True)

        if beam.aperture is None:
            apertureRSP = self.ctCalibration.convertMassDensity2RSP(self.apertureDensity)
            aperture = Aperture.fromMaskBEV(self._targetROIBEV, beam, apertureRSP*self.apertureThickness, lateralThickness=self.apertureLatThickness, clearance=self.apertureClearance)
            aperture.rsp = apertureRSP
            aperture.spikeOrientation = aperture.SpikeOrientations.FACING_NOZZLE
            aperture.baseToIsocenter = self.apertureToIsocenter
            beam.aperture = aperture
        else:
            beam.aperture = resampler3D.resampleImage3DOnImage3D(beam.aperture, refCEM, inPlace=True)

        if beam.flashRangeShifter is None:
            rangeShifter = RangeShifter.fromBeam(self._ctBEV, beam)
            rangeShifter.rsp = self.ctCalibration.convertMassDensity2RSP(self.rangeShifterDensity)
            rangeShifterWET = energyToRangeMM(self.beamEnergy) - self._maxWETOfTarget() - self.cemMinThickness*refCEM.rsp - self.rangeShifterMargin
            if self.minSlabThickness is not None:
                rangeShifterthickness = rangeShifterWET / rangeShifter.rsp
                rangeShifterthickness = (rangeShifterthickness // self.minSlabThickness) * self.minSlabThickness
                print("rangeShifterthickness",rangeShifterthickness)
                rangeShifterWET = rangeShifterthickness * rangeShifter.rsp
            rangeShifter.imageArray = np.ones(rangeShifter.imageArray.shape) * rangeShifterWET
            rangeShifter.spikeOrientation = rangeShifter.SpikeOrientations.FACING_NOZZLE
            rangeShifter.baseToIsocenter = self.rangeShifterToIsocenter
            beam.flashRangeShifter = rangeShifter
        else:
            beam.flashRangeShifter = resampler3D.resampleImage3DOnImage3D(beam.flashRangeShifter, refCEM, inPlace=True)

    def _maxWETOfTarget(self) -> np.ndarray:
        rspPBEV = RSPImage.fromCT(self._ctBEV, self.ctCalibration)

        wepl = np.cumsum(rspPBEV.imageArray, axis=2)*rspPBEV.spacing[2]

        wepl[np.logical_not(self._targetROIBEV.imageArray)] = 0
        weplMax = np.max(wepl)

        return weplMax

    def _initializeCEMOptimizer(self):
        self._initializeDoseCalculator() # Create and store CEMBEVDoseCalculator in object

        self.setOptimierObjectives() # Create and store CEMObjectives in cemOptimizer

        self._cemOptimizer.maxIterations = self.maxIteration
        self._cemOptimizer.spotSpacing = self.spotSpacing
        self._cemOptimizer.absTol = self.absTol
        self._cemOptimizer.ctCalibration = self.ctCalibration
        self._cemOptimizer.minCEMThickness = self.cemMinThickness
        self._cemOptimizer.maxCEMThickness = self.maxCEMThickness
        self._cemOptimizer._targetMask = self._targetROI
        self._cemOptimizer._ct = self.ct
        self._cemOptimizer.targetMargin = self.targetMargin
        self._cemOptimizer.bounds = self.bounds


    def _initializeDoseCalculator(self):
        """
        Create and store CEMBEVDoseCalculator in object
        """
        self._doseCalculator = CEMBEVDoseCalculator()
        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.nbPrimaries = self.nbPrimariesPerBeamlet
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.beam = self._plan[0]
        self._doseCalculator.roiBEV = self._globalROIBEV
        self._doseCalculator.ctBEV = self._ctBEV
        self._doseCalculator.patient = self.ct.patient
        self._doseCalculator.setScoringParameters(scoringGridSize=self.scoringGridSize, scoringSpacing=self.scoringVoxelSpacing,
                                                  scoringOrigin=self.scoringOrigin, adapt_gridSize_to_new_spacing=self.adapt_gridSize_to_new_spacing)


    def setOptimierObjectives(self):
        self._cemOptimizer.clearObjectives()

        for objective in self.objectives:
            objective.objectiveTerm.doseCalculator = self._doseCalculator

            roi = objective.objectiveTerm.roi
            roiBEV = imageTransform3D.dicomToIECGantry(roi, self._plan[0], cropROI=self._globalROI, cropDim0=True, cropDim1=True, cropDim2=False)
            resampler3D.resampleImage3DOnImage3D(roiBEV, self._ctBEV, inPlace=True) # To be sure
            roiG0 = imageTransformBEV.bevToGantry0(roiBEV)

            objective.objectiveTerm.roi = roiG0 # With this dose calculator, everything is computed in BEV and output as G0 to save time

            self._cemOptimizer.appendObjective(objective.objectiveTerm, weight=objective.weight)

        self._cemOptimizer.updateObjectives()

    def _computeFinalDose(self):
        self._doseCalculator.nbPrimaries = self.nbPrimariesFinalDose
        #doseCalculator.scoringVoxelSpacing = 3

        beam = self._plan[0]

        self._finalDose = self._doseCalculator.computeDoseDicom(beam.spotMUs, beam.cem)
        self._finalDose.name = 'Final dose'
        self._finalDose.patient = self.ct.patient

        self.computeCTCEM()

    def computeCTCEM(self):
        beam = self._plan[0]
        self._doseCalculator.ctBEV.update([beam.cem, beam.flashRangeShifter, beam.aperture], self.ctCalibration)
        ctDicom = imageTransform3D.iecGantryToDicom(self._doseCalculator.ctBEV, beam, fillValue=-1024)
        ctDicom.name = 'CT with CEM'
        ctDicom.patient = self.ct.patient
        self.ctCEM = ctDicom

    def getRangeShifterThickness(self):
        beam = self._plan.beams[0]
        cemRSP = self.ctCalibration.convertMassDensity2RSP(self.cemDensity)

        if beam.flashRangeShifter is None:
            # rangeShifter = RangeShifter.fromBeam(self._ctBEV, beam)
            rangeShifter_rsp = self.ctCalibration.convertMassDensity2RSP(self.rangeShifterDensity)
            rangeShifterWET = energyToRangeMM(self.beamEnergy) - self._maxWETOfTarget() - self.cemMinThickness*cemRSP - self.rangeShifterMargin
            rangeShifterthickness = rangeShifterWET / rangeShifter_rsp
            if self.minSlabThickness is not None:
                rangeShifterthickness = (rangeShifterthickness // self.minSlabThickness) * self.minSlabThickness
        else:
            rangeShifterWET = np.max(beam.flashRangeShifter.imageArray)
            rangeShifterthickness = rangeShifterWET / rangeShifter_rsp
        return rangeShifterthickness