
from abc import abstractmethod

import numpy as np
from opentps.core.data.images import ROIMask
import scipy.sparse as sp
from opentps.core.processing.imageProcessing import resampler3D
from opentps.core.data._roiContour import ROIContour
from typing import Sequence, Union

class AbstractDoseFidelityTerm:
    def __init__(self, roi:ROIMask,):
        self.roi = roi
        self.maskVec = None

    @abstractmethod
    def getValue(self, weights:np.ndarray) -> float:
        raise NotImplementedError()

    @abstractmethod
    def getDerivative(self, weights:np.ndarray) -> np.ndarray:
        raise NotImplementedError()

    def _updateMaskVec(self, spacing:Sequence[float], gridSize:Sequence[int], origin:Sequence[float]):

        if isinstance(self.roi, ROIContour):
            mask = self.roi.getBinaryMask(origin=origin, gridSize=gridSize, spacing=spacing)
        elif isinstance(self.roi, ROIMask):
            mask = self.roi
            if not (np.array_equal(mask.gridSize, gridSize) and
                np.allclose(mask.origin, origin, atol=0.01) and
                np.allclose(mask.spacing, spacing, atol=0.01)):
                mask = resampler3D.resampleImage3D(self.roi, gridSize=gridSize, spacing=spacing, origin=origin)
        else:
            raise Exception(self.roi.__class__.__name__ + ' is not a supported class for roi')

        self.maskVec = np.flip(mask.imageArray, (0, 1))
        self.maskVec = np.ndarray.flatten(self.maskVec, 'F').astype('bool')


class DoseMaxObjective(AbstractDoseFidelityTerm):
    def __init__(self, roi:ROIMask, maxDose:float, doseCalculator=None):
        super().__init__(roi=roi)
        self.maxDose:float = maxDose
        self.doseCalculator = doseCalculator

    @property
    def _roiVoxels(self):
        return np.count_nonzero(self.roi.imageArray.astype(int))

    def getValue(self, weights:np.ndarray) -> float:
        beamlets = self.doseCalculator.computeBeamlets().toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.mean(np.maximum(0, doseTotal - self.maxDose) ** 2)
        return f

    def getDerivative(self, weights:np.ndarray) -> np.ndarray:
        beamlets = self.doseCalculator.computeBeamlets().toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.maximum(0, doseTotal - self.maxDose)
        df = 2 / len(f) * np.transpose(f) @ beamlets
        return df


class DoseMinObjective(AbstractDoseFidelityTerm):
    def __init__(self, roi:ROIMask, minDose:float, doseCalculator=None):
        super().__init__(roi=roi)
        self.minDose:float = minDose
        self.doseCalculator = doseCalculator

    @property
    def _roiVoxels(self):
        return np.count_nonzero(self.roi.imageArray)

    def getValue(self, weights:np.ndarray) -> float:
        beamlets = self.doseCalculator.computeBeamlets().toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.mean(np.minimum(0, doseTotal - self.minDose) ** 2)
        return f

    def getDerivative(self, weights:np.ndarray) -> np.ndarray:
        beamlets = self.doseCalculator.computeBeamlets().toSparseMatrix()[self.maskVec]
        doseTotal = sp.csc_matrix.dot(beamlets, weights)
        f = np.minimum(0, doseTotal - self.minDose)
        df = 2 / len(f) * np.transpose(f) @ beamlets
        return df
