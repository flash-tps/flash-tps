
import numpy as np
from opentps.core.data.images import CTImage, ROIMask
from opentps.core.processing.planOptimization.planInitializer import PlanInitializer
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.processing.planInitializer import BeamInitializerBEV


class CEMBEVBeamInitializer(BeamInitializerBEV):
    def initializeBeam(self, beam:CEMBeam, ctBEV:CTImage, targetROIBEV:ROIMask):
        if beam.isocenterPosition is None:
            beam.isocenterPosition = targetROIBEV.centerOfMass

        roiDilated = ROIMask.fromImage3D(targetROIBEV)
        roiDilated.dilateMask(radius=self.targetMargin)

        if self.gridType == self.GridTypes.HEXAGONAL:
            spotGridX, spotGridY = self._defineHexagSpotGridAroundIsocenter(self.spotSpacing, targetROIBEV, beam.isocenterPosition)
        elif self.gridType == self.GridTypes.SQUARE:
            spotGridX, spotGridY = self._defineSquaredSpotGridAroundIsocenter(self.spotSpacing, targetROIBEV, beam.isocenterPosition)
        else:
            raise ValueError(f'Unrecognized Gridtype {self.gridType}')

        coordGridX, coordGridY = self._pixelCoordinatedWrtIsocenter(targetROIBEV, beam.isocenterPosition)

        spotGridX = spotGridX.flatten()
        spotGridY = spotGridY.flatten()

        for layer in beam:
            layerMask = np.sum(roiDilated.imageArray, axis=2).astype(bool)

            coordX = coordGridX[layerMask]
            coordY = coordGridY[layerMask]

            coordX = coordX.flatten()
            coordY = coordY.flatten()

            x2 = np.matlib.repmat(np.reshape(spotGridX, (spotGridX.shape[0], 1)), 1, coordX.shape[0]) \
                 - np.matlib.repmat(np.transpose(coordX), spotGridX.shape[0], 1)
            y2 = np.matlib.repmat(np.reshape(spotGridY, (spotGridY.shape[0], 1)), 1, coordY.shape[0]) \
                 - np.matlib.repmat(np.transpose(coordY), spotGridY.shape[0], 1)

            ind = (x2*x2+y2*y2).argmin(axis=0)

            spotPosCandidates = np.unique(np.array(list(zip(spotGridX[ind], -spotGridY[ind]))), axis=0)

            for i in range(spotPosCandidates.shape[0]):
                spotPos = spotPosCandidates[i, :]
                layer.addToSpot(spotPos[0], spotPos[1], 0.) # We do not append spot but rather add it because append throws an exception if already exists

        beam.spotMUs = np.ones(beam.spotMUs.shape)


class CEMBEVBeamInitializer_biRegions(CEMBEVBeamInitializer):
    def __init__(self):
        super().__init__()

        self.outerSpotSpacing = 3
        self.innerSpotSpacing = 5

    def initializeBeam(self, beam:CEMBeam, ctBEV: CTImage, targetROIBEV: ROIMask):
        roiDilated = ROIMask.fromImage3D(targetROIBEV)
        roiDilated.dilateMask(radius=self.targetMargin)

        roiEroded = ROIMask.fromImage3D(targetROIBEV)
        roiEroded.erode(radius = np.max([self.targetMargin, 8]))


        if beam.isocenterPosition is None:
            beam.isocenterPosition = roiDilated.centerOfMass

        innerSpotGridX, innerSpotGridY = self._defineHexagSpotGridAroundIsocenter(self.innerSpotSpacing, roiDilated, beam.isocenterPosition)
        outerSpotGridX, outerSpotGridY = self._defineHexagSpotGridAroundIsocenter(self.outerSpotSpacing, roiDilated, beam.isocenterPosition)
        coordGridX, coordGridY = self._pixelCoordinatedWrtIsocenter(roiDilated, beam.isocenterPosition)

        innerSpotGridX = innerSpotGridX.flatten()
        innerSpotGridY = innerSpotGridY.flatten()
        outerSpotGridX = outerSpotGridX.flatten()
        outerSpotGridY = outerSpotGridY.flatten()

        for layer in beam:
            dilatedMask = np.sum(roiDilated.imageArray, axis=2).astype(bool)
            erodedMask = np.sum(roiEroded.imageArray, axis=2).astype(bool)
            innerMask = np.logical_and(dilatedMask, erodedMask)
            outerMask = np.logical_xor(dilatedMask, erodedMask)

            for i in range(2):
                if i==0:
                    mask = innerMask
                    spotGridX = innerSpotGridX
                    spotGridY = innerSpotGridY
                elif i==1:
                    mask = outerMask
                    spotGridX = outerSpotGridX
                    spotGridY = outerSpotGridY

                coordX = coordGridX[mask]
                coordY = coordGridY[mask]

                coordX = coordX.flatten()
                coordY = coordY.flatten()

                x2 = np.matlib.repmat(np.reshape(spotGridX, (spotGridX.shape[0], 1)), 1, coordX.shape[0]) \
                     - np.matlib.repmat(np.transpose(coordX), spotGridX.shape[0], 1)
                y2 = np.matlib.repmat(np.reshape(spotGridY, (spotGridY.shape[0], 1)), 1, coordY.shape[0]) \
                     - np.matlib.repmat(np.transpose(coordY), spotGridY.shape[0], 1)

                ind = (x2 * x2 + y2 * y2).argmin(axis=0)

                spotPosCandidates = np.unique(np.array(list(zip(spotGridX[ind], -spotGridY[ind]))), axis=0)

                for i in range(spotPosCandidates.shape[0]):
                    spotPos = spotPosCandidates[i, :]
                    layer.addToSpot(spotPos[0], spotPos[1], 0.)

        beam.spotMUs = np.ones(beam.spotMUs.shape)

class CEMPlanInitializer(PlanInitializer):
    def __init__(self):
        super().__init__()

        self._beamInitializer = CEMBEVBeamInitializer()