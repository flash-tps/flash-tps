import copy
import logging

import numpy as np
import scipy.sparse as sp

from typing import Union, Sequence, Optional

from opentps.core.data import SparseBeamlets
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import DoseImage, CTImage, RSPImage
from opentps.core.data.plan import PlanIonBeam, RTPlan, PlanIonLayer
from opentps.core.processing.rangeEnergy import energyToRangeMM, rangeMMToEnergy
from opentps_flash.core.data.cem import CEM
from opentps_flash.core.data.ctWithCEM import CTBEVWithCEM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.DoseCalculation.cachedMCsquareDoseCalculator import \
    CachedMCsquareDoseCalculator
from opentps_flash.core.processing.Utils.doseComputation import computeDoseFromBeamlets


logger = logging.getLogger(__name__)


class CEMBEVPreconditioningDoseCalculator:
    def __init__(self):
        self.beamModel = None
        self.ctCalibration:AbstractCTCalibration = None
        self.beam:PlanIonBeam = None
        self.targetMaskBEV = None
        self.nbPrimaries = 1e5
        self.patient = None # DEBUG
        self.wetStep = 5

        self._plan = None
        self._initialCTBEV = None
        self._ctBEV: CTBEVWithCEM = None
        self._doseCalculator = CachedMCsquareDoseCalculator()
        self._ctCEFForBeamlets = None
        self._doseG0:DoseImage = None
        self._beamlets:SparseBeamlets = None

    @property
    def scoringVoxelSpacing(self) -> Sequence[float]:
        return self._doseCalculator.scoringVoxelSpacing
    
    @scoringVoxelSpacing.setter
    def scoringVoxelSpacing(self, spacing: Union[float, Sequence[float]]):
        self._doseCalculator.scoringVoxelSpacing = spacing
    
    @property
    def scoringGridSize(self) -> Sequence[float]:
        return self._doseCalculator.scoringGridSize
    
    @scoringGridSize.setter
    def scoringGridSize(self, gridSize:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringGridSize = gridSize

    @property
    def scoringOrigin(self) -> Sequence[float]:
        return self._doseCalculator.scoringOrigin
    
    @scoringOrigin.setter
    def scoringOrigin(self, origin:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringOrigin = origin

    def setScoringParameters(self, scoringGridSize:Optional[Sequence[int]]=None, scoringSpacing:Optional[Sequence[float]]=None,
                                scoringOrigin:Optional[Sequence[int]]=None, adapt_gridSize_to_new_spacing=False):
        """
        Sets the scoring parameters

        Parameters
        ----------
        scoringGridSize: Sequence[int]
            scoring grid size
        scoringSpacing: Sequence[float]
            scoring spacing
        scoringOrigin: Sequence[float]
            scoring origin
        adapt_gridSize_to_new_spacing: bool
            If True, automatically adapt the gridSize to the new spacing
        """
        self._doseCalculator.setScoringParameters(scoringGridSize, scoringSpacing,
                                scoringOrigin, adapt_gridSize_to_new_spacing)

    @property
    def ctBEV(self) -> CTBEVWithCEM:
        return self._ctBEV

    @ctBEV.setter
    def ctBEV(self, ct:CTImage):
        self._initialCTBEV = ct
        self._ctBEV = CTBEVWithCEM(ct)

    def computeDose(self, weights:np.ndarray, cem) -> DoseImage:
        if self._beamlets is None:
            self.computeBeamlets(cem)
        self._updateDose(weights)

        return self._doseG0

    def _updateDose(self, weights:np.ndarray):

        if not (self._doseG0 is None): # Free some memory
            self._doseG0.imageArray = None

        self._doseG0 = computeDoseFromBeamlets(self._beamlets, weights)

    def computeBeamlets(self, cem) -> SparseBeamlets:
        if not(self._beamlets is None):
            return self._beamlets

        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.nbPrimaries = self.nbPrimaries

        if self._plan is None:
            self._plan = self.createPreconditionningPlan()

        for beam in self._plan:
            self._updateCTForBeamletsWithCEM()

            planGantry0 = RTPlan()
            beam = copy.deepcopy(beam)
            beam.gantryAngle = 0
            beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, beam.isocenterPosition)
            planGantry0.appendBeam(beam)

            # roiGantry0 = imageTransformBEV.bevToGantry0(self.roiBEV)
            # TODO use the roi of the objectives and one to create for fluence estimation
            beamlets = self._doseCalculator.computeBeamlets(self._ctCEFForBeamlets, planGantry0)

            if self._beamlets is None:
                self._beamlets = beamlets
            else:
                beamletsCSC = beamlets.toSparseMatrix()
                self._beamlets.setUnitaryBeamlets(sp.hstack([self._beamlets.toSparseMatrix(), beamletsCSC]))

        return self._beamlets

    def getIMPTPlan(self):
        if self._plan is None:
            return self.createPreconditionningPlan()

        return self._plan

    def getFLASHWeights(self, weights=None):
        """
        Sum of MUs in each layer. The spotMUs of each beam are concatenated
        """
        if weights is not None:
            self._plan.spotMUs = weights # self._plan = IMPT plan
        for i, beam in enumerate(self._plan.beams):
            if i==0:
                spotMUs = np.sum(np.array([layer.spotMUs for layer in beam.layers]), axis=0)
            else:
                beam_spotMUs = np.sum(np.array([layer.spotMUs for layer in beam.layers]), axis=0)
                spotMUs = np.hstack((spotMUs, beam_spotMUs))
        return spotMUs

    def _updateCTForBeamletsWithCEM(self):
        logger.info("Updating CT with CEM for dose calculation.")

        self._ctBEV.update([self.beam.flashRangeShifter, self.beam.aperture], self.ctCalibration)
        self._ctCEFForBeamlets = imageTransformBEV.bevToGantry0(self._ctBEV)

    def createPreconditionningPlan(self) -> RTPlan:
        cem = copy.deepcopy(self.beam.cem)
        cem.imageArray = np.zeros(cem.gridSize)

        plan = RTPlan()
        beam = copy.deepcopy(self.beam)
        beam.cem = cem
        plan.appendBeam(beam)

        layerXY = np.array(beam.spotXY)

        layers = beam.layers
        for layer in layers:
            beam.removeLayer(layer)

        for wet in np.arange(self._minWETOfTarget(), self._maxWETOfTarget()+self.wetStep, self.wetStep):
            layer = PlanIonLayer()
            layer.nominalEnergy = rangeMMToEnergy(wet)

            for i in range(layerXY.shape[0]):
                layer.appendSpot(layerXY[i, 0], layerXY[i, 1], 1)

            beam.appendLayer(layer)

        return plan

    def _maxWETOfTarget(self) -> np.ndarray:
        return self._statWETOfTarget(np.nanmax)

    def _minWETOfTarget(self) -> np.ndarray:
        return self._statWETOfTarget(np.nanmin)

    def _statWETOfTarget(self, stat=np.nanmean) -> np.ndarray:
        ctBEV = CTBEVWithCEM(self._ctBEV)
        ctBEV.update([self.beam.flashRangeShifter], self.ctCalibration)

        rspPBEV = RSPImage.fromCT(ctBEV, self.ctCalibration)

        wepl = np.cumsum(rspPBEV.imageArray, axis=2) * rspPBEV.spacing[2]

        wepl[np.logical_not(self.targetMaskBEV.imageArray)] = np.nan
        weplStat = stat(wepl.flatten())

        return weplStat