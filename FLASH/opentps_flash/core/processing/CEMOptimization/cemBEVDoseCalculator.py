import copy
import logging
from enum import Enum
from typing import Union, Sequence, Optional

import numpy as np
from opentps.core.data import SparseBeamlets
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import DoseImage, CTImage, Image3D, Image2D, ROIMask
from opentps.core.data.plan import PlanIonBeam, RTPlan, PlanIonLayer
from opentps.core.processing.imageProcessing import resampler3D, imageTransform3D
from opentps.core.processing.rangeEnergy import energyToRangeMM, rangeMMToEnergy
from opentps_flash.core.data.cem import CEM
from opentps_flash.core.data.ctWithCEM import CTBEVWithCEM
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.processing.DoseCalculation.analyticalDerivative import AnalyticalDerivative
from opentps_flash.core.processing.DoseCalculation.analyticalNoScatteringBEV import \
    AnalyticalNoScatteringBEV
from opentps_flash.core.processing.DoseCalculation.cachedMCsquareDoseCalculator import \
    CachedMCsquareDoseCalculator
from opentps_flash.core.processing.DoseCalculation.fluenceBasedMCsquareDoseCalculator import \
    FluenceBasedMCsquareDoseCalculator, Beamlets
from opentps_flash.core.processing.DoseCalculation.fluenceCalculator import FluenceCalculatorBEV
from opentps_flash.core.processing.Utils.doseComputation import computeDoseFromBeamlets

logger = logging.getLogger(__name__)


class CEMBEVDoseCalculator:
    class DerivativeModes(Enum):
        ANALYTICAL = 'ANALYTICAL'
        DEFAULT = 'ANALYTICAL'
        MC = 'MC'

    def __init__(self):
        self.beamModel = None
        self.ctCalibration:AbstractCTCalibration = None
        self.beam:PlanIonBeam = None
        self.roiBEV = None
        self.nbPrimaries = 1e5
        self.derivativePrimaries = 1e4
        self.derivativeMode = self.DerivativeModes.DEFAULT
        self.patient = None # DEBUG

        self._initialCTBEV = None
        self._ctBEV: CTBEVWithCEM = None
        self._doseCalculator = CachedMCsquareDoseCalculator()
        self._fluenceBasedDoseCalculator = FluenceBasedMCsquareDoseCalculator()
        self._analyticalCalculator = AnalyticalNoScatteringBEV()
        self._ctCEFForBeamlets = None
        self._weightsForBeamlets = np.array([])
        self._cemThicknessForBeamlets = np.array([])
        self._cemThicknessForDerivative = np.array([])
        self._weightsForDerivative = np.array([])
        self._sparseDerivativeCEM = None
        self._analyticalDerivative = None
        self._doseG0:DoseImage = None
        self._beamlets:SparseBeamlets = None
        self._firstTimeBeamletDerivative = True
    
    @property
    def scoringVoxelSpacing(self) -> Sequence[float]:
        return self._doseCalculator.scoringVoxelSpacing
    
    @scoringVoxelSpacing.setter
    def scoringVoxelSpacing(self, spacing: Union[float, Sequence[float]]):
        self._doseCalculator.scoringVoxelSpacing = spacing
    
    @property
    def scoringGridSize(self) -> Sequence[float]:
        return self._doseCalculator.scoringGridSize
    
    @scoringGridSize.setter
    def scoringGridSize(self, gridSize:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringGridSize = gridSize

    @property
    def scoringOrigin(self) -> Sequence[float]:
        return self._doseCalculator.scoringOrigin
    
    @scoringOrigin.setter
    def scoringOrigin(self, origin:Sequence[float]) -> Sequence[float]:
        self._doseCalculator.scoringOrigin = origin

    def setScoringParameters(self, scoringGridSize:Optional[Sequence[int]]=None, scoringSpacing:Optional[Sequence[float]]=None,
                                scoringOrigin:Optional[Sequence[int]]=None, adapt_gridSize_to_new_spacing=False):
        """
        Sets the scoring parameters

        Parameters
        ----------
        scoringGridSize: Sequence[int]
            scoring grid size
        scoringSpacing: Sequence[float]
            scoring spacing
        scoringOrigin: Sequence[float]
            scoring origin
        adapt_gridSize_to_new_spacing: bool
            If True, automatically adapt the gridSize to the new spacing
        """
        self._doseCalculator.setScoringParameters(scoringGridSize, scoringSpacing,
                                scoringOrigin, adapt_gridSize_to_new_spacing)

    @property
    def ctBEV(self) -> CTBEVWithCEM:
        return self._ctBEV

    @ctBEV.setter
    def ctBEV(self, ct:CTImage):
        self._initialCTBEV = ct
        self._ctBEV = CTBEVWithCEM(ct)

    def kill(self):
        self._doseCalculator.kill()

    def computeDose(self, weights:np.ndarray, cem:CEM) -> DoseImage:
        return self.computeDoseGantry0(weights, cem)

    def computeDoseDicom(self, weights:np.ndarray, cem:CEM) -> DoseImage:
        self._updateCTForBeamletsWithCEM(cem)

        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.nbPrimaries = self.nbPrimaries

        planGantry0 = RTPlan()
        beam = copy.deepcopy(self.beam)
        beam.gantryAngle = 0
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, beam.isocenterPosition)
        beam.spotMUs = weights
        planGantry0.appendBeam(beam)

        doseG0 = self._doseCalculator.computeDose(self._ctCEFForBeamlets, planGantry0)
        # resampler3D.resampleImage3DOnImage3D(doseG0, self._ctCEFForBeamlets, inPlace=True) # resample if needed
        doseBEV = imageTransformBEV.gantry0ToBEV(doseG0)

        return imageTransform3D.iecGantryToDicom(doseBEV, self.beam)


    def _gantry0ToDIcom(self, imageG0: Image3D) -> Image3D:
        doseBEV = imageTransformBEV.gantry0ToBEV(imageG0)
        doseImage = imageTransform3D.iecGantryToDicom(doseBEV, self.beam)

        return doseImage


    def computeDoseGantry0(self, weights:np.ndarray, cem:CEM) -> DoseImage:
        #TODO : Dose not always updated after CEM has changed
        # if self._doseMustBeRecomputed(weights, cem):
        #     self.computeBeamletsGantry0(cem)
        #     self._updateDose(weights)

        # We could just do this as updating the dose does not take too much time
        self.computeBeamletsGantry0(cem)
        self._updateDose(weights)

        return self._doseG0

    def _doseMustBeRecomputed(self, weights:np.ndarray, cem:CEM):
        if len(self._weightsForBeamlets)==0:
            logger.info("Dose must be recomputed because: no current weights")
            return True

        if not(np.allclose(weights, self._weightsForBeamlets, atol=0.1)):
            logger.info("Dose must be recomputed because: weights must be updated")

        return not(np.allclose(weights, self._weightsForBeamlets, atol=0.1)) or self._beamletsMustBeRecomputed(cem)

    def _beamletsMustBeRecomputed(self, cem:CEM) -> bool:
        if len(self._cemThicknessForBeamlets)==0:
            logger.info("Dose must be recomputed because: no current CEM")
            return True

        if not np.allclose(self._flattenCEM(cem), self._cemThicknessForBeamlets, atol=0.1):
            logger.info("Dose must be recomputed because: CEM must be updated")

        return not np.allclose(self._flattenCEM(cem), self._cemThicknessForBeamlets, atol=0.1)

    def _flattenCEM(self, cem:CEM):
        return cem.imageArray.flatten()

    def _updateDose(self, weights:np.ndarray):
        self._weightsForBeamlets = np.array(weights)

        if not (self._doseG0 is None): # Free some memory
            self._doseG0.imageArray = None

        self._doseG0 = computeDoseFromBeamlets(self._beamlets, self._weightsForBeamlets)

    def computeBeamlets(self, cem:CEM) -> SparseBeamlets:
        return self.computeBeamletsGantry0(cem)

    def computeBeamletsGantry0(self, cem:CEM) -> SparseBeamlets:
        if self._beamletsMustBeRecomputed(cem):
            self._updateCTForBeamletsWithCEM(cem)
            self._updateBeamlets()
            self._cemThicknessForBeamlets = np.array(self._flattenCEM(cem))
            self._weightsForBeamlets = np.array([])  # We need this so that the dose computation considers new weights

        return self._beamlets

    def _updateCTForBeamletsWithCEM(self, cem:CEM):
        logger.info("Updating CT with CEM for dose calculation.")

        self._ctBEV.update([cem, self.beam.flashRangeShifter, self.beam.aperture], self.ctCalibration)
        self._ctCEFForBeamlets = imageTransformBEV.bevToGantry0(self._ctBEV)

    def _updateBeamlets(self):
        logger.info("Recomputing beamlets.")
        self._doseCalculator.beamModel = self.beamModel
        self._doseCalculator.ctCalibration = self.ctCalibration
        self._doseCalculator.nbPrimaries = self.nbPrimaries

        planGantry0 = RTPlan()
        beam = copy.deepcopy(self.beam)
        beam.gantryAngle = 0
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, beam.isocenterPosition)
        planGantry0.appendBeam(beam)

        if not (self._beamlets is None): # Free some memory
            self._beamlets.setUnitaryBeamlets(None)
            self._beamlets.setUnitaryBeamlets(None)

        # roiGantry0 = imageTransformBEV.bevToGantry0(self.roiBEV)
        # TODO use the roi of the objectives and one to create for fluence estimation
        self._beamlets = self._doseCalculator.computeBeamlets(self._ctCEFForBeamlets, planGantry0)

    def computeDerivative(self, weights: np.ndarray, cem: CEM) -> Union[Beamlets, DoseImage]:
        return self.computeDerivativeGantry0(weights, cem)

    def computeDerivativeGantry0(self, weights:np.ndarray, cem:CEM) -> Union[Beamlets, DoseImage]:
        if self.derivativeMode==self.DerivativeModes.ANALYTICAL:
            return self.computeAnalyticalDerivativeGantry0(weights, cem)
        elif self.derivativeMode==self.DerivativeModes.MC:
            return self.computeBeamletDerivativeGantry0(weights, cem)
        else:
            raise ValueError('derivativeMode is incorrect')

    def computeAnalyticalDerivativeGantry0(self, weights:np.ndarray, cem:CEM) -> DoseImage:
        if self._derivativeMustBeRecomputed(weights, cem):
            self._cemThicknessForDerivative = self._flattenCEM(cem)
            self._weightsForDerivative = np.array(weights)
            self._updateAnalyticalDerivative(weights, cem)

        return self._analyticalDerivative

    def _derivativeMustBeRecomputed(self, weights:np.ndarray, cem:CEM) -> bool:
        if len(self._cemThicknessForDerivative)==0:
            return True

        if len(self._weightsForDerivative)==0:
            return True

        return not(np.allclose(self._flattenCEM(cem), self._cemThicknessForDerivative, atol=0.1) and np.allclose(weights, self._weightsForDerivative, atol=0.1))

    def _updateAnalyticalDerivative_test(self, weights, cem:CEM):
        deltaR = 1.

        analyticalCalculator = AnalyticalDerivative(self.ctCalibration)

        doseBEV = imageTransformBEV.gantry0ToBEV(self.computeDoseGantry0(weights, cem))

        derivativeBEV = analyticalCalculator.computeDerivative(self._ctBEV, doseBEV, deltaR, self.roiBEV)

        self._analyticalDerivative = imageTransformBEV.bevToGantry0(derivativeBEV)

    def _updateAnalyticalDerivative(self, weights, cem:CEM):
        deltaR = 1.

        cemBEVFluenceCalculator = CEMBEVFluenceCalculator()
        cemBEVFluenceCalculator.doseG0 = self.computeDoseGantry0(weights, cem)
        cemBEVFluenceCalculator.cemToIsocenter = self.beam.cemToIsocenter

        self._analyticalCalculator.beamModel = self.beamModel
        self._analyticalCalculator.referenceEnergy = self.beam[0].nominalEnergy
        self._analyticalCalculator.ctCalibration = self.ctCalibration
        self._analyticalCalculator.fluenceCalculator = cemBEVFluenceCalculator

        plan = RTPlan()
        beam = copy.deepcopy(self.beam)
        beam.gantryAngle = 0
        beam.spotMUs = self._weightsForDerivative
        beam.cem.imageArray = cem.imageArray
        plan.appendBeam(beam)

        ctBEV = CTBEVWithCEM(self._initialCTBEV)
        ctBEV.update([beam.flashRangeShifter], self.ctCalibration)

        doseSequence = self._analyticalCalculator.computeDosePerBeam(ctBEV, plan, self.roiBEV)

        plan2 = self._lowerPlanEnergy(plan, deltaR=deltaR)
        doseSequence2 = self._analyticalCalculator.computeDosePerBeam(ctBEV, plan2, self.roiBEV)

        dose = Image3D.fromImage3D(doseSequence[0])
        dose.imageArray = (dose.imageArray - doseSequence2[0].imageArray)/deltaR
        outDose = imageTransformBEV.bevToGantry0(dose)

        self._analyticalDerivative = outDose

    def computeBeamletDerivativeGantry0(self, weights:np.ndarray, cem:CEM) -> Beamlets:
        flattenedCEM = self._flattenCEM(cem)

        if self._firstTimeBeamletDerivative or self._derivativeMustBeRecomputed(weights, cem):
            self._weightsForDerivative = weights
            self._updateCTForBeamletsWithCEM(cem)
            self._updateBeamletDerivative(weights, cem)
            self._cemThicknessForDerivative = flattenedCEM

            self._firstTimeBeamletDerivative = False

        return self._sparseDerivativeCEM

    def _updateBeamletDerivative(self, weights, cem):
        deltaR = 1.

        cemBEVFluenceCalculator = CEMBEVFluenceCalculator()
        cemBEVFluenceCalculator.doseG0 = self.computeDoseGantry0(weights, cem)
        cemBEVFluenceCalculator.cemToIsocenter = self.beam.cemToIsocenter


        self._fluenceBasedDoseCalculator.beamModel = self.beamModel
        self._fluenceBasedDoseCalculator.ctCalibration = self.ctCalibration
        self._fluenceBasedDoseCalculator.nbPrimaries = self.derivativePrimaries
        self._fluenceBasedDoseCalculator.fluenceCalculator = cemBEVFluenceCalculator

        # We lower the energy in computeBeamlets so that output sparseBeamle ts has exactly the same shape as that of beamlets1
        planGantry0 = RTPlan()
        beam = copy.deepcopy(self.beam)
        beam.gantryAngle = 0
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, self.beam.isocenterPosition)
        beam.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(self._ctBEV, self.beam.isocenterPosition)
        planGantry0.appendBeam(beam)

        roiGantry0 = imageTransformBEV.bevToGantry0(self.roiBEV)

        beamlets = self._fluenceBasedDoseCalculator.computeBeamlets(self._ctCEFForBeamlets, planGantry0,
                                                                    roi=roiGantry0)
        beamletsE2 = self._fluenceBasedDoseCalculator.computeBeamlets(self._ctCEFForBeamlets, planGantry0,
                                                                      roi=roiGantry0, deltaR=-deltaR)

        sparseBeamlets = beamlets.sparseBeamlets
        sparseBeamlets.setUnitaryBeamlets(sparseBeamlets.toSparseMatrix() - beamletsE2.sparseBeamlets.toSparseMatrix())
        beamlets.sparseBeamlets = sparseBeamlets

        self._sparseDerivativeCEM = beamlets

    def _lowerPlanEnergy(self, plan:RTPlan, deltaR:float=1.) -> RTPlan:
        plan2 = copy.deepcopy(plan)

        for beam in plan2:
            for layer in beam:
                layer.nominalEnergy = rangeMMToEnergy(energyToRangeMM(layer.nominalEnergy) - deltaR)

        return plan2

class CEMBEVFluenceCalculator(FluenceCalculatorBEV):
    def __init__(self):
        super().__init__()

        self.doseG0:DoseImage = None
        self.cemToIsocenter = 0

    def layerFluenceAtNozzle(self, layer:PlanIonLayer, ctBEV:Image3D, isocenterBEV:Sequence[float]) -> Image2D:
        doseBEV = imageTransformBEV.gantry0ToBEV(self.doseG0)
        # resample on ctBEV, otherwise, issues in _computeDoseForBeam of analytical calculator
        if not doseBEV.hasSameGrid(ctBEV):
            doseBEV = resampler3D.resampleImage3DOnImage3D(doseBEV, ctBEV, inPlace=False, fillValue=0.)

        pos = np.array(isocenterBEV)
        pos[2] -= self.cemToIsocenter
        ind = doseBEV.getVoxelIndexFromPosition(pos)
        ind = ind[2]

        # TODO: also use CEM orientation

        fluence = doseBEV.imageArray[:, :, ind]

        return Image2D(imageArray=fluence, spacing=doseBEV.spacing[:-1], origin=doseBEV.origin[:-1])
