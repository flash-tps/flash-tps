from __future__ import annotations

from typing import Tuple, Union


from typing import TYPE_CHECKING

import numpy as np
from opentps.core.data import ROIContour
from opentps.core.data.images import CTImage, ROIMask
from opentps.core.data.plan import PlanIonBeam
from opentps.core.processing.imageProcessing import resampler3D, imageTransform3D

from opentps_flash.core.processing.CEMOptimization import cemObjectives, planObjectives

if TYPE_CHECKING:
    from opentps_flash.core.processing.CEMOptimization import cemBEVWorkflow


def getTargetAndGlobalROIsFromObjectives(ct:CTImage, objectives:list[Union[cemObjectives.CEMAbstractDoseFidelityTerm, planObjectives.AbstractDoseFidelityTerm]]) -> Tuple[ROIMask, ROIMask]:
    """
    Return Target ROI and Union of all other ROIs (target + OARs)
    """
    targetROIVal = None
    globalROIVal = None

    for obj in objectives:
        roi = obj.roi

        if isinstance(roi, ROIContour):
            roi = roi.getBinaryMask(ct.origin, ct.gridSize, ct.spacing)
        else:
            roi = resampler3D.resampleImage3DOnImage3D(roi, ct, inPlace=False, fillValue=0)

        if (globalROIVal is None):
            globalROIVal = roi.imageArray.astype(bool)
        else:
            globalROIVal = np.logical_or(globalROIVal.astype(bool), roi.imageArray.astype(bool))

        if isinstance(obj, cemObjectives.DoseMinObjective) or isinstance(obj, planObjectives.DoseMinObjective):
            if targetROIVal is None:
                targetROIVal = roi.imageArray.astype(bool)
            else:
                targetROIVal = np.logical_or(targetROIVal.astype(bool), roi.imageArray.astype(bool))

    targetROI = ROIMask.fromImage3D(ct, patient=None)
    targetROI.imageArray = targetROIVal

    globalROI = ROIMask.fromImage3D(ct, patient=None)
    globalROI.imageArray = globalROIVal

    return targetROI, globalROI
