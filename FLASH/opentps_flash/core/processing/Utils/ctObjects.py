from opentps.core.processing.rangeEnergy import energyToRangeMM
import numpy as np
from opentps.core.data.images import RSPImage

def getRangeShifterThickness(ctCalibration, cemDensity, cemMinThickness, rangeShifterDensity, beamEnergy, rangeShifterMargin, maxWETOfTarget=None, _ctBEV=None, _targetROIBEV=None, minSlabThickness=None):
    cemRSP = ctCalibration.convertMassDensity2RSP(cemDensity)

    rangeShifterRSP = ctCalibration.convertMassDensity2RSP(rangeShifterDensity)
    if maxWETOfTarget is None:
        assert _ctBEV is not None and _targetROIBEV is not None
        maxWETOfTarget = _maxWETOfTarget(_ctBEV, _targetROIBEV, ctCalibration)
    rangeShifterWET = energyToRangeMM(beamEnergy) - maxWETOfTarget - cemMinThickness*cemRSP - rangeShifterMargin
    rangeShifterthickness = rangeShifterWET / rangeShifterRSP
    if minSlabThickness is not None:
        rangeShifterthickness = (rangeShifterthickness // minSlabThickness) * minSlabThickness

    return rangeShifterthickness

def _maxWETOfTarget(_ctBEV, _targetROIBEV, ctCalibration) -> np.ndarray:
    rspPBEV = RSPImage.fromCT(_ctBEV, ctCalibration)

    wepl = np.cumsum(rspPBEV.imageArray, axis=2)*rspPBEV.spacing[2]

    wepl[np.logical_not(_targetROIBEV.imageArray)] = 0
    weplMax = np.max(wepl)
    print("weplMax",weplMax)

    return weplMax