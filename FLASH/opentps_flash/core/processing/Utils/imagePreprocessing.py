import numpy as np

from opentps.core.data.MCsquare import BDL
from opentps.core.data.images import Image3D
from opentps.core.processing.imageProcessing import imageTransform3D
from opentps_flash.core.data.cemBeam import CEMBeam
from opentps_flash.core.data.cem import CEM


def padImageForCEM(dataBEV:Image3D, beam:CEMBeam, bdl:BDL, inPlace:bool=False):
    if not inPlace:
        dataBEV = dataBEV.__class__.fromImage3D(dataBEV, patient=None)

    isocenterInImage = imageTransform3D.dicomCoordinate2iecGantry(beam, beam.isocenterPosition)
    isocenterCoord = dataBEV.getVoxelIndexFromPosition(isocenterInImage)
    padLength = np.max((0, int(bdl.nozzle_isocenter/dataBEV.spacing[2] - isocenterCoord[2])))
    
    newOrigin = np.array(dataBEV.origin)
    newOrigin[2] = newOrigin[2] - padLength * dataBEV.spacing[2]
    newArray = -1024 * np.ones((dataBEV.gridSize[0], dataBEV.gridSize[1], dataBEV.gridSize[2] + padLength))
    newArray[:, :, padLength:] = dataBEV.imageArray
    dataBEV.imageArray = newArray
    dataBEV.origin = newOrigin

    return dataBEV

def padImageForDevice(dataBEV:Image3D, beam:CEMBeam, device:CEM, inPlace:bool=False, DICOM=False):
    if not inPlace:
        dataBEV = dataBEV.__class__.fromImage3D(dataBEV, patient=None)
                                                    
    if DICOM:
        dataBEV = imageTransform3D.dicomToIECGantry(dataBEV, beam, fillValue=-1024.,
                                                        cropDim0=True, cropDim1=True, cropDim2=False)

    isocenterInImage = imageTransform3D.dicomCoordinate2iecGantry(beam, beam.isocenterPosition)
    isocenterCoord = dataBEV.getVoxelIndexFromPosition(isocenterInImage)
    padLength = np.max((0, int(device.baseToIsocenter + np.max(device.imageArray)/device.rsp - isocenterCoord[2]) + 10))

    newOrigin = np.array(dataBEV.origin)
    newOrigin[2] = newOrigin[2] - padLength * dataBEV.spacing[2]
    newArray = -1024 * np.ones((dataBEV.gridSize[0], dataBEV.gridSize[1], dataBEV.gridSize[2] + padLength))
    newArray[:, :, padLength:] = dataBEV.imageArray
    dataBEV.imageArray = newArray
    dataBEV.origin = newOrigin

    if DICOM:
        dataBEV = imageTransform3D.iecGantryToDicom(dataBEV, beam, fillValue=-1024., cropDim0=True, cropDim1=True, cropDim2=False)

    return dataBEV

def padImageForDeviceDicom(data:Image3D, beam:CEMBeam, device:CEM, inPlace:bool=False):
    if not inPlace:
        data = data.__class__.fromImage3D(data, patient=None)

    isocenterInImage = beam.isocenterPosition
    isocenterCoord = data.getVoxelIndexFromPosition(isocenterInImage)
    padLength = np.max((0, int(device.baseToIsocenter + np.max(device.imageArray)/device.rsp - isocenterCoord[1]) + 10))

    newOrigin = np.array(data.origin)
    newOrigin[1] = newOrigin[1] - padLength * data.spacing[1]
    newArray = -1024 * np.ones((data.gridSize[0], data.gridSize[1] + padLength, data.gridSize[2]))
    newArray[:, padLength:, :] = data.imageArray
    data.imageArray = newArray
    data.origin = newOrigin

    return data