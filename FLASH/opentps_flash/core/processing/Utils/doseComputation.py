import numpy as np
try:
    import sparse_dot_mkl
    use_MKL = 0 # Currently deactivated on purpose because sparse_dot_mkl generates seg fault
except:
    use_MKL = 0

cupy_available = False
try:
    import cupy as cp
    import cupyx as cpx
    cupy_available = True
except:
    cupy_available = False

from scipy.sparse import csc_matrix
from opentps.core.data.images._doseImage import DoseImage
from opentps.core.data._sparseBeamlets import SparseBeamlets

def computeDoseFromBeamlets(beamlets:SparseBeamlets, weights:np.ndarray):

    weights = np.array(weights, dtype=np.float32)
    if use_MKL == 1:
        totalDose = sparse_dot_mkl.dot_product_mkl(beamlets._sparseBeamlets, weights)
    else:
        totalDose = csc_matrix.dot(beamlets._sparseBeamlets, weights)

    totalDose = np.reshape(totalDose, beamlets._gridSize, order='F')
    totalDose = np.flip(totalDose, 0)
    totalDose = np.flip(totalDose, 1)

    doseImage = DoseImage(imageArray=totalDose, origin=beamlets._origin, spacing=beamlets._spacing,
                            angles=beamlets._orientation)

    return doseImage
