import copy
from typing import Sequence

import numpy as np
from opentps.core.data.CTCalibrations import AbstractCTCalibration
from opentps.core.data.images import CTImage, Image3D, ROIMask
from opentps.core.processing.imageProcessing import resampler3D
from opentps_flash.core.data.abstractCTObject import AbstractCTObject

class CTBEVWithCEM(CTImage):
    def __new__(cls, ct:CTImage=None):
        if ct is None:
            ct = CTImage()
        else:
            ct = ct.__class__.fromImage3D(ct, patient=None)

        ct.__class__ = cls
        return ct

    def __init__(self, ctBEV:CTImage):
        # No call tu superclass on purpose as object is initialized in _new_ via fromImage3D
        self._initialCT = ctBEV.__class__.fromImage3D(ctBEV, patient=None)

    @property
    def ctWithoutCEM(self) -> CTImage:
        return self._initialCT

    @classmethod
    def fromImage3D(cls, image:Image3D, **kwargs):
        dic = {'imageArray': copy.deepcopy(image.imageArray), 'origin': image.origin, 'spacing': image.spacing,
               'angles': image.angles, 'seriesInstanceUID': image.seriesInstanceUID, 'patient': image.patient}
        dic.update(kwargs)
        return CTImage(**dic)

    @CTImage.imageArray.setter
    def imageArray(self, array:np.ndarray):
        raise Exception('Array cannot be modified externally')

    def update(self, ctObjects:Sequence[AbstractCTObject], ctCalibration:AbstractCTCalibration):
        roiBEV = []
        roiHU = []

        for ctObject in ctObjects:
            if ctObject is None:
                continue
            roiBEV.append(ctObject.computeROIBEV(self._initialCT))
            roiHU.append(ctCalibration.convertRSP2HU(ctObject.rsp, energy=100.)+1) # +1 because of bug in MCsquare, see https://gitlab.com/openmcsquare/MCsquare/-/issues/33  

        self.updateFromROIs(roiBEV, roiHU)

    def updateFromROIs(self, roisBEV:Sequence[ROIMask], roisHU:Sequence[float]):
        ctArray = np.array(self._initialCT.imageArray)

        for i, roi in enumerate(roisBEV):
            roi = roisBEV[i]
            roi = resampler3D.resampleImage3DOnImage3D(roi, self, fillValue=0, inPlace=False)

            ctArray[roi.imageArray.astype(bool)] = roisHU[i]

        # TODO: use super class setter
        self._imageArray = ctArray
        self.dataChangedSignal.emit()
