import copy

import numpy as np
import scipy.sparse as sp

from opentps_flash.core.data.image4D import Image4D


class InstantaneousDose(Image4D):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._irradiationTime = None
        self._startTime = None

    @property
    def irradiationTime(self):
        return self._irradiationTime

    @irradiationTime.setter
    def irradiationTime(self, it):
        self._irradiationTime = it

    @property
    def startTime(self) -> np.ndarray:
        return self._startTime

    @startTime.setter
    def startTime(self, st):
        self._startTime = st

class InstantaneousDoseRate(InstantaneousDose):
    @property
    def dose(self):
        dose = InstantaneousDose.fromImage4D(self)
        dose.irradiationTime = self.irradiationTime
        dose.startTime = self.startTime

        dose.imageArray = (sp.diags(self.irradiationTime.astype(np.float32), format='csc') * self.imageArray.transpose()).transpose()
        return dose
