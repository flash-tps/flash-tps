import copy
from typing import Optional

from opentps.core.data.plan import PlanIonBeam


class CEMBeam(PlanIonBeam):
    def __init__(self):
        super().__init__()


        self.cem = None
        self.flashRangeShifter = None
        self.aperture = None
        self.apertureToIsocenter: float = None
