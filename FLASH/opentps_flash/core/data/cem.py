import copy
import logging
from enum import Enum


from opentps.core.data.images import Image2D, ROIMask, Image3D
from opentps.core.data.plan import PlanIonBeam
from opentps.core.processing.imageProcessing import imageTransform3D
from scipy.ndimage import morphology

import numpy as np

from opentps_flash.core.data.abstractCTObject import AbstractCTObject


logger = logging.getLogger(__name__)


class CEM(AbstractCTObject, Image2D):
    class SpikeOrientations(Enum):
        FACING_PATIENT = 'FACING_PATIENT'
        FACING_NOZZLE = 'FACING_NOZZLE'
        DEFAULT = 'FACING_NOZZLE'

    def __init__(self):
        super(AbstractCTObject, self).__init__()
        super(Image2D, self).__init__()

        self.rsp:float = 1.
        self.spikeOrientation = self.SpikeOrientations.DEFAULT
        self.baseToIsocenter:float = 0

        self._referenceBeam = None

    @property
    def imageArray(self) -> np.ndarray:
        return self._imageArray

    @imageArray.setter
    def imageArray(self, array: np.ndarray):
        self._imageArray = array

    def forceImageArray(self, array: np.ndarray):
        self._imageArray = array

    @classmethod
    def fromCEM(cls, cem):
        newCEM = cls()

        newCEM._referenceBeam = copy.copy(cem._referenceBeam)

        newCEM.origin = np.array(cem.origin)
        newCEM.spacing = np.array(cem.spacing)
        newCEM._imageArray = np.array(cem.imageArray)
        newCEM.rsp = cem.rsp

        return newCEM

    @classmethod
    def fromBeam(cls, ctBEV:Image3D, beam:PlanIonBeam):
        newCEM = cls()
        newCEM._referenceBeam = beam
        newCEM.origin = ctBEV.origin[0:-1]
        newCEM.spacing = ctBEV.spacing[0:-1]
        newCEM.imageArray = np.zeros(ctBEV.gridSize[0:-1])

        return newCEM

    @property
    def referenceBeam(self):
        return self._referenceBeam

    @referenceBeam.setter
    def referenceBeam(self, beam):
        self._referenceBeam = beam

    def computeROIDicomAtGantry0(self, referenceImage) -> ROIMask:
        """
        Only work when gantry angle is zero
        """
        referenceImage = Image3D.fromImage3D(referenceImage)

        referenceImage.origin = (self.origin[0], referenceImage.origin[1], self.origin[1])
        data = np.zeros((self.imageArray.shape[0], referenceImage.imageArray.shape[1], self.imageArray.shape[1]))
        referenceImage.imageArray = data

        proxInd, distInd = self._indicesDicomAtGantry0(referenceImage)

        for i in range(data.shape[0]):
            for j in range(data.shape[2]):
                data[i, proxInd[i, j]:distInd[i, j], j] = 1

        roi = ROIMask.fromImage3D(referenceImage, patient=None)
        roi.imageArray = data.astype(bool)

        return roi

    def computeROIBEV(self, referenceImageBEV) -> ROIMask:
        referenceImageBEV = Image3D.fromImage3D(referenceImageBEV)

        referenceImageBEV.origin = (self.origin[0], self.origin[1], referenceImageBEV.origin[2])
        data = np.zeros((self.imageArray.shape[0], self.imageArray.shape[1], referenceImageBEV.imageArray.shape[2]))
        referenceImageBEV.imageArray = data

        proxInd, distInd = self._indicesBEV(referenceImageBEV)

        for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                data[i, j, proxInd[i, j]:distInd[i, j]] = 1

        roiBEV = ROIMask.fromImage3D(referenceImageBEV, patient=None)
        roiBEV.imageArray = data.astype(bool)

        return roiBEV
    
    def _indicesDicomAtGantry0(self, referenceImage):
        referenceImage.origin = (self.origin[0], referenceImage.origin[1], self.origin[1])
        data = np.zeros((self.imageArray.shape[0], referenceImage.imageArray.shape[1], self.imageArray.shape[1]))
        referenceImage.imageArray = data

        cemPixelsInDim1 = self._cemPixelsInDim1(referenceImage)
        baseInd = self._baseIndInDim1(referenceImage)

        if self.spikeOrientation == self.SpikeOrientations.FACING_NOZZLE:
            proxInd = (baseInd - cemPixelsInDim1).astype(int)
            distInd =  (baseInd * np.ones(cemPixelsInDim1.shape)).astype(int)
        elif self.spikeOrientation == self.SpikeOrientations.FACING_PATIENT:
            proxInd = (baseInd * np.ones(cemPixelsInDim1.shape)).astype(int)
            distInd = (baseInd + cemPixelsInDim1).astype(int)
        else:
            raise ValueError

        if np.any(proxInd<0):
            logger.warning('referenceImage is not large enough to contain CEM on nozzle side. ' + str(-np.min(proxInd)) + ' pixels missing. Cropping')
            proxInd[proxInd<0] = 0

        return proxInd, distInd

    def _indicesBEV(self, referenceImageBEV):
        referenceImageBEV.origin = (self.origin[0], self.origin[1], referenceImageBEV.origin[2])
        data = np.zeros((self.imageArray.shape[0], self.imageArray.shape[1], referenceImageBEV.imageArray.shape[2]))
        referenceImageBEV.imageArray = data

        cemPixelsInDim2 = self._cemPixelsInDim2(referenceImageBEV)
        baseInd = self._baseInd(referenceImageBEV)

        if self.spikeOrientation == self.SpikeOrientations.FACING_NOZZLE:
            proxInd = (baseInd - cemPixelsInDim2).astype(int)
            distInd =  (baseInd * np.ones(cemPixelsInDim2.shape)).astype(int)
        elif self.spikeOrientation == self.SpikeOrientations.FACING_PATIENT:
            proxInd = (baseInd * np.ones(cemPixelsInDim2.shape)).astype(int)
            distInd = (baseInd + cemPixelsInDim2).astype(int)
        else:
            raise ValueError

        if np.any(proxInd<0):
            logger.warning('referenceImageBEV is not large enough to contain CEM on nozzle side. ' + str(-np.min(proxInd)) + ' pixels missing. Cropping')
            proxInd[proxInd<0] = 0

        return proxInd, distInd

    def _cemPixelsInDim2(self, referenceImageBEV):
        cemArray = self.imageArray

        cemPixelsInDim2 = np.round(cemArray / (self.rsp * referenceImageBEV.spacing[2]))

        return cemPixelsInDim2.astype(int)
    
    def _cemPixelsInDim1(self, referenceImage):
        cemArray = self.imageArray

        cemPixelsInDim2 = np.round(cemArray / (self.rsp * referenceImage.spacing[1]))

        return cemPixelsInDim2.astype(int)

    def _baseInd(self, referenceImageBEV):
        isocenterInImage = imageTransform3D.dicomCoordinate2iecGantry(self._referenceBeam, self._referenceBeam.isocenterPosition)
        isocenterCoord = referenceImageBEV.getVoxelIndexFromPosition(isocenterInImage)

        distInPixels = np.ceil(self.baseToIsocenter/referenceImageBEV.spacing[2])

        return int(isocenterCoord[2] - distInPixels)

    def _baseIndInDim1(self, referenceImage):
        isocenterInImage = self._referenceBeam.isocenterPosition
        isocenterCoord = referenceImage.getVoxelIndexFromPosition(isocenterInImage)

        distInPixels = np.ceil(self.baseToIsocenter/referenceImage.spacing[1])

        return int(isocenterCoord[1] - distInPixels)

class RangeShifter(CEM):
    def dilate(self, radius:float=1.):
        radiusPixel = np.round(radius/self.spacing).astype(int)

        origin = self.origin
        origin = origin - radiusPixel*self.spacing

        rangeShifterWaterEquivThick = np.mean(self.imageArray[self.imageArray>0])

        imageArray = self.imageArray
        imageArray = np.pad(imageArray, ((radiusPixel[0], radiusPixel[0]), (radiusPixel[1], radiusPixel[1])), 'constant', constant_values=0)
        imageArray = imageArray.astype(bool)

        diameter = radiusPixel * 2 + 1  # if margin=0, filt must be identity matrix. If margin=1, we want to dilate by 1 => Filt must have three 1's per row.
        diameter = diameter + (diameter + 1) % 2
        diameter = np.round(diameter).astype(int)

        filt = np.zeros((diameter[0] + 2, diameter[1] + 2))
        filt[1:diameter[0] + 2, 1:diameter[1] + 2] = 1
        filt = filt.astype(bool)

        imageArray = morphology.binary_dilation(imageArray, structure=filt)

        self._imageArray = rangeShifterWaterEquivThick*imageArray.astype(float)
        self.origin = origin


class Aperture(CEM):
    @classmethod
    def fromMaskBEV(cls, targetMaskBEV: ROIMask, referenceBeam: PlanIonBeam, wet: float, lateralThickness: float,
                    clearance: float = 0.):
        newCEM = cls()

        targetMaskDilatedBEV = ROIMask.fromImage3D(targetMaskBEV, patient=None)
        targetMaskDilatedBEV.dilateMask(
            lateralThickness + clearance + targetMaskBEV.spacing.max())  # targetMaskBEV.spacing.max() is just a margin

        targetMaskWithClearanceBEV = ROIMask.fromImage3D(targetMaskBEV, patient=None)
        if clearance > 0:
            targetMaskWithClearanceBEV.dilateMask(clearance)

        apertureROI0 = targetMaskWithClearanceBEV.imageArray.sum(axis=2).astype(bool)
        apertureROI1 = targetMaskDilatedBEV.imageArray.sum(axis=2).astype(bool)

        apertureROI = np.logical_xor(apertureROI0, apertureROI1).astype(float)
        apertureROI[apertureROI > 0] = wet

        newCEM._referenceBeam = referenceBeam
        newCEM.origin = targetMaskBEV.origin[0:-1]
        newCEM.spacing = targetMaskBEV.spacing[0:-1]
        newCEM.imageArray = apertureROI

        return newCEM

    @classmethod
    def fromMaskDicom(cls, targetMask: ROIMask, referenceBeam: PlanIonBeam, wet: float, lateralThickness: float,
                    clearance: float = 0.):
        newCEM = cls()

        targetMaskDilated = ROIMask.fromImage3D(targetMask, patient=None)
        targetMaskDilated.dilateMask(
            lateralThickness + clearance + targetMask.spacing.max())  # targetMaskBEV.spacing.max() is just a margin

        targetMaskWithClearance = ROIMask.fromImage3D(targetMask, patient=None)
        if clearance > 0:
            targetMaskWithClearance.dilateMask(clearance)

        apertureROI0 = targetMaskWithClearance.imageArray.sum(axis=1).astype(bool)
        apertureROI1 = targetMaskDilated.imageArray.sum(axis=1).astype(bool)

        apertureROI = np.logical_xor(apertureROI0, apertureROI1).astype(float)
        apertureROI[apertureROI > 0] = wet

        newCEM._referenceBeam = referenceBeam
        newCEM.origin = targetMask.origin[[0,2]]
        newCEM.spacing = targetMask.spacing[[0,2]]
        newCEM.imageArray = apertureROI

        return newCEM
