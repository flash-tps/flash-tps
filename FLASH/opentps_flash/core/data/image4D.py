import copy
import logging
from typing import Sequence

import numpy as np

from opentps.core import Event
from opentps.core.data import PatientData


logger = logging.getLogger(__name__)


class Image4D(PatientData):
    def __init__(self, imageArray=None, name="4D Image", origin=(0, 0, 0), spacing=(1, 1, 1), angles=(0, 0, 0), doseGridSize=(0, 0, 0), seriesInstanceUID=None, patient=None):
        self.dataChangedSignal = Event()

        self.timing = None

        self._imageArray = imageArray
        self._origin = np.array(origin)
        self._spacing = np.array(spacing)
        self._angles = np.array(angles)
        self.doseGridSize = doseGridSize
        # if UID is None:
        #     UID = generate_uid()
        # self.UID = UID

        super().__init__(name=name, seriesInstanceUID=seriesInstanceUID,
                         patient=patient)  # We want to trigger super signal only when the object is fully initialized

    def __str__(self):
        gs = self.doseGridSize
        s = 'Image4D ' + str(gs[0]) + ' x ' +  str(gs[1]) +  ' x ' +  str(gs[2]) + ' x ' +  str(self.gridSize[1]) + '\n'
        return s

    # This is different from deepcopy because image can be a subclass of image3D but the method always returns an Image3D
    @classmethod
    def fromImage3D(cls, image, **kwargs):
        dic = {'imageArray': copy.deepcopy(image.imageArray), 'origin': image.origin, 'spacing': image.spacing,
               'angles': image.angles, 'seriesInstanceUID': image.seriesInstanceUID, 'patient': image.patient, 'name': image.name}
        dic.update(kwargs)
        return cls(**dic)

    @classmethod
    def fromImage4D(cls, image, **kwargs):
        dic = {'imageArray': copy.deepcopy(image.imageArray), 'origin': image.origin, 'spacing': image.spacing, 'doseGridSize':image.doseGridSize,
               'angles': image.angles, 'seriesInstanceUID': image.seriesInstanceUID, 'patient': image.patient,
               'name': image.name}
        dic.update(kwargs)
        return cls(**dic)

    @property
    def imageArray(self) -> np.ndarray:
        #return np.array(self._imageArray)
        return self._imageArray

    @imageArray.setter
    def imageArray(self, array):
        self._imageArray = array
        self.dataChangedSignal.emit()

    def update(self):
        self.dataChangedSignal.emit()

    @property
    def origin(self) -> np.ndarray:
        return self._origin

    @origin.setter
    def origin(self, origin):
        self._origin = np.array(origin)
        self.dataChangedSignal.emit()

    @property
    def spacing(self) -> np.ndarray:
        return self._spacing

    @spacing.setter
    def spacing(self, spacing):
        self._spacing = np.array(spacing)
        self.dataChangedSignal.emit()

    @property
    def angles(self) -> np.ndarray:
        return self._angles

    @angles.setter
    def angles(self, angles):
        self._angles = np.array(angles)
        self.dataChangedSignal.emit()

    @property
    def gridSize(self) -> np.ndarray:
        return np.array(self._imageArray.shape)

    @property
    def gridSizeInWorldUnit(self)  -> np.ndarray:
        raise Exception("Image4D has no gridSizeInWorldUnit property")


    def hasSameGrid(self, otherImage) -> bool:
        """Check whether the voxel grid is the same as the voxel grid of another image given as input.

            Parameters
            ----------
            otherImage : numpy array
                image to which the voxel grid is compared.

            Returns
            -------
            bool
                True if grids are identical, False otherwise.
            """

        if (np.array_equal(self.gridSize, otherImage.gridSize) and
                np.allclose(self._origin, otherImage._origin, atol=0.01) and
                np.allclose(self._spacing, otherImage.spacing, atol=0.01)):
            return True
        else:
            return False

    @property
    def numberOfVoxels(self):
        return self.gridSize[0]

    def getVectorAtPosition(self, position:Sequence[float]):
        voxelIndex = self.getVoxelIndexFromPosition(position)
        dataNumpy = self.imageArray[np.ravel_multi_index(voxelIndex, dims=self.doseGridSize), :]

        vec = dataNumpy.todense()
        return np.array(np.reshape(vec, (np.size(vec), )).tolist())

    def getVoxelIndexFromPosition(self, position:Sequence[float]) -> Sequence[float]:
        positionInMM = np.array(position)
        shiftedPosInMM = positionInMM - self.origin
        posInVoxels = np.round(np.divide(shiftedPosInMM, self.spacing)).astype(int)
        if np.any(np.logical_or(posInVoxels < 0, posInVoxels > (np.array(self.doseGridSize) - 1))):
            raise ValueError('Voxel position requested is outside of the domain of the image')

        return posInVoxels

    def getPositionFromVoxelIndex(self, index:Sequence[int]) -> Sequence[float]:
        if np.any(np.logical_or(index < 0, index > (np.array(self.doseGridSize) - 1))):
            raise ValueError('Voxel position requested is outside of the domain of the image')
        return self.origin + np.array(index).astype(dtype=float)*self.spacing

    def getMeshGridPositions(self) -> np.ndarray:
        x = self.origin[0] + np.arange(np.array(self.doseGridSize)[0]) * self.spacing[0]
        y = self.origin[1] + np.arange(np.array(self.doseGridSize)[1]) * self.spacing[1]
        z = self.origin[2] + np.arange(np.array(self.doseGridSize)[2]) * self.spacing[2]
        return np.meshgrid(x,y,z, indexing='ij')
    
    def min(self):
        return self._imageArray.min()

    def max(self):
        return self._imageArray.max()