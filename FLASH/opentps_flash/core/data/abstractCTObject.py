from abc import abstractmethod

from opentps.core.data.images import ROIMask


class AbstractCTObject:
    @abstractmethod
    def computeROI(self, *args) -> ROIMask:
        raise NotImplementedError
