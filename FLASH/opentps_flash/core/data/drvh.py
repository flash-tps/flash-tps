import numpy as np
from opentps.core.data._dvh import DVH
# from opentps.core import Event
from typing import Union, Optional

from opentps.core.data.images import Image3D
from opentps.core.data.images._doseImage import DoseImage
from opentps.core.data.images._roiMask import ROIMask
from opentps.core.data._roiContour import ROIContour
from opentps.core.processing.imageProcessing import resampler3D

class DRVH(DVH):
    def __init__(self, roiMask:Union[ROIContour, ROIMask], doseRate:Image3D, dose:DoseImage=None, prescription=None, thresholdDose=1.):
        self.thresholdDose = thresholdDose
        self._doseRate = doseRate
        super().__init__(roiMask=roiMask, dose=dose, prescription=prescription)

    def computeDVH(self, maxDVH:float=200.0):
        if (self._doseImage is None):
            return

        self._convertContourToROI()
        roiMask = self._roiMask
        doseImage = self._doseImage
        if not (self._doseRate.hasSameGrid(self._doseImage)):
            doseImage = resampler3D.resampleImage3DOnImage3D(self._doseImage, self._doseRate, inPlace=False, fillValue=0.)
            doseImage.patient = None
        if not(self._doseRate.hasSameGrid(self._roiMask)):
            roiMask = resampler3D.resampleImage3DOnImage3D(self._roiMask, self._doseRate, inPlace=False, fillValue=0.)
            roiMask.patient = None
        dose = np.array(doseImage.imageArray)
        doseRate = np.array(self._doseRate.imageArray)
        mask = roiMask.imageArray.astype(bool)
        mask[np.logical_or(dose<=self.thresholdDose, doseRate<=0)] = False # difference from DVH
        spacing = doseImage.spacing
        number_of_bins = 4096
        DVH_interval = [0, maxDVH]
        bin_size = (DVH_interval[1] - DVH_interval[0]) / number_of_bins
        bin_edges = np.arange(DVH_interval[0], DVH_interval[1] + 0.5 * bin_size, bin_size)
        bin_edges[-1] = maxDVH + doseRate.max()
        self._dose = bin_edges[:number_of_bins] + 0.5 * bin_size

        d = doseRate[mask]
        h, _ = np.histogram(d, bin_edges)
        h = np.flip(h, 0)
        h = np.cumsum(h)
        h = np.flip(h, 0)
        self._volume = h * 100 / len(d)  # volume in %
        self._volume_absolute = h * spacing[0] * spacing[1] * spacing[2] / 1000  # volume in cm3

        # compute metrics
        self._Dmean = np.mean(d)
        self._Dstd = np.std(d)
        self._Dmin = d.min() if len(d) > 0 else 0
        self._Dmax = d.max() if len(d) > 0 else 0
        self._D98 = self.computeDx(98)
        self._D95 = self.computeDx(95)
        self._D50 = self.computeDx(50)
        self._D5 = self.computeDx(5)
        self._D2 = self.computeDx(2)

        self.dataUpdatedEvent.emit()