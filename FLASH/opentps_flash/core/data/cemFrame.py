import numpy as np
from opentps_flash.core.data.cem import CEM


class CEMFrame:
    def __init__(self):
        self.minimumClearance:int = int(10)
        self.horizontalSize:int = int(100)
        self.thickness:int = int(5)
        self.lateralWidth:int = int(10)

    def addToCEMBEV(self, cem:CEM) -> CEM:
        outCEM = CEM.fromCEM(cem)

        isoInd = cem.getVoxelIndexFromPosition(cem.referenceBeam.isocenterPosition[0:-1]).astype(int)
        padLengths = (np.array(cem.gridSize) - 2*np.array(isoInd)).astype(int)
        clearanePixels = np.ceil(self.minimumClearance/cem.spacing).astype(int)
        lateralWidthPixels = np.ceil(self.lateralWidth/cem.spacing).astype(int)

        cemFrameArrayLen = (cem.gridSize + padLengths + 2*clearanePixels + 2*lateralWidthPixels).astype(int)
        cemFrameArray = self.thickness*np.ones(cemFrameArrayLen.astype(int))

        cemFrameArray[self.lateralWidth:np.floor((cemFrameArrayLen[0]-self.lateralWidth)/2.).astype(int),
            self.lateralWidth:np.floor((cemFrameArrayLen[1]-self.lateralWidth)/2.).astype(int)] = 0
        cemFrameArray[self.lateralWidth:np.floor((cemFrameArrayLen[0] - self.lateralWidth) / 2.).astype(int),
            np.ceil((cemFrameArrayLen[1] + self.lateralWidth) / 2.).astype(int):cemFrameArrayLen[1]-self.lateralWidth] = 0
        cemFrameArray[np.ceil((cemFrameArrayLen[0] + self.lateralWidth) / 2.).astype(int):cemFrameArrayLen[0] - self.lateralWidth,
            np.ceil((cemFrameArrayLen[1] + self.lateralWidth) / 2.).astype(int):cemFrameArrayLen[1] - self.lateralWidth] = 0
        cemFrameArray[np.ceil((cemFrameArrayLen[0] + self.lateralWidth) / 2.).astype(int):cemFrameArrayLen[0] - self.lateralWidth,
            self.lateralWidth:np.floor((cemFrameArrayLen[1]-self.lateralWidth)/2.).astype(int)] = 0

        ind0 = clearanePixels[0] + lateralWidthPixels[0]
        if padLengths[0]<0:
            ind0 += padLengths[0]

        ind1 = clearanePixels[1] + lateralWidthPixels[1]
        if padLengths[1] < 0:
            ind1 += padLengths[1]

        cemFrameArray[ind0:ind0+cem.gridSize[0], ind1:ind1+cem.gridSize[1]] = cem.imageArray
        outCEM.origin = cem.origin - np.array([ind0*cem.spacing[0], ind1*cem.spacing[1]])
        outCEM.imageArray = cemFrameArray

        return outCEM
