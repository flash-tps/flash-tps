from opentps_flash.core.data.cem import CEM
from opentps_flash.core.io.numpyToSTL import numpyToSTL


def writeCEM(cem:CEM, filePath:str):

    numpyToSTL(cem.imageArray/cem.rsp, cem.spacing, filePath)
