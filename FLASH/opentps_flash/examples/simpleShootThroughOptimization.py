import numpy as np
from opentps import gui
from opentps.core.data import Patient
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareMaterial import MCsquareMaterial
from opentps.core.data.images import CTImage, ROIMask
from opentps.core.io import scannerReader, mcsquareIO
from opentps.core.io.serializedObjectIO import saveDataStructure
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps.core.utils.programSettings import ProgramSettings
from opentps_flash.core.processing.CEMOptimization import cemObjectives
from opentps_flash.core.processing.CEMOptimization.cemBEVWorkflow import Objective, \
    SingleBeamCEMBEVOptimizationWorkflow
from opentps_flash.core.processing.CEMOptimization.planOptimizer import PlanOptimizerObjectives
from opentps_flash.core.processing.CEMOptimization.shootThroughWorkflow import ShootThroughOptimizationWorkflow
from opentps_flash.core.processing.doseRate.doseRateComputationWorkflow import DoseRateComputationWorkflow
from opentps_flash.core.processing.flashConfig import FLASHConfig
from opentps_flash.core.utils.plots import *
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator
from opentps_flash.core.processing.CEMOptimization import planObjectives
from opentps_flash.core.processing import imageTransformBEV
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D


# Create a patient structure and add it to the GUI for visualization later
patient = Patient(name='Patient')
gui.patientList.append(patient)

# CT Calibration and BDL
optiConfig = FLASHConfig()

bdlFile = optiConfig.bdlFile
bdl = mcsquareIO.readBDL(bdlFile)

scannerFolder = optiConfig.scannerFolder
ctCalibration = scannerReader.readScanner(scannerFolder)

# Load MCsquare materials to be used in the FLASH devices
brass = MCsquareMaterial.load("brass")

# Add the HU - density - material information to the CT calibration
ctCalibration.addEntry(5000, brass.density, brass)

# Create synthetic CT and ROI
ctSize = 120
huAir = -1000.
huWater = 0 #ctCalibration.convertRSP2HU(1.)
data = huAir * np.ones((ctSize, ctSize, ctSize))
data[:, 30:, :] = huWater
ct = CTImage(name='CT', patient=patient, imageArray=data)

roi = ROIMask(name='TV')
roi.color = (255, 0, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[ctSize//2-15:ctSize//2+15, 50+30-15:50+30+15, ctSize//2-15:ctSize//2+15] = True
roi.imageArray = data
roi.patient = patient

oar = ROIMask(name='OAR')
oar.color = (0, 255, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[ctSize//2-15:ctSize//2+15, 95:115, ctSize//2-15:ctSize//2+15] = True
oar.imageArray = data
oar.patient = patient

# Objectives
prescription = 8
objectiveTerms = PlanOptimizerObjectives()
obj = planObjectives.DoseMinObjective(roi, prescription)
objectiveTerms.append(obj, weight=1.)
obj = planObjectives.DoseMaxObjective(roi, prescription)
objectiveTerms.append(obj, weight=1.)
obj = planObjectives.DoseMaxObjective(oar, prescription)
objectiveTerms.append(obj, weight=1.)


# Device distances
airgap = 50 # airgap between aperture and water surface
iso_water = 65 # isocenter to water surface
apertureLength = 60
apertureLateralThickness = 30
apertureClearance = 7
aperture2ISO = airgap + iso_water

# Other parameters
beamAngle = 0
beamEnergy = 226
spotSpacing = 7
apertureDensity = brass.density

# Optimization workflow
workflow = ShootThroughOptimizationWorkflow()
workflow.ctCalibration = ctCalibration
workflow.beamModel = bdl
workflow.gantryAngle = beamAngle
workflow.beamEnergy = beamEnergy
workflow.ct = ct
workflow.spotSpacing = spotSpacing
workflow.scoringVoxelSpacing = [2,2,2]
workflow.adapt_gridSize_to_new_spacing = True

# Aperture options
workflow.apertureToIsocenter = aperture2ISO
workflow.apertureDensity = apertureDensity
workflow.apertureThickness = apertureLength
workflow.apertureClearance = apertureClearance
workflow.apertureLatThickness = apertureLateralThickness

# Optimization parameters
workflow.objectives = objectiveTerms
workflow.nbPrimariesFinalDose = 5e7

plan = workflow.run()
dose = workflow._finalDose
ct = workflow.ct

# Compute DVH
target_DVH = DVH(roiMask=roi, dose=dose)
oar_DVH = DVH(roiMask=oar, dose=dose)
plot_DVH([target_DVH, oar_DVH], xlim=(0,10))

# Plot profiles
plot_depth_dose_profile(dose, roi)
plot_lateral_dose_profile_x(dose, roi)
plot_lateral_dose_profile_y(dose, roi)

# Compute dose rate
drWorkflow = DoseRateComputationWorkflow(plan, ct, roi=[roi, oar], bdl=bdl, ctCalibration=ctCalibration, deliveryModel="CONTINOUS_BEAM_FLASH", doseRateCalculator="PDR", current=250e-9)
drWorkflow.percentile = 0.95
dr = drWorkflow.run()
dr.patient = patient # add to GUI

drWorkflow.computeDRVH(dose)
drWorkflow.plotDRVH()

# # Save for reloading later: uncomment lines
# path = ProgramSettings().exampleFolder
# saveDataStructure(patient, os.path.join(path, 'exampleCEM'))

gui.run() # launch the GUI to visualize the result
