import sys
sys.path.append('..')
import copy
import os
from opentps_flash.core.utils.plots import *
from opentps.core.data.plan import RTPlan
from opentps.core.data.images import CTImage, DoseImage, ROIMask
from opentps.core.data import DVH
from opentps_flash.core.data.drvh import DRVH
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps.core.data.CTCalibrations.MCsquareCalibration import MCsquareMaterial
from opentps.core.io import scannerReader, mcsquareIO
from opentps.core.data import Patient
from opentps import gui
from opentps_flash.core.data.cem import Aperture
from opentps.core.processing.imageProcessing import imageTransform3D, resampler3D, sitkImageProcessing
from opentps_flash.core.processing.Utils.imagePreprocessing import padImageForDevice
from opentps_flash.core.processing import imageTransformBEV
from opentps_flash.core.utils.plans import generate_uniform_field
from opentps_flash.core.processing.doseRate.doseRateComputationWorkflow import DoseRateComputationWorkflow
from opentps_flash.core.processing.flashConfig import FLASHConfig


# Create a patient structure and add it to the GUI for visualization later
patient = Patient(name='Patient')
gui.patientList.append(patient)

# CT Calibration and BDL
optiConfig = FLASHConfig()

bdlFile = optiConfig.bdlFile
bdl = mcsquareIO.readBDL(bdlFile)

scannerFolder = optiConfig.scannerFolder
ctCalibration = scannerReader.readScanner(scannerFolder)

# Load MCsquare materials to be used in the FLASH devices
brass = MCsquareMaterial.load("brass")

# Add the HU - density - material information to the CT calibration
ctCalibration.addEntry(5000, brass.density, brass)
brassHU = ctCalibration.convertMaterial2HU('Brass') + 1e-4
print('brassHU', brassHU)

# Create synthetic CT and ROI
ctSize = 120
huAir = -1000.
huWater = 0 #ctCalibration.convertRSP2HU(1.)
data = huAir * np.ones((ctSize, ctSize, ctSize))
data[:, 30:, :] = huWater
ct = CTImage(name='CT', patient=patient, imageArray=data)

roi = ROIMask(name='TV')
roi.color = (255, 0, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[ctSize//2-15:ctSize//2+15, 50+30-15:50+30+15, ctSize//2-15:ctSize//2+15] = True
roi.imageArray = data
roi.patient = patient

oar = ROIMask(name='OAR')
oar.color = (0, 255, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[ctSize//2-15:ctSize//2+15, 95:115, ctSize//2-15:ctSize//2+15] = True
oar.imageArray = data
oar.patient = patient

# Device distances
airgap = 50 # airgap between aperture and water surface
iso_water = 65 # isocenter to water surface
apertureLength = 60
apertureLateralThickness = 30
apertureClearance = 7
aperture2ISO = airgap + iso_water

# Other parameters
prescription = 8
beamAngle = 0
beamEnergy = 226
spotSpacing = 7
fieldSize=[28,28]
apertureDensity = brass.density


plan = generate_uniform_field(gantryAngle=0, energy=beamEnergy, spacing=spotSpacing, fieldSize=fieldSize, MU=10.)
plan.beams[0].isocenterPosition = roi.centerOfMass
plot_map(plan)

# Dose calculator
mc2 = MCsquareDoseCalculator()
mc2.beamModel = bdl
mc2.ctCalibration = ctCalibration
mc2.nbPrimaries = 5e6

# Dicom to BEV
target_mask_BEV = imageTransform3D.dicomToIECGantry(roi, beam=plan.beams[0], cropDim0=True, cropDim1=True, cropDim2=False)
aperture = Aperture.fromMaskBEV(target_mask_BEV, plan.beams[0], apertureLength*ctCalibration.convertMassDensity2RSP(brass.density), lateralThickness=apertureLateralThickness, clearance=apertureClearance)
aperture.rsp = brass.rsp
aperture.baseToIsocenter = aperture2ISO
ctBEV = imageTransform3D.dicomToIECGantry(ct, beam=plan.beams[0], cropDim0=True, cropDim1=True, cropDim2=False)
ctBEV = padImageForDevice(ctBEV, plan.beams[0], aperture)
apertureBEV = aperture.computeROIBEV(ctBEV)
ctBEV.imageArray[apertureBEV.imageArray] = brassHU

# BEV to dicom (for display)
ctWithAperture = imageTransform3D.iecGantryToDicom(ctBEV, plan.beams[0], fillValue=-1000, cropDim0=True, cropDim1=False, cropDim2=True)
ctWithAperture.name = 'CT with Aperture'
ctWithAperture.patient = patient

# Gantry 0
ctGantry0 = imageTransformBEV.bevToGantry0(ctBEV)
planGantry0 = RTPlan()
beamG0 = copy.deepcopy(plan.beams[0])
beamG0.gantryAngle = 0.
beamG0.isocenterPosition = imageTransformBEV.bevCoordinateToGantry0(ctBEV, plan.beams[0].isocenterPosition)
planGantry0.appendBeam(beamG0)

target_maskGantry0 = imageTransformBEV.bevToGantry0(target_mask_BEV)
doseG0 = mc2.computeDose(ctGantry0, planGantry0)

# Rescale spot MUs so that median dose = prescription
DVH_G0 = DVH(roiMask=target_maskGantry0, dose=doseG0)
scaling_factor = prescription / DVH_G0.D50
planGantry0.spotMUs = planGantry0.spotMUs * scaling_factor
mc2.nbPrimaries = mc2.nbPrimaries * 5
doseG0 = mc2.computeDose(ctGantry0, planGantry0)

# BEV to DICOM
doseImageBEV = imageTransformBEV.gantry0ToBEV(doseG0)
dose = imageTransform3D.iecGantryToDicom(doseImageBEV, plan.beams[0], fillValue=0., cropDim0=True, cropDim1=False, cropDim2=True)
dose.name = 'Final dose'
dose.patient = patient

# Compute DVH
target_DVH = DVH(roiMask=roi, dose=dose)
oar_DVH = DVH(roiMask=oar, dose=dose)
plot_DVH([target_DVH, oar_DVH], xlim=(0,10))

# Plot profiles
plot_depth_dose_profile(dose, roi)
plot_lateral_dose_profile_x(dose, roi)
plot_lateral_dose_profile_y(dose, roi)

# Compute dose rate
drWorkflow = DoseRateComputationWorkflow(plan, ct, roi=[roi, oar], bdl=bdl, ctCalibration=ctCalibration, deliveryModel="CONTINOUS_BEAM_FLASH", doseRateCalculator="PDR", current=250e-9)
drWorkflow.percentile = 0.95
dr = drWorkflow.run()
dr.patient = patient # add to GUI

drWorkflow.computeDRVH(dose)
drWorkflow.plotDRVH()

# # Save for reloading later: uncomment lines
# path = ProgramSettings().exampleFolder
# saveDataStructure(patient, os.path.join(path, 'exampleCEM'))

gui.run() # launch the GUI to visualize the result