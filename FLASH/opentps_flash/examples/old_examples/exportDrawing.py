import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from opentps import gui
from opentps.core.data.CTCalibrations.MCsquareCalibration import MCsquareElement, MCsquareMolecule
from opentps.core.data.images import CTImage
from opentps.core.data.plan import RTPlan
from opentps.core.io import scannerReader, mcsquareIO
from opentps.core.io.dataLoader import loadData
from opentps.core.processing.imageProcessing import imageTransform3D
from opentps.core.utils.programSettings import ProgramSettings
import opentps.core.processing.doseCalculation.MCsquare.Materials as MCsquareModule

from opentps_flash.core.data.ctWithCEM import CTBEVWithCEM
from opentps_flash.core.processing.Utils import imagePreprocessing
from opentps_flash.core.processing.flashConfig import FLASHConfig

# exampleCEM is created by waterBox.py .


path = ProgramSettings().exampleFolder

loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))
patient = gui.patientList[0]
plan = patient.getPatientDataOfType(RTPlan)[0]
ct = patient.getPatientDataOfType(CTImage)[0]


# CT Calibration and BDL
optiConfig = FLASHConfig()
ctCalibration = scannerReader.readScanner(optiConfig.scannerFolder)
aluminiumElement = MCsquareElement.load(5, os.path.join(MCsquareModule.__path__[0]))
tungstenElement = MCsquareElement.load(11, os.path.join(MCsquareModule.__path__[0]))
matNb = len(ctCalibration.allMaterialsAndElements())
aluminium = MCsquareMolecule(density=aluminiumElement.density, electronDensity=aluminiumElement.electronDensity, name='Aluminium_material', number=matNb,
                             sp=aluminiumElement.sp, radiationLength=aluminiumElement.radiationLength,
                             MCsquareElements=[aluminiumElement], weights=[100.])
tungsten = MCsquareMolecule(density=tungstenElement.density, electronDensity=tungstenElement.electronDensity, name='Tungsten_material', number=matNb+1,
                             sp=tungstenElement.sp, radiationLength=tungstenElement.radiationLength,
                             MCsquareElements=[tungstenElement], weights=[100.])
ctCalibration.addEntry(2500, 2.7, aluminium)
ctCalibration.addEntry(5000, 19.3, tungsten)
bdl = mcsquareIO.readBDL(optiConfig.bdlFile)


beam = plan[0]
ctBEV = imageTransform3D.dicomToIECGantry(ct, beam, fillValue=-1024.)
imagePreprocessing.padImageForCEM(ctBEV, beam, inPlace=True)
ctCEM = CTBEVWithCEM(ctBEV)
ctCEM.update(beam.cem, ctCalibration, beam.aperture)

imageArray = ctCEM.imageArray
imageArray[imageArray<-500] = 0

projection = np.sum(imageArray, 1)
projection[projection!=0] = 1

isocenterIndex = ctCEM.getVoxelIndexFromPosition(beam.isocenterPosition)

gridSizeInMM = ctCEM.gridSizeInWorldUnit

fig, ax = plt.subplots()
cax = ax.imshow(projection, extent=(ctCEM.origin[2]-beam.isocenterPosition[2], ctCEM.origin[2]+gridSizeInMM[2]-beam.isocenterPosition[2],
                                    ctCEM.origin[0]-beam.isocenterPosition[0], ctCEM.origin[0]+gridSizeInMM[0]-beam.isocenterPosition[0]),
                interpolation='nearest', clim=(0,.4), cmap='afmhot_r')
ax.plot(0, 0, marker='o')
plt.savefig(os.path.join(path, 'exampleCEM.png'), dpi=600)
plt.show()


