import os

import numpy as np

from opentps import gui
from opentps.core.data import Patient
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareElement import MCsquareElement
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareMolecule import MCsquareMolecule
from opentps.core.data.images import CTImage, ROIMask
from opentps.core.io import scannerReader, mcsquareIO
import opentps.core.processing.doseCalculation.MCsquare.Materials as MCsquareModule
from opentps.core.io.serializedObjectIO import saveDataStructure
from opentps.core.utils.programSettings import ProgramSettings
from opentps_flash.core.data.cem import CEM
from opentps_flash.core.processing.CEMOptimization import cemObjectives

from opentps_flash.core.processing.flashConfig import FLASHConfig
from opentps_flash.core.processing.CEMOptimization.cemBEVWorkflow import Objective, SingleBeamCEMBEVOptimizationWorkflow
# from opentps_flash.core.processing.CEMOptimization.cemBEVWorkflowExtended import SingleBeamCEMBEVOptimizationWorkflowExtended
from opentps_flash.core.processing.planInitializer import BeamInitializerBEV


# Parameters
nozzle_current = 150e-9 # 150 nA
prescription = 8 #Gy
nominalEnergy = 226 # MeV
cemToIsoDist = 400. # from Leuven BDL (Proximal to isocenter)
distance_aperture_water_surface = 50 # 5cm between water surface and distal part of the aperture
gridType = BeamInitializerBEV.GridTypes.HEXAGONAL
CT_depth = int(cemToIsoDist + 100)


# Config files, BDL, scanner
patient = Patient(name='Patient')
gui.patientList.append(patient)
optiConfig = FLASHConfig()
scannerFolder = optiConfig.scannerFolder
bdl = mcsquareIO.readBDL("/home/sylvain/Documents/flash_tps/FLASH/opentps_flash/defaultData/BDL_D58.txt")
ctCalibration = scannerReader.readScanner(scannerFolder)
aluminiumElement = MCsquareElement.load(5, os.path.join(MCsquareModule.__path__[0]))
tungstenElement = MCsquareElement.load(11, os.path.join(MCsquareModule.__path__[0]))
matNb = len(ctCalibration.allMaterialsAndElements())
aluminium = MCsquareMolecule(density=aluminiumElement.density, electronDensity=aluminiumElement.electronDensity, name='Aluminium_material', number=matNb,
                             sp=aluminiumElement.sp, radiationLength=aluminiumElement.radiationLength,
                             MCsquareElements=[aluminiumElement], weights=[100.]) # for RS
tungsten = MCsquareMolecule(density=tungstenElement.density, electronDensity=tungstenElement.electronDensity, name='Tungsten_material', number=matNb+1,
                             sp=tungstenElement.sp, radiationLength=tungstenElement.radiationLength,
                             MCsquareElements=[tungstenElement], weights=[100.]) # for collimator
ctCalibration.addEntry(2500, 2.7, aluminium)
ctCalibration.addEntry(5000, 19.3, tungsten)


# create CT
ctSize = 100
huAir = -1024.
huWater = ctCalibration.convertRSP2HU(1.)
data = huAir * np.ones((200, CT_depth, 200)) # y and z will be flip in BEV
data[:, -150:, :] = huWater
ct = CTImage(name='CT', patient=patient, imageArray=data)

# Target volume
roi = ROIMask(name='TV')
roi.color = (255, 0, 0) # red
data = np.zeros((200, CT_depth, 200)).astype(bool)
# data[100-10:100+10, -100:-80, 100-10:100+10] = True
data[100-30:100+30, -100:-40, 100-30:100+30] = True
roi.imageArray = data
roi.patient = patient

# Water box
water_roi = ROIMask(name='water')
water_roi.color = (0, 255, 0) # green
data = np.zeros((200, CT_depth, 200)).astype(bool)
data[:, -150:, :] = True
water_roi.imageArray = data
water_roi.patient = patient

# Objectives
objectiveTerms = []
obj = cemObjectives.DoseMinObjective(roi, prescription)
objectiveTerms.append(Objective(objectiveTerm=obj, weight=1.))
obj = cemObjectives.DoseMaxObjective(roi, prescription)
objectiveTerms.append(Objective(objectiveTerm=obj, weight=1.))


# Aperture to isocenter distance
ind_water = np.nonzero(water_roi.imageArray)
ind_water_depth_closest = np.min(ind_water[1])
ind_TV = np.nonzero(roi.imageArray)
ind_TV_avg = np.min(ind_TV[1]) + (np.max(ind_TV[1]) - np.min(ind_TV[1])) // 2
# water_surface_to_isocenter_distance = np.abs(ind_water_depth_closest * voxel_spacing - ind_TV_avg * voxel_spacing)
water_surface_to_isocenter_distance = (ind_TV_avg - ind_water_depth_closest) * ct.spacing[2]
distance_aperture_isocenter = distance_aperture_water_surface + water_surface_to_isocenter_distance
print("distance_aperture_isocenter",distance_aperture_isocenter)

workflow = SingleBeamCEMBEVOptimizationWorkflow()

workflow.ctCalibration = ctCalibration
workflow.beamModel = bdl
workflow.gantryAngle = 0
workflow.beamEnergy = 229
workflow.ct = ct
workflow.objectives = objectiveTerms
workflow.spotSpacing = 6.
workflow.maxIteration = 3
workflow.absTol = 0.01

# CEM options
workflow.cemToIsocenter = cemToIsoDist # Base to isocenter
workflow.cemSpikeOrientation = CEM.SpikeOrientations.FACING_PATIENT
workflow.cemDensity = 1.2
workflow.cemMinThickness = 5

# Aperture options
workflow.apertureToIsocenter = distance_aperture_isocenter
workflow.apertureDensity = 8.5
workflow.apertureThickness = 60
workflow.apertureClearance = 2
workflow.apertureLatThickness = 50

# Range shifter options
workflow.rangeShifterToIsocenter = distance_aperture_isocenter + workflow.apertureThickness + 67
print("rangeShifterToIsocenter",workflow.rangeShifterToIsocenter)
workflow.rangeShifterDensity = 2.7
workflow.rangeShifterMargin = 10

# workflow.gridType = gridType
# workflow.maxStep = 20. / 4.

plan = workflow.run()
plan.patient = patient

path = ProgramSettings().exampleFolder
saveDataStructure(patient, os.path.join(path, 'conformalPOne6x6x6_6mmHexaSpikePatient'))

gui.run()