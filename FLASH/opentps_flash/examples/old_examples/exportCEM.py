import os

from opentps import gui
from opentps.core.data.plan import RTPlan
from opentps.core.io.dataLoader import loadData
from opentps.core.utils.programSettings import ProgramSettings

from opentps_flash.core.data.cemFrame import CEMFrame
from opentps_flash.core.io.cemIO import writeCEM

# exampleCEM is created by waterBox.py

def exportCEM(cem):
    _, cem = cem.split()

    cemFrame = CEMFrame()
    cem = cemFrame.addToCEMBEV(cem)

    writeCEM(cem, os.path.join(path, 'exampleCEM.stl'))

if __name__=='__main__':
    path = ProgramSettings().exampleFolder

    loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))
    patient = gui.patientList[0]
    plan = patient.getPatientDataOfType(RTPlan)[0]

    cem = plan[0].cem

    exportCEM(cem)
