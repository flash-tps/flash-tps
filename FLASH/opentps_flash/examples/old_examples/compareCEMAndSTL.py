import os

from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QMainWindow
from opentps import gui
from opentps.core.data.images import CTImage
from opentps.core.io.dataLoader import loadData
from opentps.core.utils.programSettings import ProgramSettings
from opentps.gui.viewer.dataForViewer.image3DForViewer import Image3DForViewer
from opentps.gui.viewer.dataForViewer.polyDataForViewer import PolyDataForViewer
from opentps.gui.viewer.dataViewerComponents.imageViewerComponents.polyData3DLayer_3D import PolyData3DLayer_3D
from opentps.gui.viewer.dataViewerComponents.imageViewerComponents.primaryImage3DLayer_3D import PrimaryImage3DLayer_3D
from vtkmodules import vtkRenderingCore, vtkInteractionStyle
from vtkmodules.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtkmodules.vtkIOGeometry import vtkSTLReader


# exampleCEM is created by waterBox.py . STL is generated by exportCEM.py


path = ProgramSettings().exampleFolder

loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))
patient = gui.patientList[0]
ctCEM = patient.getPatientDataOfType(CTImage)[-1]

imageArray = ctCEM.imageArray
imageArray[imageArray>2000] = -1024 # remove range shifter and aperture
ctCEM.imageArray = imageArray


stlReader = vtkSTLReader()
stlReader.SetFileName(os.path.join(path, 'exampleCEM.stl'))

cemSTLImage = PolyDataForViewer(stlReader)

app = QApplication([])

mainWindow = QMainWindow()
mainWindow.resize(1000, 1000)

centralWidget = QWidget()
layout = QHBoxLayout()
centralWidget.setLayout(layout)
mainWindow.setCentralWidget(centralWidget)


renderer = vtkRenderingCore.vtkRenderer()
vtkWidget = QVTKRenderWindowInteractor(centralWidget)
renderWindow = vtkWidget.GetRenderWindow()
iStyle = vtkInteractionStyle.vtkInteractorStyleTrackballCamera()

renderer.SetBackground(0, 0, 0)
renderWindow.GetInteractor().SetInteractorStyle(iStyle)
renderWindow.AddRenderer(renderer)

stlLayer = PolyData3DLayer_3D(renderer, renderWindow)
stlLayer.image = cemSTLImage
stlLayer.update()
renderer.ResetCamera()

layout.addWidget(vtkWidget)


renderer2 = vtkRenderingCore.vtkRenderer()
vtkWidget2 = QVTKRenderWindowInteractor(centralWidget)
renderWindow2 = vtkWidget2.GetRenderWindow()
iStyle2 = vtkInteractionStyle.vtkInteractorStyleTrackballCamera()

renderer2.SetBackground(0, 0, 0)
renderWindow2.GetInteractor().SetInteractorStyle(iStyle2)
renderWindow2.AddRenderer(renderer2)

layer2 = PrimaryImage3DLayer_3D(renderer2, renderWindow2, iStyle2)
layer2.image = Image3DForViewer(ctCEM)
layer2.update()
renderer2.ResetCamera()

layout.addWidget(vtkWidget2)

mainWindow.show()
app.exec_()
