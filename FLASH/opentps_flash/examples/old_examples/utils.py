import matplotlib.pyplot as plt
import numpy as np
from opentps_flash.core.data.drvh import DRVH
from opentps.core.data._dvh import DVH

def plot_map(plan, startTime=None, save_plot=None):
    xy = plan[0].spotXY
    x= np.array([elem[0] for elem in xy])
    y= np.array([elem[1] for elem in xy])
    mu = plan[0].spotMUs

    fig, ax = plt.subplots(1, 1)
    for i, xyElem in enumerate(xy):
        if i==0:
            if startTime is not None:  
                ax.text(x[i],y[i],f'{startTime[i]*1000:.0f}')
            continue
        # ax.arrow(-xy[i-1][0], xy[i-1][1], -xyElem[0]+xy[i-1][0], xyElem[1]-xy[i-1][1], fc='k', ec='k', head_width=1, head_length=1)
        ax.arrow(xy[i-1][0], xy[i-1][1], xyElem[0]-xy[i-1][0], xyElem[1]-xy[i-1][1], fc='k', ec='k', head_width=1, head_length=1)
        if startTime is not None:  
            # ax.text(-x[i],y[i],f'{startTime[i]*1000:.0f}')
            ax.text(x[i],y[i],f'{startTime[i]*1000:.0f}')

    # cax = ax.scatter(-x, y, c=mu, cmap=plt.cm.jet)
    cax = ax.scatter(x, y, c=mu, cmap=plt.cm.jet)
    ax.set_xlabel("x (mm)")
    ax.set_ylabel("y (mm)")
    ax.set_aspect('equal')
    cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
    cbar.set_ticks([np.min(mu), (np.min(mu)+np.max(mu))/2., np.max(mu)])
    cbar.ax.set_title('Monitor units')
    ax.grid(True)
    ax.legend()
    if save_plot is not None:
        fig.savefig(save_plot, format='png')

def plot_DVH(DVHs, save_plot=None, prescription=8):
    fig, ax = plt.subplots(1, 1)
    if type(DVHs) is list:
        for dvh in DVHs:
            ax.plot(dvh.histogram[0], dvh.histogram[1], label=dvh.name, linewidth=2)
        dvh_type = type(DVHs[0])
    else:
        ax.plot(DVHs.histogram[0], DVHs.histogram[1], label=DVHs.name, linewidth=2)
        dvh_type = type(DVHs)
    if dvh_type is DVH:
        ax.set_title("DVH")
        ax.set_xlabel("Dose (Gy)")
        ax.set_ylabel("Volume (%)")
        ax.grid(True)
        ax.set_ylim((0, 100))
        ax.set_xlim((0, prescription+5))
    elif dvh_type is DRVH:
        ax.set_title("Percentile-95 DRVH")
        ax.set_xlabel("Dose rate (Gy/s)")
        ax.set_ylabel("Volume (%)")
        ax.grid(True)
        ax.set_ylim((0, 100))
        ax.set_xlim((0, 100))
    ax.legend()
    if save_plot is not None:
        fig.savefig(save_plot, format='png')

def plot_CT_with_dose(ct, roi, dose, save_plot=None, prescription=8):
    # TODO: ensure that all images have the same grid size
    roiCenterOfMass = roi.centerOfMass
    roiCenterOfMassInVoxels = ct.getVoxelIndexFromPosition(roiCenterOfMass)
    fig, ax = plt.subplots(1, 1)
    ax.imshow(ct.imageArray[:,:,roiCenterOfMassInVoxels[2]].T, cmap='gray')
    dose_to_plot = dose.imageArray.copy()
    dose_to_plot[dose_to_plot<0.1] = np.nan
    dose_to_plot[dose_to_plot>prescription+1] = np.nan
    ax.imshow(dose_to_plot[:,:,roiCenterOfMassInVoxels[2]].T, cmap='jet', alpha=0.5)
    roi_to_plot = roi.getBinaryContourMask().imageArray.astype(float)
    roi_to_plot[roi_to_plot==0.] = np.nan
    ax.imshow(roi_to_plot[:,:,roiCenterOfMassInVoxels[2]].T, interpolation='None')
    if save_plot is not None:
        fig.savefig(save_plot, format='png')