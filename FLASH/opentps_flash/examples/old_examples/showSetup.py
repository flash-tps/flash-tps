import os

from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QMainWindow
from opentps import gui
from opentps.core.data.images import CTImage
from opentps.core.io.dataLoader import loadData
from opentps.core.utils.programSettings import ProgramSettings
from opentps.gui.viewer.dataForViewer.image3DForViewer import Image3DForViewer
from opentps.gui.viewer.dataViewerComponents.imageViewerComponents.primaryImage3DLayer_3D import PrimaryImage3DLayer_3D
from vtkmodules import vtkRenderingCore, vtkInteractionStyle
from vtkmodules.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor


# exampleCEM is created by waterBox.py .
def showSetup(ctCEM:CTImage):
    app = QApplication([])

    mainWindow = QMainWindow()
    mainWindow.resize(1000, 1000)

    centralWidget = QWidget()
    layout = QHBoxLayout()
    centralWidget.setLayout(layout)
    mainWindow.setCentralWidget(centralWidget)

    renderer2 = vtkRenderingCore.vtkRenderer()
    vtkWidget2 = QVTKRenderWindowInteractor(centralWidget)
    renderWindow2 = vtkWidget2.GetRenderWindow()
    iStyle2 = vtkInteractionStyle.vtkInteractorStyleTrackballCamera()

    renderer2.SetBackground(0, 0, 0)
    renderWindow2.GetInteractor().SetInteractorStyle(iStyle2)
    renderWindow2.AddRenderer(renderer2)

    layer2 = PrimaryImage3DLayer_3D(renderer2, renderWindow2, iStyle2)
    layer2.image = Image3DForViewer(ctCEM)
    layer2.update()
    renderer2.ResetCamera()

    layout.addWidget(vtkWidget2)

    mainWindow.show()
    app.exec_()

if __name__=='__main__':
    path = ProgramSettings().exampleFolder

    loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))
    patient = gui.patientList[0]
    ctCEM = patient.getPatientDataOfType(CTImage)[-1]

    showSetup(ctCEM)
