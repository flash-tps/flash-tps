
import os

import numpy as np
from matplotlib import pyplot as plt
from opentps import gui
from opentps.core.data.plan import RTPlan
from opentps.core.io.dataLoader import loadData
from opentps.core.utils.programSettings import ProgramSettings


# exampleCEM is created by waterBox.py

def showSpotMap(plan: RTPlan):
    xy = plan[0].spotXY
    x = np.array([elem[0] for elem in xy])
    y = np.array([elem[1] for elem in xy])
    mu = plan[0].spotMUs

    fig, ax = plt.subplots(1, 1, figsize=(12, 5))
    for i, xyElem in enumerate(xy):
        if i == 0:
            continue
        ax.arrow(-xy[i - 1][0], xy[i - 1][1], -xyElem[0] + xy[i - 1][0], xyElem[1] - xy[i - 1][1], fc='k', ec='k',
                 head_width=1, head_length=1)

    cax = ax.scatter(-x, y, c=mu, cmap=plt.cm.jet)
    ax.set_xlabel("x (mm)", fontsize=20)
    ax.set_ylabel("y (mm)", fontsize=20)
    ax.set_aspect('equal')
    cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
    cbar.set_ticks([np.min(mu), (np.min(mu) + np.max(mu)) / 2., np.max(mu)])
    cbar.ax.set_title('Monitor units')
    plt.grid(True)
    plt.legend(prop={'size': 18})
    plt.rcParams.update({'axes.labelsize': 30, 'axes.titlesize': 30, 'legend.fontsize': 30, 'xtick.labelsize': 30,
                         'ytick.labelsize': 30})

    plt.show()


if __name__=='__main__':
    path = ProgramSettings().exampleFolder

    loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))
    patient = gui.patientList[0]
    plan = patient.getPatientDataOfType(RTPlan)[0]

    plan.reorderPlan(order_layers="decreasing", order_spots="scanAlgo")

    showSpotMap(plan)

