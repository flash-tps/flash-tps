import numpy as np

from opentps.core.io import mcsquareIO


# refaire en http://www.openmcsquare.org/documentation_commissioning.html

def nozzleToIsocenter_first(BDL):
    if type(BDL) == str:
        bdl = mcsquareIO.readBDL(BDL)
    else:
        bdl = BDL
    posNozzle = np.array([bdl.spotSize1x[-1], bdl.spotSize1y[-1]])
    distToIso = bdl.nozzle_isocenter
    divergence = np.array([bdl.divergence1x[-1], bdl.divergence1y[-1]])

    return (2 * np.tan(divergence / 2) * distToIso) + posNozzle


def nozzleToIsocenter_snyder(BDL):
    if type(BDL) == str:
        bdl = mcsquareIO.readBDL(BDL)
    else:
        bdl = BDL

    dist = bdl.nozzle_isocenter

    div_x = bdl.divergence1x[-1]
    div_y = bdl.divergence1y[-1]

    pos_x_z = bdl.spotSize1x[-1]
    pos_y_z = bdl.spotSize1y[-1]

    corr_x_z = bdl.correlation1x[-1]
    corr_y_z = bdl.correlation1y[-1]

    pos_x_iso_squared = (pos_x_z ** 2) + (2 * corr_x_z * pos_x_z * div_x * dist) + (div_x ** 2) * (dist ** 2)
    pos_x_iso = np.sqrt(pos_x_iso_squared)

    pos_y_iso_squared = (pos_y_z ** 2) + (2 * corr_y_z * pos_y_z * div_y * dist) + (div_y ** 2) * (dist ** 2)
    pos_y_iso = np.sqrt(pos_y_iso_squared)

    return pos_x_iso, pos_y_iso

def zToIsocenter(sigma_z, div_z, corr_z, z):
    pos_x_iso_squared = (sigma_z ** 2) + (2 * corr_z * sigma_z * div_z * z) + (div_z ** 2) * (z ** 2)
    sigma_0 = np.sqrt(pos_x_iso_squared)

    corr_0 = (corr_z*sigma_z + div_z*z)/sigma_0

    return sigma_0, div_z, corr_0


def isocenterToZ(sigma_0, div_0, corr_0, z):
    sigma_z = np.sqrt(sigma_0**2 - 2*corr_0*sigma_0*div_0*z + div_0**2*z**2)
    div_z = div_0
    corr_z = (corr_0*sigma_0-div_0*z)/sigma_z

    return sigma_z, div_z, corr_z



if __name__ == '__main__':

    BDL3 = "/Users/manopletinckx/Desktop/TFE/flash_tps/FLASH/opentps_flash/defaultData/BDL3.txt"
    BDL_memoire = "/Users/manopletinckx/Desktop/TFE/flash_tps/FLASH/opentps_flash/defaultData/BDL_memoire.txt"
    BDL4_5_5 = "/Users/manopletinckx/Desktop/TFE/flash_tps/FLASH/opentps_flash/defaultData" \
               "/BDL_default_RS_Leuven_4_5_5.txt"
    print(nozzleToIsocenter_snyder(BDL4_5_5))
    print(" vs ")
    print(nozzleToIsocenter_first(BDL4_5_5))


    sigma_z, div_z, corr_z = isocenterToZ(0.499, 0.00048, 0.00425, 410)
    print(sigma_z, div_z, corr_z)
    print(zToIsocenter(sigma_z, div_z, corr_z, 410))