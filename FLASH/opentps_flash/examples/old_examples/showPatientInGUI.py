

import os

from opentps import gui
from opentps.core.io.dataLoader import loadData
from opentps.core.utils.programSettings import ProgramSettings


# exampleCEM is created by waterBox.py

path = ProgramSettings().exampleFolder

loadData(gui.patientList, os.path.join(path, 'exampleCEM.p'))

gui.run()
