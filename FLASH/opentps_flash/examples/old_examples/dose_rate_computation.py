import os
import matplotlib.pyplot as plt
from opentps.core.io import mcsquareIO, scannerReader
from opentps.core.io.serializedObjectIO import loadBeamlets, loadDataStructure
from opentps.core.data.plan import RTPlan
from opentps.core.data.images._ctImage import CTImage
from opentps.core.data._rtStruct import RTStruct
from opentps_flash.core.data.drvh import DRVH
from opentps.core.processing.doseCalculation.mcsquareDoseCalculator import MCsquareDoseCalculator
from opentps_flash.core.processing.doseRate.flashDeliveryTimings import FlashDeliveryTimings
from opentps_flash.core.processing.doseRate.percentileDoseRateCalculator import PercentileDoseRateCalculator
from opentps_flash.core.processing.doseRate.doseAveragedDoseRateCalculator import DoseAveragedDoseRateCalculator
from opentps_flash.core.processing.flashConfig import FLASHConfig
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareElement import MCsquareElement
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareMolecule import MCsquareMolecule
import opentps.core.processing.doseCalculation.MCsquare.Materials as MCsquareModule
from opentps.core.processing.planDeliverySimulation.simpleBeamDeliveryTimings import SimpleBeamDeliveryTimings

filepath = r'C:\Users\valentin.hamaide\data\FLASH\PT_1.p'
folderpath = r'C:\Users\valentin.hamaide\data\FLASH\PT_1'
patient = loadDataStructure(filepath)[0]
plan = patient.getPatientDataOfType(RTPlan)[-1]
ct = [ct for ct in patient.getPatientDataOfType(CTImage) if ct.name == "CT with CEM"][-1]
struct = patient.getPatientDataOfType(RTStruct)[0]
ptv7000ROI = struct.getContourByName("FLASHTarget")
ptv7000ROI = ptv7000ROI.getBinaryMask(ct.origin, ct.gridSize, ct.spacing)
ptv7000ROI.dilateMask(2.)
print(ct.name)

optiConfig = FLASHConfig()
ctCalibration = scannerReader.readScanner(optiConfig.scannerFolder)
aluminiumElement = MCsquareElement.load(5, os.path.join(MCsquareModule.__path__[0]))
tungstenElement = MCsquareElement.load(11, os.path.join(MCsquareModule.__path__[0]))
matNb = len(ctCalibration.allMaterialsAndElements())
aluminium = MCsquareMolecule(density=aluminiumElement.density, electronDensity=aluminiumElement.electronDensity, name='Aluminium_material', number=matNb,
                             sp=aluminiumElement.sp, radiationLength=aluminiumElement.radiationLength,
                             MCsquareElements=[aluminiumElement], weights=[100.])
tungsten = MCsquareMolecule(density=tungstenElement.density, electronDensity=tungstenElement.electronDensity, name='Tungsten_material', number=matNb+1,
                             sp=tungstenElement.sp, radiationLength=tungstenElement.radiationLength,
                             MCsquareElements=[tungstenElement], weights=[100.])
ctCalibration.addEntry(2500, 2.7, aluminium)
ctCalibration.addEntry(5000, 19.3, tungsten)
bdl = mcsquareIO.readBDL(optiConfig.bdlFile)

mc2 = MCsquareDoseCalculator()
mc2.beamModel = bdl
mc2.ctCalibration = ctCalibration
mc2.nbPrimaries = 1e5

pdrc = PercentileDoseRateCalculator(deliveryModel=FlashDeliveryTimings())
# pdrc = DoseAveragedDoseRate()
pdrc.doseCalculator = mc2
# pdrc.bdl = mc2.beamModel
pdrc.percentile = 0.95
# pdrc.current = 350e-9
# pdrc.scanningSpeed = 8000
# pdrc.deliveryModel = SimpleBeamDeliveryTimings

beamlet_filepath = os.path.join(folderpath, "BeamletMatrix_" + plan.seriesInstanceUID + ".blm")
if not os.path.exists(beamlet_filepath):
    beamlets = mc2.computeBeamlets(ct, plan, roi=[ptv7000ROI])
    beamlets.storeOnFS(beamlet_filepath)
else:
    beamlets = loadBeamlets(beamlet_filepath)

# pdrc.method = 'MIROpt'
dr = pdrc.computeDoseRate(ct, plan, [ptv7000ROI], beamlets=beamlets)
dr.patient = patient

#### Plot Dose rate map ####
fig, ax = plt.subplots(1,2,figsize=(10,5))
# Plot CT
PTVCenterOfMass = ptv7000ROI.centerOfMass
PTVCenterOfMassInVoxels = ct.getVoxelIndexFromPosition(PTVCenterOfMass)
ax[0].imshow(ct.imageArray[:,:,PTVCenterOfMassInVoxels[2]].transpose(1,0), cmap='gray')
ax[0].imshow(dr.imageArray[:,:,PTVCenterOfMassInVoxels[2]].transpose(1,0), cmap='jet', alpha=.2)
# plt.colorbar()
# plt.show()


# Plot Dose-rate volumne histogram
target_DVH = DRVH(ptv7000ROI, dr)
oar_DVH = DRVH(struct.getContourByName("Larynx-CTV"), dr)
ax[1].plot(target_DVH.histogram[0], target_DVH.histogram[1], label='PTV', linewidth=2)
ax[1].plot(oar_DVH.histogram[0], oar_DVH.histogram[1], label='Larynx-CTV', linewidth=2)
ax[1].set_xlabel("Dose rate (Gy/s)", fontsize=20)
ax[1].set_ylabel("Volume (%)", fontsize=20)
ax[1].grid(True)
ax[1].set_ylim((0, 100))
ax[1].set_xlim((0, 200))
ax[1].legend(prop={'size': 18})
plt.rcParams.update({'font.size': 20})

plt.show()