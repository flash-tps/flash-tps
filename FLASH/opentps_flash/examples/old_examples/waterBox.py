import os

import numpy as np
from opentps import gui
from opentps.core.data import Patient
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareElement import MCsquareElement
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareMolecule import MCsquareMolecule
from opentps.core.data.CTCalibrations.MCsquareCalibration._mcsquareMaterial import MCsquareMaterial
from opentps.core.data.images import CTImage, ROIMask
from opentps.core.io import scannerReader, mcsquareIO
import opentps.core.processing.doseCalculation.MCsquare.Materials as MCsquareModule
from opentps.core.io.serializedObjectIO import saveDataStructure
from opentps.core.utils.programSettings import ProgramSettings

from opentps_flash.core.processing.CEMOptimization import cemObjectives
from opentps_flash.core.processing.CEMOptimization.cemBEVWorkflow import Objective, \
    SingleBeamCEMBEVOptimizationWorkflow
from opentps_flash.core.processing.flashConfig import FLASHConfig


patient = Patient(name='Patient')
gui.patientList.append(patient)

# CT Calibration and BDL
optiConfig = FLASHConfig()
scannerFolder = optiConfig.scannerFolder
bdlFile = optiConfig.bdlFile
ctCalibration = scannerReader.readScanner(scannerFolder)
aluminium = MCsquareMaterial.load("aluminium")
abs_resin = MCsquareMaterial.load("ABS_Resin")
brass = MCsquareMaterial.load("brass")

ctCalibration.addEntry(2, abs_resin.density, abs_resin)
ctCalibration.addEntry(2500, aluminium.density, aluminium)
ctCalibration.addEntry(5000, brass.density, brass)
bdl = mcsquareIO.readBDL(bdlFile)


# CT and ROI
ctSize = 100
huAir = -1000.
huWater = 0 #ctCalibration.convertRSP2HU(1.)
data = huAir * np.ones((ctSize, ctSize, ctSize))
data[:, 30:, :] = huWater
ct = CTImage(name='CT', patient=patient, imageArray=data)

roi = ROIMask(name='TV')
roi.color = (255, 0, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[40:60, 40:60, 40:60] = True
roi.imageArray = data
roi.patient = patient

oar = ROIMask(name='OAR')
oar.color = (0, 255, 0) # red
data = np.zeros((ctSize, ctSize, ctSize)).astype(bool)
data[50-15:50+15, 30:50, 50-15:50+15] = True
oar.imageArray = data
oar.patient = patient

# Objectives
objectiveTerms = []
obj = cemObjectives.DoseMinObjective(roi, 50)
objectiveTerms.append(Objective(objectiveTerm=obj, weight=1.))
obj = cemObjectives.DoseMaxObjective(roi, 50.5)
objectiveTerms.append(Objective(objectiveTerm=obj, weight=1.))
obj = cemObjectives.DoseMaxObjective(oar, 50.5)
objectiveTerms.append(Objective(objectiveTerm=obj, weight=1.))

#Optimizer
beamAngle = 0
cemToIsoDist = 400
rsDist = 200
apertureDist = 75
beamEnergy = 226
spotSpacing = 7
apertureDensity = brass.density
cemDensity = abs_resin.density
rangeShifterDensity = aluminium.density

workflow = SingleBeamCEMBEVOptimizationWorkflow()
workflow.ctCalibration = ctCalibration
workflow.beamModel = bdl
workflow.gantryAngle = beamAngle
workflow.cemToIsocenter = cemToIsoDist
workflow.beamEnergy = beamEnergy
workflow.ct = ct
workflow.spotSpacing = spotSpacing

# CEM options
workflow.cemToIsocenter = cemToIsoDist # Base to isocenter
workflow.cemDensity = cemDensity

# Aperture options
workflow.apertureToIsocenter = apertureDist
workflow.apertureDensity = apertureDensity
workflow.apertureThickness = 30
workflow.apertureClearance = 5
workflow.apertureLatThickness = 30

# Range shifter options
workflow.rangeShifterToIsocenter = rsDist
print("rangeShifterToIsocenter",workflow.rangeShifterToIsocenter)
workflow.rangeShifterDensity = aluminium.density


workflow.objectives = objectiveTerms
workflow.maxIteration = 3
workflow.nbPrimariesFinalDose = 1e7

plan = workflow.run()
plan.patient = patient

path = ProgramSettings().exampleFolder
saveDataStructure(patient, os.path.join(path, 'exampleCEM'))

gui.run()
