import numpy as np
from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from pyqtgraph import PlotWidget, PlotCurveItem


class DoseRateProfile(QMainWindow):
    def __init__(self, image, parent=None):
        super().__init__(parent)

        self._doseRate = None
        self._dose = None
        self._image = None

        self.setWindowTitle('Dose rate')
        self.resize(800, 600)

        centralWidget = QWidget()
        self.setCentralWidget(centralWidget)
        self._layout = QHBoxLayout()
        centralWidget.setLayout(self._layout)

        self._profilePlot = _ProfilePlot()
        self._profile = self._profilePlot.newProfile([0, 0], [0, 0])

        self._layout.addWidget(self._profilePlot)

    def setImage(self, image):
        if not(self._image is None):
            self._image.selectedPositionChangedSignal.disconnect(self._update)

        self._image = image
        self._image.selectedPositionChangedSignal.connect(self._update)

    @property
    def doseRate(self):
        return self._doseRate

    @doseRate.setter
    def doseRate(self, dr):
        self._doseRate = dr
        self._dose = self._doseRate.dose


    def _update(self, val):
        val = self._dose.getVectorAtPosition(self._image.selectedPosition)
        self._profilePlot.removeItem(self._profile)
        self._profile = self._profilePlot.newProfile(self._dose.startTime, np.cumsum(np.squeeze(val)))

class _ProfilePlot(PlotWidget):
    def __init__(self):
        PlotWidget.__init__(self)

        self.addLegend()
        self.getPlotItem().setContentsMargins(5, 0, 20, 5)
        self.setBackground('k')
        self.setTitle("Profiles")
        self.setLabel('left', 'Intensity')
        self.setLabel('bottom', 'Time (s)')

    def newProfile(self, *args, **kwargs) -> PlotCurveItem:
        pl = PlotCurveItem(*args, **kwargs)
        self.addItem(pl)

        return pl